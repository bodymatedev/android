package de.bodymate.bodymateapp.util

import com.google.common.truth.Truth.assertThat
import org.joda.time.DateTimeConstants
import org.joda.time.LocalDateTime
import org.joda.time.LocalTime
import org.junit.Test
import java.util.*

class DateUtilsTests {

    private val locale = Locale.GERMANY
    private val timeZone = TimeZone.getTimeZone("Europe/Berlin")

    @Test
    fun dateTimeNextWeekday_returns_correct(){

        val nextMonday = dateTimeNextWeekday(1, time("18:30:00")!!)

        assertThat(nextMonday).isNotNull()
        assertThat(nextMonday?.dayOfWeek).isEqualTo(DateTimeConstants.TUESDAY)
        assertThat(nextMonday?.hourOfDay).isEqualTo(18)
        assertThat(nextMonday?.minuteOfHour).isEqualTo(30)
        assertThat(nextMonday?.secondOfMinute).isEqualTo(0)
    }

    @Test
    fun LocalTime_toPrint_returns_correct(){

        assertThat(time("18:30:00")?.toPrint()).isEqualTo("18:30")
        assertThat(time("18:00:00")?.toPrint()).isEqualTo("18")
    }

    @Test
    fun LocalDateTime_timeInMillies_returns_correct(){

        val calendarJavaNow = Calendar.getInstance(timeZone, locale)
        calendarJavaNow.set(2019, 0, 28, 18, 0, 0)
        calendarJavaNow.set(Calendar.MILLISECOND, 0)

        val localDateTime = dateTimeNextWeekday(0, LocalTime(18, 0, 0))
        //assertThat(calendarJavaNow.timeInMillis).isEqualTo(localDateTime!!.timeInMillis())
    }
}