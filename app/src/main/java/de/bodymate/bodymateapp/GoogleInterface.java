package de.bodymate.bodymateapp;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;
import android.support.v7.app.AppCompatActivity;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.Task;


public class GoogleInterface implements GoogleApiClient.OnConnectionFailedListener{

	//private static final String WEB_CLIENT_ID = "358458322823-oupq6veju0rrmvqjus2st0oputq14ef0.apps.googleusercontent.com";
	private static final String WEB_CLIENT_ID = "647900371015-rdeii2p205hv8r13alsj1vln4mekhs0g.apps.googleusercontent.com";
	public static final int RC_SIGN_IN = 9001;

	private AppCompatActivity activity;
	private GoogleLoginCallback activityLoginCallback;

	private static GoogleSignInOptions googleSignInOptions;

	private GoogleInterface(@NonNull final AppCompatActivity activity){

		this.activity = activity;
	}

	@NonNull
	public static GoogleInterface create(@NonNull AppCompatActivity activity){

		if( googleSignInOptions == null ){
			googleSignInOptions = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
					.requestIdToken(WEB_CLIENT_ID)
					.requestEmail()
					.build();
		}

		return new GoogleInterface(activity);
	}

	public void loginGoogle(@NonNull GoogleLoginCallback activityLoginCallback){

		this.activityLoginCallback = activityLoginCallback;

		Intent signInIntent = GoogleSignIn.getClient(activity, googleSignInOptions).getSignInIntent();
		activity.startActivityForResult(signInIntent, RC_SIGN_IN);
	}

	public void onLoginResult(@NonNull Task<GoogleSignInAccount> completedTask){
		try{
			GoogleSignInAccount account = completedTask.getResult(ApiException.class);
			if( account != null ){
				activityLoginCallback.onSuccess(account);
			}else{
				activityLoginCallback.onFailure(R.string.google_login_failed);
			}
		}catch( ApiException apiEx ){
			activityLoginCallback.onFailure(R.string.google_login_failed);
		}
	}

	@Override
	public void onConnectionFailed(@NonNull ConnectionResult connectionResult){
		activityLoginCallback.onConnectionFailed(R.string.google_login_connection_failed);
	}

	public interface GoogleLoginCallback{
		void onSuccess(@NonNull GoogleSignInAccount account);
		void onFailure(@StringRes int textId);
		void onConnectionFailed(@StringRes int textId);
	}
}