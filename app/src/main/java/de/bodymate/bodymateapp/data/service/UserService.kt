package de.bodymate.bodymateapp.data.service

import android.app.Application
import android.arch.lifecycle.LiveData
import de.bodymate.bodymateapp.api.response.RBUser
import de.bodymate.bodymateapp.data.repository.UserRepository
import de.bodymate.bodymateapp.data.repository.entity.RoomUser
import io.reactivex.Single

class UserService private constructor(application: Application) {

    private val repository = UserRepository(application)

    val currentUser: LiveData<RoomUser> by lazy {
        repository.first
    }

    val currentUserAsSingle: Single<RoomUser> by lazy {
        repository.firstAsSingle
    }

    fun update(rbUser: RBUser) {
        repository.setUser(RoomUser.createFromApiEntity(rbUser))
    }

    fun deleteUser() {
        repository.delete()
    }

    companion object {

        @Volatile
        private var INSTANCE: UserService? = null

        fun getInstance(application: Application): UserService =
                INSTANCE ?: synchronized(this) {
                    INSTANCE ?: UserService(application).also { INSTANCE = it }
                }
    }
}
