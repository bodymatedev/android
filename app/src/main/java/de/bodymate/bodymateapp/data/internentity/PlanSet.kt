package de.bodymate.bodymateapp.data.internentity

/**
 * Created by Leonard on 28.06.2018.
 */

data class PlanSet(val set: Int, val reps: Int)