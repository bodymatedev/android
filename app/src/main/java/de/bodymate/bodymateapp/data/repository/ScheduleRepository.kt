package de.bodymate.bodymateapp.data.repository

import android.app.Application
import android.arch.lifecycle.LiveData
import de.bodymate.bodymateapp.data.repository.dao.ScheduleDao
import de.bodymate.bodymateapp.data.repository.database.BodyMateDatabase
import de.bodymate.bodymateapp.data.repository.entity.RoomSchedule
import io.reactivex.Single
import org.jetbrains.anko.doAsync

/**
 * Created by Leonard on 14.06.2018.
 */

class ScheduleRepository(application: Application) {

    private val mScheduleDao: ScheduleDao = BodyMateDatabase.getDatabase(application).scheduleDao()

    val allSchedules: LiveData<List<RoomSchedule>> by lazy {
        mScheduleDao.getAllSchedules()
    }

    val allSchedulesAsSingle: Single<List<RoomSchedule>> by lazy {
        mScheduleDao.getAllSchedulesAsSingle()
    }

    fun all(): List<RoomSchedule> = mScheduleDao.getAllSchedulesAsCor()

    fun updateAll(roomSchedules: List<RoomSchedule>) {

        doAsync {
            mScheduleDao.deleteAll()
            mScheduleDao.insertAll(roomSchedules)
        }
    }

    fun deleteAll() {
        doAsync { mScheduleDao.deleteAll() }
    }
}
