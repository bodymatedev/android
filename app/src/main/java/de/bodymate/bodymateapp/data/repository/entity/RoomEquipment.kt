package de.bodymate.bodymateapp.data.repository.entity

import android.arch.persistence.room.Entity
import android.arch.persistence.room.Ignore
import android.arch.persistence.room.PrimaryKey
import de.bodymate.bodymateapp.api.response.RBEquipment

/**
 * Created by Leonard on 05.06.2018.
 */

@Entity(tableName = "equipment")
data class RoomEquipment(@field:PrimaryKey val id: Int,
                         val name: String,
                         val specification: String?) {

    companion object {
        @Ignore
        fun createFromApiEntity(rbEquipment: RBEquipment): RoomEquipment {
            return RoomEquipment(rbEquipment.id, rbEquipment.name, rbEquipment.specification)
        }
    }
}