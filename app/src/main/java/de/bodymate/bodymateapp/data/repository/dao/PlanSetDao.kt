package de.bodymate.bodymateapp.data.repository.dao

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query

import de.bodymate.bodymateapp.data.repository.entity.RoomPlanSet

import android.arch.persistence.room.OnConflictStrategy.REPLACE

/**
 * Created by Leonard on 26.06.2018.
 */
@Dao
abstract class PlanSetDao {

    @Insert(onConflict = REPLACE)
    abstract fun insertAll(roomPlanSets: List<RoomPlanSet>)

    @Query("SELECT * FROM plan_set WHERE plan_id=:planId")
    abstract fun getAllPlanSetsOfPlan(planId: Int): LiveData<List<RoomPlanSet>>

    @Query("DELETE FROM plan_set")
    abstract fun deleteAll()
}
