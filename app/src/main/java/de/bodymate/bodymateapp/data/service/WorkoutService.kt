package de.bodymate.bodymateapp.data.service

import android.app.Application
import android.arch.lifecycle.LiveData
import de.bodymate.bodymateapp.api.response.RBWorkout
import de.bodymate.bodymateapp.data.repository.WorkoutRepository
import de.bodymate.bodymateapp.data.repository.entity.RoomWorkout
import de.bodymate.bodymateapp.util.DateUtils
import de.bodymate.bodymateapp.util.todayAsString
import io.reactivex.Single

class WorkoutService private constructor(application: Application) {

    private val repository = WorkoutRepository(application)

    val latest3: LiveData<List<RoomWorkout>> by lazy {
        repository.latest3
    }

    fun latest3AsCor(): List<RoomWorkout> = repository.latest3AsCor()

    fun deleteAll(){
        repository.deleteAll()
    }

    fun update(rbWorkout: RBWorkout?){

        if( rbWorkout != null ) {
            repository.update(RoomWorkout.fromApiEntity(rbWorkout))
        }else{
            repository.deleteAll()
        }
    }

    fun add(rbWorkout: RBWorkout?) {

        if (rbWorkout != null) {
            repository.insertOne(RoomWorkout.fromApiEntity(rbWorkout))
        }
    }

    fun addAll(rbWorkouts: List<RBWorkout>?){

        if( rbWorkouts != null ){
            repository.insertAll(rbWorkouts.map { RoomWorkout.fromApiEntity(it) })
        }
    }

    fun workoutCount(): LiveData<Int> {
        return repository.count
    }

    fun countTodays(): Int = repository.countTodays(todayAsString())

    companion object {

        @Volatile
        private var INSTANCE: WorkoutService? = null

        fun getInstance(application: Application): WorkoutService =
                INSTANCE ?: synchronized(this) {
                    INSTANCE ?: WorkoutService(application).also { INSTANCE = it }
                }
    }
}
