package de.bodymate.bodymateapp.data.repository.dao

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query

import de.bodymate.bodymateapp.data.repository.entity.RoomSchedule

import android.arch.persistence.room.OnConflictStrategy.REPLACE
import io.reactivex.Observable
import io.reactivex.Single

/**
 * Created by Leonard on 14.06.2018.
 */

@Dao
abstract class ScheduleDao {

    @Query("SELECT * FROM schedule")
    abstract fun getAllSchedules(): LiveData<List<RoomSchedule>>

    @Query("SELECT * FROM schedule")
    abstract fun getAllSchedulesAsSingle(): Single<List<RoomSchedule>>

    @Query("SELECT * FROM schedule")
    abstract fun getAllSchedulesAsCor(): List<RoomSchedule>

    @Insert(onConflict = REPLACE)
    abstract fun insertAll(roomSchedules: List<RoomSchedule>)

    @Query("DELETE FROM schedule")
    abstract fun deleteAll()
}
