package de.bodymate.bodymateapp.data.service

import android.app.Application
import android.arch.lifecycle.LiveData
import de.bodymate.bodymateapp.api.response.RBEquipment
import de.bodymate.bodymateapp.data.repository.EquipmentRepository
import de.bodymate.bodymateapp.data.repository.entity.RoomEquipment

class EquipmentService private constructor(application: Application) {

    private val repository = EquipmentRepository(application)

    val allEquipments: LiveData<List<RoomEquipment>> by lazy {
        repository.allEquipments
    }

    fun updateAll(rbEquipments: List<RBEquipment>?) {

        if (rbEquipments != null) {
            repository.updateAll(rbEquipments.map { RoomEquipment.createFromApiEntity(it) })
        }
    }

    fun deleteAll() {
        repository.deleteAll()
    }

    companion object {

        @Volatile
        private var INSTANCE: EquipmentService? = null

        fun getInstance(application: Application): EquipmentService =
                INSTANCE ?: synchronized(this) {
                    INSTANCE ?: EquipmentService(application).also { INSTANCE = it }
                }
    }
}
