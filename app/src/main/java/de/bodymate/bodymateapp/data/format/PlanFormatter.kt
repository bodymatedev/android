package de.bodymate.bodymateapp.data.format

import de.bodymate.bodymateapp.data.internentity.Exercise
import de.bodymate.bodymateapp.data.internentity.Plan
import de.bodymate.bodymateapp.data.repository.entity.RoomExercise

object PlanFormatter {

    fun fillPlanWithExercises(plan: Plan, exercises: List<RoomExercise>): Plan {

        for (planPosition in plan.positions) {
            for (roomExercise in exercises) {
                if (roomExercise.id == planPosition.exerciseId) {
                    planPosition.exercise = Exercise.createFromRoomEntity(roomExercise)
                }
            }
        }
        return plan
    }

    fun checkPlan(plan: Plan): Int {

        when {
            plan.generatedTime.trim().isEmpty() -> return 1
            plan.revision < 0 -> return 1
            plan.number < 0 -> return 1
            plan.positions.isEmpty() -> return 1
            else -> {
                for (planPosition in plan.positions) {

                    if (planPosition.sets.isEmpty()) {
                        return 1
                    }
                }
                return 0
            }
        }
    }
}
