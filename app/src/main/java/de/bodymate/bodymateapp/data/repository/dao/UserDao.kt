package de.bodymate.bodymateapp.data.repository.dao

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy.REPLACE
import android.arch.persistence.room.Query
import de.bodymate.bodymateapp.data.repository.entity.RoomUser
import io.reactivex.Single

@Dao
abstract class UserDao {

    @Query("DELETE FROM user")
    abstract fun deleteAll()

    @Insert(onConflict = REPLACE)
    abstract fun insert(roomUser: RoomUser)

    @Query("SELECT * FROM user LIMIT 1")
    abstract fun getFirstUser(): LiveData<RoomUser>

    @Query("SELECT * FROM user LIMIT 1")
    abstract fun getFirstUserAsSingle(): Single<RoomUser>
}