package de.bodymate.bodymateapp.data.repository.dao

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query

import de.bodymate.bodymateapp.data.repository.entity.RoomPlanPosition

import android.arch.persistence.room.OnConflictStrategy.REPLACE

/**
 * Created by Leonard on 26.06.2018.
 */
@Dao
abstract class PlanPositionDao {

    @Insert(onConflict = REPLACE)
    abstract fun insertAll(roomPlanPositions: List<RoomPlanPosition>)

    @Query("SELECT * FROM plan_position WHERE plan_id=:planId")
    abstract fun getAllPlanPositionsOfPlan(planId: Int): LiveData<List<RoomPlanPosition>>

    @Query("DELETE FROM plan_position")
    abstract fun deleteAll()
}
