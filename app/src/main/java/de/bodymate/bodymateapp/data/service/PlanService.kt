package de.bodymate.bodymateapp.data.service

import android.app.Application
import de.bodymate.bodymateapp.api.response.RBPlan
import de.bodymate.bodymateapp.data.internentity.Plan
import de.bodymate.bodymateapp.data.internentity.PlanPosition
import de.bodymate.bodymateapp.data.internentity.PlanSet
import de.bodymate.bodymateapp.data.repository.PlanRepository
import de.bodymate.bodymateapp.data.repository.entity.RoomPlan
import de.bodymate.bodymateapp.data.repository.entity.RoomPlanPosition
import de.bodymate.bodymateapp.data.repository.entity.RoomPlanSet
import de.bodymate.bodymateapp.data.repository.entity.joined.PlanPositionSet
import de.bodymate.bodymateapp.exception.EntityFormatException
import io.reactivex.Single
import java.util.*

class PlanService private constructor(application: Application) {

    private val repository = PlanRepository(application)

    val allPlans: Single<List<Plan>> by lazy {

        repository.allPlans.map {
            joinedPlanFormatToInternalEntities(it) ?: throw EntityFormatException()
        }
    }

    fun getPlanOneAsCor(): RoomPlan? = repository.getPlanOneAsCor()

    fun getPlanTwoAsCor(): RoomPlan? = repository.getPlanTwoAsCor()

    fun getPlanByNumberAsCor(number: Int): RoomPlan? = repository.getPlanByNumberAsCor(number)

    fun hasPlansBesidesFitnesstest(): Boolean = repository.countAsCor() > 0

    fun getById(planId: Int): Single<Plan> =
            repository.getPlanById(planId).map{
                joinedPlanFormatToInternalEntity(it) ?: throw EntityFormatException()
            }

    fun addAll(rbPlans: List<RBPlan>?) {

        if (rbPlans != null) {
            repository.addAll(apiToRoomPlanDataEntites(rbPlans))
        }
    }

    fun updateAll(rbPlans: List<RBPlan>?) {

        if (rbPlans != null) {
            repository.updateAll(apiToRoomPlanDataEntites(rbPlans))
        }
    }

    fun deleteAll() {
        repository.deleteAll()
    }

    companion object {

        @Volatile
        private var INSTANCE: PlanService? = null

        fun getInstance(application: Application): PlanService =
                INSTANCE ?: synchronized(this) {
                    INSTANCE ?: PlanService(application).also { INSTANCE = it }
                }

        private fun joinedPlanFormatToInternalEntity(planPositionSets: List<PlanPositionSet>?): Plan? {

            if( planPositionSets.isNullOrEmpty() ) return null

            val plans = joinedPlanFormatToInternalEntities(planPositionSets) ?: return null

            if (plans.isEmpty()) return null

            if (plans.size > 1) throw EntityFormatException("Did not expect more than one plan to format")

            return plans.first()
        }

        private fun joinedPlanFormatToInternalEntities(planPositionSets: List<PlanPositionSet>?): List<Plan>? {

            if ( planPositionSets.isNullOrEmpty() ) return null

            val plans = mutableListOf<Plan>()
            var lastPlan = -1
            var lastPosition = -1
            var lastSet = -1

            planPositionSets.forEach {

                if (lastPlan != it.id) {

                    plans.add(Plan(it.id, it.generatedTime, it.number, it.revision))
                    lastPlan = it.id
                    lastPosition = -1
                    lastSet = -1
                }

                if (lastPosition != it.position && it.position > 0) {

                    plans.last().positions.add(PlanPosition(it.position, it.exerciseId))
                    lastPosition = it.position
                    lastSet = -1
                }

                if (lastSet != it.set && it.set > 0) {

                    plans.last().positions.last().sets.add(PlanSet(it.set, it.reps))
                    lastSet = it.set
                }
            }

            return plans
        }

        private fun apiToRoomPlanDataEntites(rbPlans: List<RBPlan>): PlanRepository.InsertablePlanData {

            val plans = ArrayList<RoomPlan>()
            val positions = ArrayList<RoomPlanPosition>()
            val sets = ArrayList<RoomPlanSet>()

            for (rbPlan in rbPlans) {

                plans.add(RoomPlan.fromApiEntity(rbPlan))

                if (rbPlan.positions != null) {

                    for (rbPlanPosition in rbPlan.positions) {

                        positions.add(RoomPlanPosition.fromApiEntity(rbPlanPosition, rbPlan.id))

                        if (rbPlanPosition.sets != null) {

                            for (rbPlanSet in rbPlanPosition.sets) {

                                sets.add(RoomPlanSet.fromApiEntity(rbPlanSet, rbPlan.id, rbPlanPosition.position))
                            }
                        }
                    }
                }
            }

            return PlanRepository.InsertablePlanData(plans, positions, sets)
        }
    }
}
