package de.bodymate.bodymateapp.data.repository.entity

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.Ignore
import android.arch.persistence.room.PrimaryKey

import de.bodymate.bodymateapp.api.response.RBExercise

/**
 * Created by Leonard on 26.06.2018.
 */

@Entity(tableName = "exercise")
data class RoomExercise(@field:PrimaryKey val id: Int,
                        val name: String,
                        val description: String,
                        val type: String,
                        @field:ColumnInfo(name = "video_url") val videoUrl: String?) {

    companion object {
        @Ignore
        fun fromApiEntity(rbExercise: RBExercise): RoomExercise {
            return RoomExercise(rbExercise.id, rbExercise.name, rbExercise.description, rbExercise.type, rbExercise.videoUrl)
        }
    }
}