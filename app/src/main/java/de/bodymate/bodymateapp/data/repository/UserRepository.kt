package de.bodymate.bodymateapp.data.repository

import android.app.Application
import android.arch.lifecycle.LiveData
import de.bodymate.bodymateapp.data.repository.dao.UserDao
import de.bodymate.bodymateapp.data.repository.database.BodyMateDatabase
import de.bodymate.bodymateapp.data.repository.entity.RoomUser
import io.reactivex.Single
import org.jetbrains.anko.doAsync

/**
 * Created by Leonard on 08.06.2018.
 */

class UserRepository(application: Application) {

    private val mUserDao: UserDao = BodyMateDatabase.getDatabase(application).userDao()

    val first: LiveData<RoomUser> by lazy {
        mUserDao.getFirstUser()
    }

    val firstAsSingle: Single<RoomUser> by lazy {
        mUserDao.getFirstUserAsSingle()
    }

    fun delete() {

        doAsync { mUserDao.deleteAll() }
    }

    fun setUser(roomUser: RoomUser) {

        doAsync {
            mUserDao.deleteAll()
            mUserDao.insert(roomUser)
        }
    }
}
