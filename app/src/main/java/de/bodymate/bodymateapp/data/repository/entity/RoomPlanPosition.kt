package de.bodymate.bodymateapp.data.repository.entity

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.Ignore

import de.bodymate.bodymateapp.api.response.RBPlanPosition

/**
 * Created by Leonard on 26.06.2018.
 */

@Entity(tableName = "plan_position", primaryKeys = ["plan_id", "position", "exercise_id"])
data class RoomPlanPosition(@field:ColumnInfo(name = "plan_id") val planId: Int,
                            val position: Int,
                            @field:ColumnInfo(name = "exercise_id") val exerciseId: Int) {

    companion object {
        @Ignore
        fun fromApiEntity(rbPlanPosition: RBPlanPosition, planId: Int): RoomPlanPosition {
            return RoomPlanPosition(planId, rbPlanPosition.position, rbPlanPosition.exercise!!.id)
        }
    }
}