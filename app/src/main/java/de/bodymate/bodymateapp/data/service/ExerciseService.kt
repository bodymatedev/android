package de.bodymate.bodymateapp.data.service

import android.app.Application
import de.bodymate.bodymateapp.api.response.RBPlan
import de.bodymate.bodymateapp.data.repository.ExerciseRepository
import de.bodymate.bodymateapp.data.repository.entity.RoomExercise
import io.reactivex.Single

class ExerciseService private constructor(application: Application) {

    private val repository = ExerciseRepository(application)

    val allExercises: Single<List<RoomExercise>> by lazy {
        repository.allExercises
    }

    fun updateAll(rbPlans: List<RBPlan>?) {

        if (rbPlans != null) {
            repository.updateAll(getAllExercisesFromPlans(rbPlans))
        }
    }

    fun addAll(rbPlans: List<RBPlan>?){

        if( !rbPlans.isNullOrEmpty() ){
            repository.addAll(getAllExercisesFromPlans(rbPlans))
        }
    }

    fun deleteAll() {
        repository.deleteAll()
    }

    companion object {

        @Volatile
        private var INSTANCE: ExerciseService? = null

        fun getInstance(application: Application): ExerciseService =
                INSTANCE ?: synchronized(this) {
                    INSTANCE ?: ExerciseService(application).also { INSTANCE = it }
                }

        fun getAllExercisesFromPlans(rbPlans: List<RBPlan>): List<RoomExercise> =
                rbPlans
                    .filterNot { it.positions == null }
                    .flatMap {
                        it.positions!!
                                .filterNot { it.exercise == null }
                                .map { RoomExercise.fromApiEntity(it.exercise!!) } }
                    .distinct()
    }
}
