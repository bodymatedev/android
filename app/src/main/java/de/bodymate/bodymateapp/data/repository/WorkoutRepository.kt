package de.bodymate.bodymateapp.data.repository

import android.app.Application
import android.arch.lifecycle.LiveData
import de.bodymate.bodymateapp.data.repository.dao.WorkoutDao
import de.bodymate.bodymateapp.data.repository.database.BodyMateDatabase
import de.bodymate.bodymateapp.data.repository.entity.RoomWorkout
import io.reactivex.Single
import org.jetbrains.anko.doAsync

class WorkoutRepository(application: Application) {

    private val mWorkoutDao: WorkoutDao

    val latest3: LiveData<List<RoomWorkout>>
        get() = mWorkoutDao.getLatest3()

    val count: LiveData<Int>
        get() = mWorkoutDao.count()

    init {

        val db = BodyMateDatabase.getDatabase(application.baseContext)
        mWorkoutDao = db.workoutDao()
    }

    fun insertAll(roomWorkouts: List<RoomWorkout>){

        doAsync {
            mWorkoutDao.insertAll(roomWorkouts)
        }
    }

    fun insertOne(roomWorkout: RoomWorkout) {

        doAsync {
            mWorkoutDao.insertOne(roomWorkout)
        }
    }

    fun deleteAll() {
        doAsync { mWorkoutDao.deleteAll() }
    }

    fun update(roomWorkout: RoomWorkout) {

        doAsync {
            mWorkoutDao.deleteAll()
            mWorkoutDao.insertOne(roomWorkout)
        }
    }

    fun latest3AsCor(): List<RoomWorkout> = mWorkoutDao.getLatest3AsCor()

    fun countTodays(today: String): Int = mWorkoutDao.countTodays(today)
}
