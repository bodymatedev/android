package de.bodymate.bodymateapp.data.repository.dao

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query

import de.bodymate.bodymateapp.data.repository.entity.RoomEquipment

import android.arch.persistence.room.OnConflictStrategy.REPLACE

/**
 * Created by Leonard on 05.06.2018.
 */

@Dao
abstract class EquipmentDao {

    @Query("SELECT * FROM equipment")
    abstract fun getAllEquipments(): LiveData<List<RoomEquipment>>

    @Insert(onConflict = REPLACE)
    abstract fun insertAll(roomEquipments: List<RoomEquipment>)

    @Query("DELETE FROM equipment")
    abstract fun deleteAll()
}
