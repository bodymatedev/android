package de.bodymate.bodymateapp.data.service

import android.app.Application
import android.arch.lifecycle.LiveData
import de.bodymate.bodymateapp.api.response.RBAchievement
import de.bodymate.bodymateapp.data.repository.AchievementRepository
import de.bodymate.bodymateapp.data.repository.entity.RoomAchievement

class AchievementService private constructor(application: Application){

    private val repository = AchievementRepository(application)

    val allAchievements: LiveData<List<RoomAchievement>> by lazy {
        repository.allAchievements
    }

    fun updateAll(rbAchievements: List<RBAchievement>?){

        if( rbAchievements != null ){
            repository.updateAll(rbAchievements.map { RoomAchievement.createFromApiEntity(it) })
        }
    }

    fun deleteAll() {
        repository.deleteAll()
    }

    companion object {

        @Volatile
        private var INSTANCE: AchievementService? = null

        fun getInstance(application: Application): AchievementService =
                INSTANCE ?: synchronized(this) {
                    INSTANCE ?: AchievementService(application).also { INSTANCE = it }
                }
    }
}