package de.bodymate.bodymateapp.data.repository.dao

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query

import de.bodymate.bodymateapp.data.repository.entity.RoomExercise
import io.reactivex.Single

/**
 * Created by Leonard on 26.06.2018.
 */
@Dao
abstract class ExerciseDao {

    @Query("SELECT * FROM exercise")
    abstract fun getAllExercises(): Single<List<RoomExercise>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertAll(roomExercise: List<RoomExercise>)

    @Query("DELETE FROM exercise")
    abstract fun deleteAll()

    @Query("SELECT * FROM exercise WHERE id IN (:ids)")
    abstract fun getAllIn(ids: Set<Int>): LiveData<List<RoomExercise>>
}
