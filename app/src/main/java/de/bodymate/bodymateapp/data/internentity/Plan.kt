package de.bodymate.bodymateapp.data.internentity

import android.content.Context
import de.bodymate.bodymateapp.R
import de.bodymate.bodymateapp.exception.MissingExerciseException
import de.bodymate.bodymateapp.exception.MissingPositionException
import de.bodymate.bodymateapp.exception.MissingSetException
import de.bodymate.bodymateapp.util.C.PLAN_ID_FITNESSTEST
import de.bodymate.bodymateapp.util.C.PLAN_ONE
import de.bodymate.bodymateapp.util.C.PLAN_TWO
import de.bodymate.bodymateapp.util.Util.text

/**
 * Created by Leonard on 28.06.2018.
 */

class Plan(var id: Int , var generatedTime: String, var number: Int, var revision: Int) {

    val positions = mutableListOf<PlanPosition>()

    fun getDisplayName(context: Context): String {

        return if (id == PLAN_ID_FITNESSTEST) text(context, R.string.fitnesstest) else text(context, R.string.plan) + " " + number.toString()
    }

    fun getLongDisplayName(context: Context): String {

        return if (id == PLAN_ID_FITNESSTEST) text(context, R.string.fitnesstest) else
            text(context, R.string.plan) + " " + number.toString() + " - Version " + revision.toString()
    }

    val displayPicture: Int
        get() =
            when(number){
                PLAN_ONE -> R.drawable.placeholder_plan_1
                PLAN_TWO -> R.drawable.placeholder_plan_2
                else -> R.drawable.placeholder_plan_1
            }

    fun positionCount(): Int = positions.size

    fun getPosition(position: Int): PlanPosition {
        try {
            return positions[position]
        } catch (ex: IndexOutOfBoundsException) {
            throw MissingPositionException()
        }
    }

    fun getSetsAt(position: Int): List<PlanSet>? {

        val planPosition = getPosition(position)
        return planPosition.sets
    }

    fun getSetCountAt(position: Int): Int {

        val planPosition = getPosition(position)
        return planPosition.sets.size
    }

    fun getRepsAt(position: Int, set: Int): Int {

        val planPosition = getPosition(position)

        try {
            return planPosition.sets[set].reps
        } catch (ex: IndexOutOfBoundsException) {
            throw MissingSetException()
        }
    }

    fun getExerciseAt(position: Int): Exercise {

        return getPosition(position).exercise
    }
}
