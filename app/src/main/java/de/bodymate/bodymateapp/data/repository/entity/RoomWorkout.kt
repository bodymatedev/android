package de.bodymate.bodymateapp.data.repository.entity

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.Ignore
import android.arch.persistence.room.PrimaryKey
import de.bodymate.bodymateapp.api.response.RBWorkout

@Entity(tableName = "workout")
data class RoomWorkout(@PrimaryKey val id: Int,
                       @ColumnInfo(name = "plan_id") val planId: Int,
                       @ColumnInfo(name = "start") val datetimeStart: String,
                       @ColumnInfo(name = "end") val datetimeEnd: String,
                       val rating: Int,
                       var review: String?,
                       @ColumnInfo(name = "planned_for") val plannedFor: String) {

    companion object {
        @Ignore
        fun fromApiEntity(rbWorkout: RBWorkout): RoomWorkout {
            return RoomWorkout(rbWorkout.id, rbWorkout.planId, rbWorkout.datetimeStart,
                    rbWorkout.datetimeEnd, rbWorkout.rating, rbWorkout.review, rbWorkout.plannedFor)
        }
    }
}