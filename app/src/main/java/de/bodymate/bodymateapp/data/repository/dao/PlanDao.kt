package de.bodymate.bodymateapp.data.repository.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import de.bodymate.bodymateapp.data.repository.entity.RoomPlan
import de.bodymate.bodymateapp.data.repository.entity.joined.PlanPositionSet
import io.reactivex.Single

@Dao
abstract class PlanDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertAll(roomPlans: List<RoomPlan>)

    @Query("DELETE FROM `plan`")
    abstract fun deleteAll()

    @Query("SELECT p.id, p.generated_time, p.number, p.revision, pp.position, pp.exercise_id, ps.`set`, ps.reps FROM `plan` AS p " +
            "LEFT JOIN plan_position AS pp ON p.id = pp.plan_id " +
            "LEFT JOIN plan_set AS ps ON pp.plan_id = ps.plan_id AND pp.position = ps.position " +
            "ORDER BY id, pp.position, ps.`set`")
    abstract fun getAllPlans(): Single<List<PlanPositionSet>>

    @Query("SELECT p.id, p.generated_time, p.number, p.revision, pp.position, pp.exercise_id, ps.`set`, ps.reps FROM `plan` AS p " +
            "LEFT JOIN plan_position AS pp ON p.id = pp.plan_id " +
            "LEFT JOIN plan_set AS ps ON pp.plan_id = ps.plan_id AND pp.position = ps.position " +
            "WHERE p.id = :planId " +
            "ORDER BY id, pp.position, ps.`set`")
    abstract fun getPlanById(planId: Int): Single<List<PlanPositionSet>>

    @Query("SELECT COUNT(*) FROM `plan` WHERE `plan`.id != 1")
    abstract fun countAsCor(): Int

    @Query("SELECT * FROM `plan` WHERE number LIKE 1")
    abstract fun getFlatPlanOneAsCor(): RoomPlan?

    @Query("SELECT * FROM `plan` WHERE number LIKE 2")
    abstract fun getFlatPlanTwoAsCor(): RoomPlan?

    @Query("SELECT * FROM `plan` WHERE number LIKE :number")
    abstract fun getFlatPlanByNumberAsCor(number: Int): RoomPlan?
}