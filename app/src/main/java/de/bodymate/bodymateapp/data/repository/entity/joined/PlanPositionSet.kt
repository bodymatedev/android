package de.bodymate.bodymateapp.data.repository.entity.joined

import android.arch.persistence.room.ColumnInfo

import lombok.EqualsAndHashCode

/**
 * Created by Leonard on 28.06.2018.
 */

data class PlanPositionSet(val id: Int,
                           @ColumnInfo(name = "generated_time") val generatedTime: String,
                           val number: Int,
                           val revision: Int,
                           val position: Int,
                           @ColumnInfo(name = "exercise_id") val exerciseId: Int,
                           val set: Int,
                           val reps: Int)