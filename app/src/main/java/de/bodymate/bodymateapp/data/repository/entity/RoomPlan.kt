package de.bodymate.bodymateapp.data.repository.entity

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.Ignore
import android.arch.persistence.room.PrimaryKey
import de.bodymate.bodymateapp.api.response.RBPlan

/**
 * Created by Leonard on 26.06.2018.
 */

@Entity(tableName = "plan")
data class RoomPlan(@field:PrimaryKey val id: Int,
                    @field:ColumnInfo(name = "generated_time") val generatedTime: String,
                    val number: Int,
                    val revision: Int) {

    companion object {
        @Ignore
        fun fromApiEntity(rbPlan: RBPlan): RoomPlan {
            return RoomPlan(rbPlan.id, rbPlan.generated, rbPlan.number, rbPlan.revision)
        }
    }
}