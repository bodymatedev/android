package de.bodymate.bodymateapp.data.repository.entity

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.Ignore

import de.bodymate.bodymateapp.api.response.RBPlanSet

/**
 * Created by Leonard on 26.06.2018.
 */

@Entity(tableName = "plan_set", primaryKeys = ["plan_id", "position", "set"])
data class RoomPlanSet(@field:ColumnInfo(name = "plan_id") val planId: Int,
                       val position: Int,
                       val set: Int,
                       val reps: Int) {

    companion object {
        @Ignore
        fun fromApiEntity(rbPlanSet: RBPlanSet, planId: Int, position: Int): RoomPlanSet {
            return RoomPlanSet(planId, position, rbPlanSet.set, rbPlanSet.reps)
        }
    }
}