package de.bodymate.bodymateapp.data.repository.dao

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy.REPLACE
import android.arch.persistence.room.Query
import de.bodymate.bodymateapp.data.repository.entity.RoomWorkout
import io.reactivex.Single

@Dao
abstract class WorkoutDao {

    @Query("SELECT COUNT(*) FROM workout")
    abstract fun count(): LiveData<Int>

    @Query("SELECT COUNT(*) FROM workout WHERE planned_for LIKE :today")
    abstract fun countTodays(today: String): Int

    @Query("SELECT * FROM workout ORDER BY planned_for DESC LIMIT 3")
    abstract fun getLatest3(): LiveData<List<RoomWorkout>>

    @Query("SELECT * FROM workout ORDER BY planned_for DESC LIMIT 3")
    abstract fun getLatest3AsCor(): List<RoomWorkout>

    @Query("DELETE FROM workout")
    abstract fun deleteAll()

    @Insert(onConflict = REPLACE)
    abstract fun insertOne(roomWorkout: RoomWorkout)

    @Insert(onConflict = REPLACE)
    abstract fun insertAll(roomWorkouts: List<RoomWorkout>)
}