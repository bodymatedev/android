package de.bodymate.bodymateapp.data.internentity

import de.bodymate.bodymateapp.util.C
import de.bodymate.bodymateapp.data.repository.entity.RoomExercise

data class Exercise(val id: Int, val name: String, val description: String, val type: String, val videoUrl: String?) {

    val isTypeTime: Boolean
        get() = this.type.equals(C.TYPE_TIME, ignoreCase = true)

    val isTypeReps: Boolean
        get() = this.type.equals(C.TYPE_REPS, ignoreCase = true)

    companion object {

        fun createFromRoomEntity(roomExercise: RoomExercise): Exercise {
            return Exercise(roomExercise.id, roomExercise.name, roomExercise.description, roomExercise.type, roomExercise.videoUrl)
        }
    }
}
