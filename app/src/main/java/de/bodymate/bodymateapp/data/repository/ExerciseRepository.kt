package de.bodymate.bodymateapp.data.repository

import android.app.Application
import de.bodymate.bodymateapp.data.repository.dao.ExerciseDao
import de.bodymate.bodymateapp.data.repository.database.BodyMateDatabase
import de.bodymate.bodymateapp.data.repository.entity.RoomExercise
import io.reactivex.Single
import org.jetbrains.anko.doAsync

/**
 * Created by Leonard on 26.06.2018.
 */

class ExerciseRepository(application: Application) {

    private val exerciseDao: ExerciseDao = BodyMateDatabase.getDatabase(application).exerciseDao()

    val allExercises: Single<List<RoomExercise>> by lazy {
        exerciseDao.getAllExercises()
    }

    fun updateAll(roomExercises: List<RoomExercise>) {

        doAsync {
            exerciseDao.deleteAll()
            exerciseDao.insertAll(roomExercises)
        }
    }

    fun addAll(roomExercises: List<RoomExercise>){

        doAsync {
            exerciseDao.insertAll(roomExercises)
        }
    }

    fun deleteAll() {
        doAsync { exerciseDao.deleteAll() }
    }
}
