package de.bodymate.bodymateapp.data.repository.entity

import android.arch.persistence.room.Entity
import android.arch.persistence.room.Ignore
import android.arch.persistence.room.PrimaryKey

import de.bodymate.bodymateapp.api.response.RBSchedule

/**
 * Created by Leonard on 14.06.2018.
 */
@Entity(tableName = "schedule")
data class RoomSchedule(@field:PrimaryKey val weekday: Int,
                        val time: String) {

    companion object {
        @Ignore
        fun createFromApiEntity(rbSchedule: RBSchedule): RoomSchedule {
            return RoomSchedule(rbSchedule.weekday, rbSchedule.time)
        }
    }
}