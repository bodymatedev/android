package de.bodymate.bodymateapp.data.service

import android.app.Application
import android.arch.lifecycle.LiveData
import de.bodymate.bodymateapp.api.response.RBSchedule
import de.bodymate.bodymateapp.data.repository.ScheduleRepository
import de.bodymate.bodymateapp.data.repository.entity.RoomSchedule
import io.reactivex.Single

class ScheduleService private constructor(application: Application) {

    private val repository = ScheduleRepository(application)

    val allSchedules: LiveData<List<RoomSchedule>> by lazy {
        repository.allSchedules
    }

    val allSchedulesAsSingle: Single<List<RoomSchedule>> by lazy {
        repository.allSchedulesAsSingle
    }

    fun updateAll(rbSchedules: List<RBSchedule>?) {

        if (rbSchedules != null) {
            repository.updateAll(rbSchedules.map { RoomSchedule.createFromApiEntity(it) })
        }
    }

    fun deleteAll() {
        repository.deleteAll()
    }

    fun all(): List<RoomSchedule> = repository.all()

    companion object {

        @Volatile
        private var INSTANCE: ScheduleService? = null

        fun getInstance(application: Application): ScheduleService =
                INSTANCE ?: synchronized(this) {
                    INSTANCE ?: ScheduleService(application).also { INSTANCE = it }
                }
    }
}
