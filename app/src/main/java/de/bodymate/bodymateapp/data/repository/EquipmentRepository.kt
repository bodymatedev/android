package de.bodymate.bodymateapp.data.repository

import android.app.Application
import android.arch.lifecycle.LiveData

import de.bodymate.bodymateapp.data.repository.dao.EquipmentDao
import de.bodymate.bodymateapp.data.repository.database.BodyMateDatabase
import de.bodymate.bodymateapp.data.repository.entity.RoomEquipment
import org.jetbrains.anko.doAsync

/**
 * Created by Leonard on 13.06.2018.
 */
class EquipmentRepository(application: Application) {

    private val mEquipmentDao: EquipmentDao = BodyMateDatabase.getDatabase(application).equipmentDao()

    val allEquipments: LiveData<List<RoomEquipment>> by lazy {
        mEquipmentDao.getAllEquipments()
    }

    fun updateAll(roomEquipments: List<RoomEquipment>) {

        doAsync {
            mEquipmentDao.deleteAll()
            mEquipmentDao.insertAll(roomEquipments)
        }
    }

    fun deleteAll() {
        doAsync { mEquipmentDao.deleteAll() }
    }
}
