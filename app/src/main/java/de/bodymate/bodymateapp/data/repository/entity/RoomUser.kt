package de.bodymate.bodymateapp.data.repository.entity

import android.arch.persistence.room.Entity
import android.arch.persistence.room.Ignore
import android.arch.persistence.room.PrimaryKey
import de.bodymate.bodymateapp.api.response.RBUser

/**
 * Created by Leonard on 05.06.2018.
 */

@Entity(tableName = "user")
data class RoomUser(@PrimaryKey val fireId: String,
                    val email: String,
                    val firstname: String?,
                    val lastname: String?,
                    val birthday: String?,
                    val sex: String?,
                    val goal: Int,
                    val pictureUrl: String?,
                    val workoutsCompleted: Int?,
                    val minutesTrained: Int?){

    companion object {
        @Ignore
        fun createFromApiEntity(rbUser: RBUser) : RoomUser{
            return RoomUser(rbUser.fireId, rbUser.email, rbUser.firstname, rbUser.lastname, rbUser.birthday, rbUser.sex, rbUser.goal, rbUser.pictureUrl, rbUser.workoutAccumulates?.workoutsCompleted, rbUser.workoutAccumulates?.minutesTrained)
        }
    }
}
