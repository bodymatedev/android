package de.bodymate.bodymateapp.data.repository

import android.app.Application
import android.arch.lifecycle.LiveData
import de.bodymate.bodymateapp.data.repository.database.BodyMateDatabase
import de.bodymate.bodymateapp.data.repository.entity.RoomAchievement
import org.jetbrains.anko.doAsync

/**
 * Created by Leonard on 16.06.2018.
 */
class AchievementRepository(application: Application) {

    private val achievementDao = BodyMateDatabase.getDatabase(application).achievementDao()

    val allAchievements: LiveData<List<RoomAchievement>> by lazy {
        achievementDao.getAllAchievements()
    }

    fun updateAll(roomAchievements: List<RoomAchievement>) {

        doAsync {
            achievementDao.deleteAll()
            achievementDao.insertAll(roomAchievements)
        }
    }

    fun deleteAll() {
        doAsync { achievementDao.deleteAll() }
    }
}
