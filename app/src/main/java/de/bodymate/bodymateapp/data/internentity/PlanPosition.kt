package de.bodymate.bodymateapp.data.internentity

/**
 * Created by Leonard on 28.06.2018.
 */

class PlanPosition(val position: Int, val exerciseId: Int) {

    lateinit var exercise: Exercise

    val sets = mutableListOf<PlanSet>()

    val setCount: Int
        get() = this.sets.size
}
