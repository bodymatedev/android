package de.bodymate.bodymateapp.data.repository.entity

import android.arch.persistence.room.Entity
import android.arch.persistence.room.Ignore
import android.arch.persistence.room.PrimaryKey
import de.bodymate.bodymateapp.api.response.RBAchievement

/**
 * Created by Leonard on 16.06.2018.
 */

@Entity(tableName = "achievement")
data class RoomAchievement(@field:PrimaryKey val id: Int,
                           val name: String?,
                           val description: String?,
                           val preserved: String?) {

    companion object {
        @Ignore
        fun createFromApiEntity(rbAchievement: RBAchievement): RoomAchievement {
            return RoomAchievement(rbAchievement.id, rbAchievement.name,
                    rbAchievement.description, rbAchievement.date)
        }
    }
}