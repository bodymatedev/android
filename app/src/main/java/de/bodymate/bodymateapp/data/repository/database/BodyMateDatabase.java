package de.bodymate.bodymateapp.data.repository.database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import de.bodymate.bodymateapp.data.repository.dao.AchievementDao;
import de.bodymate.bodymateapp.data.repository.dao.EquipmentDao;
import de.bodymate.bodymateapp.data.repository.dao.ExerciseDao;
import de.bodymate.bodymateapp.data.repository.dao.PlanDao;
import de.bodymate.bodymateapp.data.repository.dao.PlanPositionDao;
import de.bodymate.bodymateapp.data.repository.dao.PlanSetDao;
import de.bodymate.bodymateapp.data.repository.dao.ScheduleDao;
import de.bodymate.bodymateapp.data.repository.dao.UserDao;
import de.bodymate.bodymateapp.data.repository.dao.WorkoutDao;
import de.bodymate.bodymateapp.data.repository.entity.RoomAchievement;
import de.bodymate.bodymateapp.data.repository.entity.RoomEquipment;
import de.bodymate.bodymateapp.data.repository.entity.RoomExercise;
import de.bodymate.bodymateapp.data.repository.entity.RoomPlan;
import de.bodymate.bodymateapp.data.repository.entity.RoomPlanPosition;
import de.bodymate.bodymateapp.data.repository.entity.RoomPlanSet;
import de.bodymate.bodymateapp.data.repository.entity.RoomSchedule;
import de.bodymate.bodymateapp.data.repository.entity.RoomUser;
import de.bodymate.bodymateapp.data.repository.entity.RoomWorkout;

/**
 * Created by Leonard on 05.06.2018.
 */

@Database(entities = {RoomUser.class,
					  RoomEquipment.class,
					  RoomSchedule.class,
		              RoomAchievement.class,
		              RoomExercise.class,
					  RoomPlan.class,
					  RoomPlanPosition.class,
					  RoomPlanSet.class,
		              RoomWorkout.class},
		  version = 2)

public abstract class BodyMateDatabase extends RoomDatabase{

	public abstract UserDao userDao();
	public abstract EquipmentDao equipmentDao();
	public abstract ScheduleDao scheduleDao();
	public abstract AchievementDao achievementDao();
	public abstract ExerciseDao exerciseDao();
	public abstract PlanDao planDao();
	public abstract PlanPositionDao planPositionDao();
	public abstract PlanSetDao planSetDao();
	public abstract WorkoutDao workoutDao();

	private static BodyMateDatabase INSTANCE;

	public static BodyMateDatabase getDatabase(final Context context){
		if( INSTANCE == null ){
			synchronized( BodyMateDatabase.class ){
				if( INSTANCE == null ){
					INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
							BodyMateDatabase.class, "bodymate_database")
							.fallbackToDestructiveMigration()
							.build();
				}
			}
		}
		return INSTANCE;
	}
}
