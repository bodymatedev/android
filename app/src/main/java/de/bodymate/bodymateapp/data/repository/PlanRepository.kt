package de.bodymate.bodymateapp.data.repository

import android.app.Application

import de.bodymate.bodymateapp.data.repository.dao.PlanDao
import de.bodymate.bodymateapp.data.repository.dao.PlanPositionDao
import de.bodymate.bodymateapp.data.repository.dao.PlanSetDao
import de.bodymate.bodymateapp.data.repository.database.BodyMateDatabase
import de.bodymate.bodymateapp.data.repository.entity.RoomPlan
import de.bodymate.bodymateapp.data.repository.entity.RoomPlanPosition
import de.bodymate.bodymateapp.data.repository.entity.RoomPlanSet
import de.bodymate.bodymateapp.data.repository.entity.joined.PlanPositionSet
import io.reactivex.Single
import org.jetbrains.anko.doAsync

/**
 * Created by Leonard on 26.06.2018.
 */
class PlanRepository(application: Application) {

    private val database = BodyMateDatabase.getDatabase(application)

    private val planDao: PlanDao = database.planDao()
    private val planPositionDao: PlanPositionDao = database.planPositionDao()
    private val planSetDao: PlanSetDao = database.planSetDao()

    val allPlans: Single<List<PlanPositionSet>> by lazy {
        planDao.getAllPlans()
    }

    fun countAsCor(): Int = planDao.countAsCor()

    fun getPlanOneAsCor(): RoomPlan? = planDao.getFlatPlanOneAsCor()

    fun getPlanTwoAsCor(): RoomPlan? = planDao.getFlatPlanTwoAsCor()

    fun getPlanByNumberAsCor(number: Int): RoomPlan? = planDao.getFlatPlanByNumberAsCor(number)

    fun getPlanById(planId: Int): Single<List<PlanPositionSet>> =
            planDao.getPlanById(planId)

    fun updateAll(insertablePlanData: InsertablePlanData) {

        doAsync {

            planSetDao.deleteAll()
            planPositionDao.deleteAll()
            planDao.deleteAll()

            planDao.insertAll(insertablePlanData.plans)
            planPositionDao.insertAll(insertablePlanData.positions)
            planSetDao.insertAll(insertablePlanData.sets)
        }
    }

    fun addAll(insertablePlanData: InsertablePlanData) {

        doAsync {

            planDao.insertAll(insertablePlanData.plans)
            planPositionDao.insertAll(insertablePlanData.positions)
            planSetDao.insertAll(insertablePlanData.sets)
        }
    }

    fun deleteAll() {

        doAsync {
            planSetDao.deleteAll()
            planPositionDao.deleteAll()
            planDao.deleteAll()
        }
    }

    data class InsertablePlanData(val plans: List<RoomPlan>, val positions: List<RoomPlanPosition>, val sets: List<RoomPlanSet>)
}
