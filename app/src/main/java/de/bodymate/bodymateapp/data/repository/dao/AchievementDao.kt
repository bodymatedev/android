package de.bodymate.bodymateapp.data.repository.dao

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query

import de.bodymate.bodymateapp.data.repository.entity.RoomAchievement

import android.arch.persistence.room.OnConflictStrategy.REPLACE

/**
 * Created by Leonard on 16.06.2018.
 */
@Dao
abstract class AchievementDao {

    @Query("SELECT * FROM achievement")
    abstract fun getAllAchievements(): LiveData<List<RoomAchievement>>

    @Insert(onConflict = REPLACE)
    abstract fun insertAll(roomAchievements: List<RoomAchievement>)

    @Query("DELETE FROM achievement")
    abstract fun deleteAll()
}
