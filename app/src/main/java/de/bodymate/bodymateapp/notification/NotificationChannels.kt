package de.bodymate.bodymateapp.notification

import android.annotation.TargetApi
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.os.Build
import de.bodymate.bodymateapp.R
import de.bodymate.bodymateapp.extensions.color
import de.bodymate.bodymateapp.extensions.string
import de.bodymate.bodymateapp.util.Util

// Notification-channel id's
internal const val NOTIFICATION_CHANNEL_FALLBACK = "NOTIFICATION_CHANNEL_OTHER"
internal const val NOTIFICATION_CHANNEL_PLAN_UPDATES = "NOTIFICATION_CHANNEL_PLAN_UPDATES"
internal const val NOTIFICATION_CHANNEL_WORKOUT_REMINDER = "NOTIFICATION_CHANNEL_WORKOUT_REMINDER"

@TargetApi(Build.VERSION_CODES.O)
fun createWorkoutReminderChannel(context: Context): NotificationChannel{

    val channelWorkoutReminder =  NotificationChannel(NOTIFICATION_CHANNEL_WORKOUT_REMINDER,
            context.getString(R.string.notification_channel_workout_reminder_name),
            NotificationManager.IMPORTANCE_DEFAULT).apply {

        description = context.getString(R.string.notification_channel_workout_reminder_description)
        enableLights(true)
        lightColor = Util.colorPrimary(context)
        enableVibration(true)
    }

    (context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager)
            .createNotificationChannel(channelWorkoutReminder)

    return channelWorkoutReminder
}
@TargetApi(Build.VERSION_CODES.O)
fun createPlanUpdatesChannel(context: Context): NotificationChannel{

    // Create channel for plan updates
    val channelPlanUpdates = NotificationChannel(NOTIFICATION_CHANNEL_PLAN_UPDATES,
            context.string(R.string.notification_channel_plan_updates_name),
            NotificationManager.IMPORTANCE_DEFAULT).apply {

        description = context.string(R.string.notification_channel_plan_updates_description)
        enableLights(true)
        lightColor = context.color(R.color.colorPrimary)
        enableVibration(true)
    }

    (context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager)
            .createNotificationChannel(channelPlanUpdates)

    return channelPlanUpdates
}

@TargetApi(Build.VERSION_CODES.O)
fun createFallbackChannel(context: Context): NotificationChannel{


    val channelFallback = NotificationChannel(NOTIFICATION_CHANNEL_FALLBACK,
            context.string(R.string.notification_channel_fallback_name),
            NotificationManager.IMPORTANCE_DEFAULT).apply {

        description = context.string(R.string.notification_channel_fallback_description)
        enableLights(true)
        lightColor = context.color(R.color.colorPrimary)
        enableVibration(true)
    }

    (context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager)
            .createNotificationChannel(channelFallback)

    return channelFallback
}