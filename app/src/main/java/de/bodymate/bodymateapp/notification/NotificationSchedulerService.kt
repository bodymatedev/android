package de.bodymate.bodymateapp.notification

import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.util.Log
import de.bodymate.bodymateapp.CurrentUser
import de.bodymate.bodymateapp.api.response.RBSchedule
import de.bodymate.bodymateapp.data.repository.entity.RoomSchedule
import de.bodymate.bodymateapp.notification.WorkoutReminderPublisher.Companion.EXTRA_FIRE_ID
import de.bodymate.bodymateapp.notification.WorkoutReminderPublisher.Companion.EXTRA_WEEKDAY
import de.bodymate.bodymateapp.util.Tags.NOTIFICATION
import de.bodymate.bodymateapp.util.dateTimeNextWeekday
import de.bodymate.bodymateapp.util.time
import de.bodymate.bodymateapp.util.timeInMillis

object NotificationSchedulerService {

    const val REMIND_HOURS_BEFORE = 4

    /**
     * Set alarms for any weekday, the user wants to train
     * @param context the calling context
     * @param schedules A list of RBSchedule objects (can be null)
     */
    fun scheduleWorkoutReminderNotifications(context: Context, schedules: List<RBSchedule>?){

        val alarmManager = context.getSystemService(Context.ALARM_SERVICE) as AlarmManager

        if( schedules.isNullOrEmpty() ) return

        val alarmIntent = Intent(context, WorkoutReminderPublisher::class.java)

        // Provide the alarm-intent with the current logged in user-id
        alarmIntent.putExtra(EXTRA_FIRE_ID, CurrentUser.FIRE_ID)

        schedules.forEach {

            alarmIntent.putExtra(EXTRA_WEEKDAY, it.weekday)

            val dateTimeStart = dateTimeNextWeekday(it.weekday, time(it)?.minusHours(REMIND_HOURS_BEFORE)) ?: return@forEach

            val requestCode = getRequestCode(CurrentUser.FIRE_ID!!, it.weekday)

            //Check if the same alarms for the user and weekday already exists
            val alarmAlreadySet = PendingIntent.getBroadcast(context, requestCode, alarmIntent, PendingIntent.FLAG_NO_CREATE) != null

            if( !alarmAlreadySet ){

                val pendingIntent = PendingIntent.getBroadcast(context, requestCode, alarmIntent, PendingIntent.FLAG_CANCEL_CURRENT)

                val interval = AlarmManager.INTERVAL_DAY * 7

                alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, dateTimeStart.timeInMillis(), interval, pendingIntent)

                Log.d(NOTIFICATION, "Planned notification for weekday ${it.weekday}")
            }
        }
    }


    /**
     * @param schedules A list of the user's schedules
     */
    fun cancelAllWorkoutReminderNotification(context: Context, schedules: List<RoomSchedule>){

        if( schedules.isEmpty() ) return

        val fireId = CurrentUser.FIRE_ID ?: return

        Log.d(NOTIFICATION, "Cancelling all (${schedules.size}) workout-reminder-alarms for user $fireId ...")

        val alarmManager = context.getSystemService(Context.ALARM_SERVICE) as AlarmManager

        val alarmIntentReplica = Intent(context, WorkoutReminderPublisher::class.java)
        alarmIntentReplica.putExtra(EXTRA_FIRE_ID, CurrentUser.FIRE_ID)

        schedules.forEach {

            alarmIntentReplica.putExtra(EXTRA_WEEKDAY, it.weekday)

            val pendingIntent = PendingIntent.getBroadcast(context, getRequestCode(fireId, it.weekday), alarmIntentReplica, PendingIntent.FLAG_CANCEL_CURRENT)
            alarmManager.cancel(pendingIntent)
            pendingIntent.cancel()

            Log.d(NOTIFICATION, "Cancelled workout-reminder-alarm for weekday ${it.weekday}")
        }
    }

    /**
     * @param context The context, that the BroadcastReceiver was called with
     * @param loggedOutFireId The Fire id, page the old alarm, that is no longer logged in
     */
    fun cancelWorkoutReminderNotification(context: Context, loggedOutFireId: String, weekday: Int){

        Log.d(NOTIFICATION, "Cancelling alarm for logged out user: $loggedOutFireId on weekday: $weekday")

        val alarmManager = context.getSystemService(Context.ALARM_SERVICE) as AlarmManager

        val alarmIntent = Intent(context, WorkoutReminderPublisher::class.java)
        alarmIntent.putExtra(EXTRA_FIRE_ID, loggedOutFireId)
        alarmIntent.putExtra(EXTRA_WEEKDAY, weekday)

        val pendingIntent = PendingIntent.getBroadcast(context, getRequestCode(loggedOutFireId, weekday), alarmIntent, PendingIntent.FLAG_CANCEL_CURRENT)
        alarmManager.cancel(pendingIntent)
        pendingIntent.cancel()
    }

    private fun getRequestCode(fireId: String, weekday: Int): Int
         = fireId.plus(weekday.toString()).hashCode()
}