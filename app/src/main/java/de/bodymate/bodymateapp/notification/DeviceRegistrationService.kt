package de.bodymate.bodymateapp.notification

import android.util.Log
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.messaging.FirebaseMessaging
import de.bodymate.bodymateapp.api.ApiFunctions
import de.bodymate.bodymateapp.api.ApiUtils
import de.bodymate.bodymateapp.api.requestbody.RequestBodyRegisterDevice

object DeviceRegistrationService {

    private const val CREATED = "CREATED"
    private const val TOPIC_PLAN_UPDATES = "planUpdates"
    private const val LOG_TAG_FIREBASE = "FIREBASE_LOG"

    fun registerDeviceForUser(firebaseIdToken:String){

        // Receive the firebase device registration id
        FirebaseInstanceId.getInstance().instanceId.addOnCompleteListener {

            // Add or update the device id and token on the backend
            ApiFunctions.registerDevice(RequestBodyRegisterDevice(firebaseIdToken, it.result!!.id, it.result!!.token),

                    onResponse = { rbRegisterDevice ->

                        // Only register this device to the topic once
                        if( rbRegisterDevice != null && rbRegisterDevice.result == CREATED){

                            FirebaseMessaging.getInstance().subscribeToTopic(TOPIC_PLAN_UPDATES).addOnCompleteListener { topicTask ->

                                // Check if subscription failed, and log it if so
                                if( topicTask.isSuccessful.not() ){
                                    Log.d(LOG_TAG_FIREBASE, "Failed to subscribe to topic: $TOPIC_PLAN_UPDATES")
                                }
                            }
                        }
                    },

                    onError = { volleyError -> ApiUtils.logError(volleyError) })
        }
    }
}