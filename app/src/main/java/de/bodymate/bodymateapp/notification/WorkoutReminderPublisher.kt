package de.bodymate.bodymateapp.notification

import android.annotation.SuppressLint
import android.app.Application
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.Build
import android.support.v4.app.NotificationCompat
import android.util.Log
import com.google.firebase.auth.FirebaseAuth
import de.bodymate.bodymateapp.R
import de.bodymate.bodymateapp.activity.SplashActivity
import de.bodymate.bodymateapp.data.repository.entity.RoomSchedule
import de.bodymate.bodymateapp.data.service.PlanService
import de.bodymate.bodymateapp.data.service.ScheduleService
import de.bodymate.bodymateapp.extensions.getFireId
import de.bodymate.bodymateapp.extensions.string
import de.bodymate.bodymateapp.notification.NotificationSchedulerService.REMIND_HOURS_BEFORE
import de.bodymate.bodymateapp.util.*
import de.bodymate.bodymateapp.util.Tags.NOTIFICATION
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.joda.time.LocalTime

class WorkoutReminderPublisher: BroadcastReceiver() {

    private val firebaseAuth: FirebaseAuth by lazy {
        FirebaseAuth.getInstance()
    }

    override fun onReceive(context: Context, intent: Intent?) {

        Log.d(NOTIFICATION, "Received a notification broadcast")

        if( intent == null ){
            Log.d(NOTIFICATION, "Failed to process alarm, because the intent was null")
            return
        }

        val fireIdPlanner = intent.getStringExtra(EXTRA_FIRE_ID) ?: ""

        if( fireIdPlanner.isEmpty() ){
            Log.d(NOTIFICATION, "Failed to process alarm, because the key 'EXTRA_FIRE_ID' is missing")
            return
        }

        val weekdayPlanner = intent.getIntExtra(EXTRA_WEEKDAY, -1)

        if( weekdayPlanner == -1 ){
            Log.d(NOTIFICATION, "Failed to process alarm, because the key 'EXTRA_WEEKDAY' is missing")
            return
        }

        Log.d(NOTIFICATION, "Received it for user $fireIdPlanner")

        val scheduleService = ScheduleService.getInstance(context.applicationContext as Application)
        val planService = PlanService.getInstance(context.applicationContext as Application)

        firebaseAuth.getFireId { fireId ->

            if( fireId != null ){

                // A user is currently logged in
                // Compare it to the user the alarm intent was intended for
                if( fireId == fireIdPlanner ){

                    //Check if the given weekday does not match the current weekday
                    if( weekdayPlanner != DateUtils.getWeekday() ){
                        Log.d(NOTIFICATION, "Not showing this notification, because it was delayed")
                        //return@getFireId
                    }

                    GlobalScope.launch {

                        //Check if the user has a plan, that is not the fitnesstest
                        if( planService.hasPlansBesidesFitnesstest().not() ){
                            Log.d(NOTIFICATION, "Not showing this notification, because the user did not complete the fitnesstest")
                            return@launch
                        }

                        //Check if this alarm fits to any schedule
                        val relevantSchedule = triggeredBeforeRelevantSchedule(scheduleService.all())

                        if( relevantSchedule == null ){
                            Log.d(NOTIFICATION, "Not showing this notification, because it was delayed")
                            return@launch
                        }

                        //This alarm was triggered in the right time slot ( was not delayed by device )
                        showNotification(context, time(relevantSchedule)!!.toPrint())
                    }

                }else{
                    // Another user is now logged in, cancel pending alarms for the fire id of the received alarm
                    NotificationSchedulerService.cancelWorkoutReminderNotification(context.applicationContext, fireIdPlanner, weekdayPlanner)
                }
            }else{

                // No user is currently logged in, cancel pending alarms for the fire id of the received alarm
                NotificationSchedulerService.cancelWorkoutReminderNotification(context.applicationContext, fireIdPlanner, weekdayPlanner)
            }
        }
    }

    @SuppressLint("StringFormatInvalid")
    private fun showNotification(context: Context, time: String){

        val notificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        @Suppress("DEPRECATION")
        val notificationBuilder: NotificationCompat.Builder =
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
                    NotificationCompat.Builder(context, createWorkoutReminderChannel(context).id)
                else
                    NotificationCompat.Builder(context)

        // Build PendingIntent that's executed on notification click
        val clickIntent = Intent(context, SplashActivity::class.java).apply {
            flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        }

        val clickPendingIntent = PendingIntent.getActivity(context, 0, clickIntent, PendingIntent.FLAG_UPDATE_CURRENT)

        // Build PendingIntent that will be executed if the user clicks the action "Start now"
        val startNowIntent = Intent(context, SplashActivity::class.java).apply {
            flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            putExtra(context.string(R.string.notif_workout_reminder_action_start_now_extra_key), C.TRUE)
            putExtra(context.string(R.string.notif_workout_reminder_action_start_now_date_extra_key), todayAsString())
        }

        val startNowPendingIntent = PendingIntent.getActivity(context, 0, startNowIntent, PendingIntent.FLAG_UPDATE_CURRENT)

        // Build the notification
        notificationBuilder.apply {

            setContentIntent(clickPendingIntent)
            setContentTitle(context.string(R.string.notif_workout_reminder_title))
            setContentText(context.resources.getString(R.string.notif_workout_reminder_body, time))
            setSmallIcon(R.drawable.ic_workout_reminder)
            color = Util.color(context, R.color.colorAccent)
            setAutoCancel(true)

            addAction(
                    NotificationCompat.Action.Builder(0,
                            context.string(R.string.notif_workout_reminder_action_start_now_text),
                            startNowPendingIntent
                    ).build()
            )
        }

        // Show notification
        notificationManager.notify(NOTIFICATION_ID, notificationBuilder.build())
    }

    private fun triggeredBeforeRelevantSchedule(schedules: List<RoomSchedule>): RoomSchedule?{

        val scheduleToday = schedules.find { it.weekday == DateUtils.getWeekday() } ?: return null

        val timeOfWorkout = time(scheduleToday) ?: return null
        val timeOfOptAlarm = timeOfWorkout.minusHours(REMIND_HOURS_BEFORE)

        val timeNow = LocalTime.now()

        return if( (timeOfOptAlarm == timeNow) || (timeOfOptAlarm.isBefore(timeNow) && timeOfWorkout.isAfter(timeNow)) ){
            scheduleToday
        }else{
            null
        }
    }

    companion object {

        // Used for every notification
        const val NOTIFICATION_ID = 9001

        // To check if the fired alarm is for the current logged in user
        const val EXTRA_FIRE_ID = "EXTRA_FIRE_ID"
        const val EXTRA_WEEKDAY = "EXTRA_WEEKDAY"
    }
}