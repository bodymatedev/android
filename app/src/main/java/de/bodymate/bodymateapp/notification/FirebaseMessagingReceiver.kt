package de.bodymate.bodymateapp.notification

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.Build
import android.support.annotation.DrawableRes
import android.util.ArrayMap
import android.util.Log
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import de.bodymate.bodymateapp.R
import de.bodymate.bodymateapp.activity.SplashActivity
import de.bodymate.bodymateapp.api.ApiFunctions
import de.bodymate.bodymateapp.api.ApiUtils
import de.bodymate.bodymateapp.api.requestbody.RequestBodyPlansUpdated
import de.bodymate.bodymateapp.extensions.color
import de.bodymate.bodymateapp.util.DateUtils
import de.bodymate.bodymateapp.util.Tags.NOTIFICATION

/**
 * @author Leonard Palm
 */
class FirebaseMessagingReceiver : FirebaseMessagingService() {

    private val notificationManager: NotificationManager by lazy {
        getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
    }

    private val firebaseAuth: FirebaseAuth by lazy {
        FirebaseAuth.getInstance()
    }

    private lateinit var channelPlanUpdates:NotificationChannel
    private lateinit var channelFallback:NotificationChannel

    override fun onMessageReceived(remoteMessage: RemoteMessage?) {

        if( remoteMessage == null ){
            Log.d(NOTIFICATION, "Received a null-message")
            return
        }

        // Check if message is a data-message
        if( !remoteMessage.data.isEmpty() ) {

            Log.d(NOTIFICATION, "Received a data-message")

            // Has to have a 'reason' value
            val reason: String = remoteMessage.data["reason"] ?: ""

            if( reason.equals(REASON_PLAN_UPDATES, true) ){

                val updated: String = remoteMessage.data["updated"] ?: ""

                onPlanUpdateMessageReceived(updated)
            }else{
                Log.d(NOTIFICATION, "Did not know what to do with this message ('reason'=$reason)")
            }
        }else{
            Log.d(NOTIFICATION, "Received empty message")
        }
    }

    private fun onPlanUpdateMessageReceived(updated: String){

        val updateDate = DateUtils.getDateFromSqlDate(updated)

        if( updateDate == null ){
            Log.d(NOTIFICATION, "Plan update notification: Failed to parse value of 'updated' ($updated)")
            return
        }

        // If this notification was pending more than a week, don't proceed it anymore
        if( DateUtils.daysPastFromNow(updateDate) >= 7 ){
            Log.d(NOTIFICATION, "Plan update notification: Received a message older than a week")
            return
        }

        // Check if a user is logged into the app at this point (even cached)
        if( firebaseAuth.currentUser != null ){

            firebaseAuth.currentUser!!.getIdToken(true).addOnCompleteListener { tokenTask ->

                if( tokenTask.isSuccessful && tokenTask.result?.claims != null ){

                    val userData = tokenTask.result?.claims ?: ArrayMap(0)

                    if( !userData.containsKey("user_id") ){
                        Log.d(NOTIFICATION, "Could not find the fire-id in cached user")
                        return@addOnCompleteListener
                    }

                    val fireId:String = userData["user_id"] as String

                    Log.d(NOTIFICATION, "Firebase user login was successful (Fire-Id: $fireId)")

                    val requestBodyPlansUpdated = RequestBodyPlansUpdated(fireId, updated)

                    // Check on the backend, if this specific user received any
                    // changes to his active allPlans (because this is a broadcast message)
                    ApiFunctions.getPlansUpdated(requestBodyPlansUpdated,

                            onResponse = { rbPlansUpdated ->

                                if( rbPlansUpdated == null ){
                                    ApiUtils.logEmptyResponse()
                                    return@getPlansUpdated
                                }
                                if( rbPlansUpdated.updated ){

                                    // Show the notification, because this user received some plan changes
                                    showNotification(
                                            title = getString(R.string.notif_plan_updates_title),
                                            body = getString(R.string.notif_plan_updates_body),
                                            icon = R.drawable.ic_trainer_selected,
                                            type = NOTIFICATION_TYPE_PLAN_UPDATES)
                                }else{
                                    Log.d(NOTIFICATION, "Not showing plan-update notification, because this user did not received any changes")
                                }
                            },

                            onError = ApiUtils::logError
                    )
                }

            }
        }else{
            Log.d(NOTIFICATION, "Not processing plan-update message, because no logged in user was found")
        }
    }

    /**
     * @param title The header of the notification
     * @param body The main text of the notification
     * @param icon The small icon on the top left corner of the notification
     * @param type (optional) Defines the type of the notification (only effects devices >= Android Oreo)
     */
    private fun showNotification(title:String, body:String, @DrawableRes icon:Int, type:Int = 0) {

        Log.d(NOTIFICATION, "About to show notification: title=$title, body=$body, type=$type")

        // Check for newer or equal Android Oreo
        if( Build.VERSION.SDK_INT >= Build.VERSION_CODES.O ) {
            channelPlanUpdates = createPlanUpdatesChannel(this)
            channelFallback = createFallbackChannel(this)
        }

        val notificationBuilder: Notification.Builder = if( Build.VERSION.SDK_INT >= Build.VERSION_CODES.O ) {

            // For devices with Android Oreo or newer

            val channelId = when( type ) {
                NOTIFICATION_TYPE_PLAN_UPDATES -> channelPlanUpdates.id
                NOTIFICATION_TYPE_FALLBACK -> channelFallback.id
                else -> channelFallback.id
            }

            // Check if notifications are blocked for this channel (Prevent the 'Misc' channel to come into place)
            if( notificationManager.getNotificationChannel(channelId).importance == NotificationManager.IMPORTANCE_NONE ){
                // Do not show the notification
                Log.d(NOTIFICATION, "Notification was blocked by user")
                return
            }

            Notification.Builder(this, channelId)

        }else{

            // For devices below Android Oreo
            @Suppress("DEPRECATION")
            Notification.Builder(this)
        }

        // Build PendingIntent that's executed on notification click
        val clickIntent = Intent(this, SplashActivity::class.java).apply {
            flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        }

        val clickPendingIntent = PendingIntent.getActivity(this, 0, clickIntent, PendingIntent.FLAG_UPDATE_CURRENT)

        notificationBuilder.apply {

            setContentIntent(clickPendingIntent)
            setContentTitle(title)
            setContentText(body)
            setSmallIcon(icon)
            setAutoCancel(true)
            setColor(this@FirebaseMessagingReceiver.color(R.color.colorAccent))
        }

        notificationManager.notify(DEFAULT_NOTIFICATION_ID, notificationBuilder.build())
    }

    companion object {

        // External message topic indicators
        private const val REASON_PLAN_UPDATES = "planUpdates"

        // Internal notification type indicators
        private const val NOTIFICATION_TYPE_FALLBACK = 0
        private const val NOTIFICATION_TYPE_PLAN_UPDATES = 1

        // Used for every notification
        private const val DEFAULT_NOTIFICATION_ID = 9002
    }
}
