package de.bodymate.bodymateapp.activity

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.readystatesoftware.systembartint.SystemBarTintManager
import de.bodymate.bodymateapp.R
import de.bodymate.bodymateapp.extensions.isTrue
import de.bodymate.bodymateapp.extensions.string
import de.bodymate.bodymateapp.fragment.FragmentProfile
import de.bodymate.bodymateapp.fragment.FragmentTrainer
import de.bodymate.bodymateapp.fragment.FragmentTraining
import de.bodymate.bodymateapp.gui.MyFragmentPagerAdapter
import de.bodymate.bodymateapp.gui.popup.PopupFreshman
import de.bodymate.bodymateapp.util.C
import de.bodymate.bodymateapp.util.Util
import de.bodymate.bodymateapp.util.date
import de.bodymate.bodymateapp.util.isToday
import de.bodymate.bodymateapp.viewmodel.ViewModelActivityMain
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*


class MainActivity : AppCompatActivity() {

    private var leaveAppOnClick = false

    private var fragmentTrainer: FragmentTrainer? = null
    private var fragmentTraining: FragmentTraining? = null
    private var fragmentProfile: FragmentProfile? = null

    private lateinit var tabViewList : MutableList<View>
    private lateinit var ivNavigatorList : MutableList<ImageView>
    private lateinit var tvNavigatorList : MutableList<TextView>

    private lateinit var viewModelActivityMain: ViewModelActivityMain
    private lateinit var tintManager: SystemBarTintManager

    override fun onCreate(savedInstanceState: Bundle?) {

        setTheme(R.style.AppTheme)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // create our manager instance after the content view is set
        tintManager = SystemBarTintManager(this)
        // enable status bar tint
        tintManager.isStatusBarTintEnabled = true

        considerShowingFreshmanPopup(intent.getStringExtra(Util.text(this, R.string.bkey_freshman)))

        tabViewList = mutableListOf(tabMainTrainer, tabMainWorkout, tabMainProfile)
        ivNavigatorList = mutableListOf(ivMainTrainer, ivMainWorkout, ivMainProfile)
        tvNavigatorList = mutableListOf(tvMainTrainer, tvMainWorkout, tvMainProfile)

        if (savedInstanceState != null) {
            fragmentTrainer = supportFragmentManager.getFragment(savedInstanceState, Util.text(this, R.string.bkey_frag_trainer)) as FragmentTrainer
            fragmentTraining = supportFragmentManager.getFragment(savedInstanceState, Util.text(this, R.string.bkey_frag_training)) as FragmentTraining
            fragmentProfile = supportFragmentManager.getFragment(savedInstanceState, Util.text(this, R.string.bkey_frag_profile)) as FragmentProfile
        }

        setUpViewPager()

        viewModelActivityMain = ViewModelProviders.of(this).get(ViewModelActivityMain::class.java)

        viewModelActivityMain.selectedTab.observe(this, Observer {

            if( it == null ) return@Observer

            if( it == 2 ){
                tintManager.setStatusBarTintDrawable(getDrawable(R.drawable.background_profile_head))
            }else{
                tintManager.setStatusBarTintColor(Util.color(this, R.color.grey850))
            }

            vpMain.currentItem = it

            for (tvNavigator in tvNavigatorList) tvNavigator.setTextColor(Util.color(this, R.color.grey500))
            for (ivNavigator in ivNavigatorList) ivNavigator.imageTintList = Util.getOneColorStateList(this, R.color.grey500)

            ivNavigatorList[it].imageTintList = Util.getOneColorStateList(this, R.color.colorPrimary)
            tvNavigatorList[it].setTextColor(Util.color(this, R.color.colorPrimary))
        })

        viewModelActivityMain.selectedTab.postValue(1)
    }

    private fun considerShowingFreshmanPopup(freshman: String?){

        if( freshman != null && freshman.isTrue() ){
            clActivityMain.post {
                PopupFreshman(this, clActivityMain).show()
            }
        }
    }

    private fun setUpViewPager() {

        if( fragmentTrainer == null ) fragmentTrainer = FragmentTrainer()

        if( fragmentProfile == null ) fragmentProfile = FragmentProfile()

        if( fragmentTraining == null ){

            fragmentTraining = FragmentTraining()

            // Check if the activity was launched from the workout reminder notification action "Start now"
            if( startWorkoutDirecly() ){

                // Tell the fragment to directly start todays workout
                fragmentTraining!!.arguments = Bundle().apply {
                    putString(string(R.string.notif_workout_reminder_action_start_now_extra_key), C.TRUE)
                }
            }
        }else{

            // If fragment was recreated, clear arguments
            fragmentTraining!!.arguments = Bundle.EMPTY
        }

        val myFragmentPagerAdapter = MyFragmentPagerAdapter(supportFragmentManager, fragmentTrainer!!, fragmentTraining!!, fragmentProfile!!)

        vpMain.adapter = myFragmentPagerAdapter
        vpMain.offscreenPageLimit = myFragmentPagerAdapter.maxOffscreenPageLimit

        tabViewList.forEach {
            it.setOnClickListener { viewModelActivityMain.selectedTab.postValue(it.tag.toString().toInt()) }
        }
    }

    private fun startWorkoutDirecly(): Boolean{

        // Check if the activity was launched through the workout reminder notification action "Start now"
        val startWorkoutNow = intent.getStringExtra(string(R.string.notif_workout_reminder_action_start_now_extra_key))

        if( startWorkoutNow != null && startWorkoutNow == C.TRUE ){

            // Check if this notification action was clicked at the same day, as the notification was published
            val notificationPublishDate = date(intent.getStringExtra(string(R.string.notif_workout_reminder_action_start_now_date_extra_key)) ?: "")

            return ( notificationPublishDate != null && notificationPublishDate.isToday() )
        }

        return false
    }

    override fun onBackPressed() {

        //Leave APP
        if (leaveAppOnClick) {
            super.onBackPressed()
        } else {
            leaveAppOnClick = true
            Util.toastIt(this, R.string.app_cancel)
            Timer().schedule(object : TimerTask() {
                override fun run() {
                    leaveAppOnClick = false
                }
            }, 1000)
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {

        super.onSaveInstanceState(outState)

        supportFragmentManager.putFragment(outState, Util.text(this, R.string.bkey_frag_trainer), fragmentTrainer!!)
        supportFragmentManager.putFragment(outState, Util.text(this, R.string.bkey_frag_training), fragmentTraining!!)
        supportFragmentManager.putFragment(outState, Util.text(this, R.string.bkey_frag_profile), fragmentProfile!!)
    }
}
