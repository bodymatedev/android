package de.bodymate.bodymateapp.activity

import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import android.view.WindowManager
import android.widget.ProgressBar
import android.widget.TextView
import br.com.simplepass.loading_button_lib.customViews.CircularProgressButton
import com.android.volley.Response
import com.android.volley.VolleyError
import com.readystatesoftware.systembartint.SystemBarTintManager
import de.bodymate.bodymateapp.CurrentUser
import de.bodymate.bodymateapp.R
import de.bodymate.bodymateapp.api.ApiFunctions
import de.bodymate.bodymateapp.api.ApiUtils
import de.bodymate.bodymateapp.api.ResponseUnpacker
import de.bodymate.bodymateapp.api.requestbody.RequestBodyWorkoutSave
import de.bodymate.bodymateapp.api.requestbody.RequestBodyWorkoutSaveFitnesstest
import de.bodymate.bodymateapp.api.requestbody.dto.RequestDtoWorkoutPosition
import de.bodymate.bodymateapp.api.requestbody.dto.RequestDtoWorkoutSet
import de.bodymate.bodymateapp.api.response.RBPlan
import de.bodymate.bodymateapp.api.response.RBWorkout
import de.bodymate.bodymateapp.api.response.entity.dto.ResponseBodySaveFitnesstest
import de.bodymate.bodymateapp.data.format.PlanFormatter
import de.bodymate.bodymateapp.data.internentity.Plan
import de.bodymate.bodymateapp.data.service.ExerciseService
import de.bodymate.bodymateapp.data.service.PlanService
import de.bodymate.bodymateapp.data.service.WorkoutService
import de.bodymate.bodymateapp.gui.WorkoutExerciseViewer
import de.bodymate.bodymateapp.gui.popup.PopupWorkoutCancel
import de.bodymate.bodymateapp.util.*
import de.bodymate.bodymateapp.util.Timer
import de.bodymate.bodymateapp.viewmodel.ViewModelActivityWorkout
import io.reactivex.SingleObserver
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import kotlinx.android.synthetic.main.activity_workout.*
import java.util.*

class WorkoutActivity : AppCompatActivity() {

    private val compositeDisposable = CompositeDisposable()

    private var planId: Int = 0
    private var plannedFor: String = ""
    private var plan: Plan? = null

    private var running = true

    private val workoutTimer: Timer by lazy{
        Timer()
    }

    private var dateTimeStart: String? = null

    private var workoutExerciseViewer: WorkoutExerciseViewer? = null
    private var cancelWarningPopup: PopupWorkoutCancel? = null

    private lateinit var mViewModel: ViewModelActivityWorkout

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_workout)
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)

        val tintManager = SystemBarTintManager(this)
        tintManager.isStatusBarTintEnabled = true
        tintManager.setStatusBarTintColor(Util.color(this, R.color.grey1000))

        ivWorkoutResume.setOnClickListener(onClickResume)
        ibWorkoutStopResume.setOnClickListener(onClickStopResume)

        planId = intent.getIntExtra(EXTRA_PLAN_ID, 0)
        plannedFor = intent.getStringExtra(EXTRA_PLAN_PLANNED_FOR) ?: ""

        if (planId == 0 || plannedFor.isEmpty() ) {
            Util.toastIt(this, R.string.workout_start_error)
            finish()
            return
        }

        mViewModel = ViewModelProviders.of(this).get(ViewModelActivityWorkout::class.java)

        mViewModel.getWorkoutData(planId).subscribe(object : SingleObserver<ViewModelActivityWorkout.WorkoutDataModel> {

            override fun onSubscribe(disposable: Disposable) {
                compositeDisposable.add(disposable)
            }

            override fun onSuccess(workoutDataModel: ViewModelActivityWorkout.WorkoutDataModel) {
                onPlanPresent(PlanFormatter.fillPlanWithExercises(workoutDataModel.plan, workoutDataModel.exercises))
            }

            override fun onError(throwable: Throwable) {
                Log.e(Tags.REACTIVE, throwable.message)
            }
        })
    }

    private fun onLoadingError() {

        Util.toastIt(this@WorkoutActivity, R.string.workout_start_error)
        this.finish()
    }

    private var onClickStopResume = View.OnClickListener {

        if (running) {
            pauseWorkout()
        } else {
            resumeWorkout()
        }
    }

    private var onClickResume = View.OnClickListener {
        resumeWorkout()
    }

    private fun onPlanPresent(plan: Plan) {

        //Check plan
        if (PlanFormatter.checkPlan(plan) != 0) {
            onLoadingError()
            return
        }

        this.plan = plan

        //Set plan name
        tvWorkoutPlanName.text = plan.getDisplayName(this@WorkoutActivity)

        //Initialize progress bar
        pbWorkoutProgress.max = plan.positions.size
        pbWorkoutProgress.progress = 0

        //Create workout timer
        workoutTimer.start()
        workoutTimer.tickListener = object : Timer.TickListener {
            override fun onTick(time: String) {
                tvWorkoutTime.text = time
                mViewModel.workoutTime.postValue(time)
            }
        }

        //Set start dateTime
        dateTimeStart = DateUtils.now()

        //Create workout viewer
        workoutExerciseViewer = WorkoutExerciseViewer(this, plan, rvWorkoutExerciseList)

        //Count total set amount
        for (planPosition in plan.positions) {
            mViewModel.standardSetAmount = mViewModel.standardSetAmount + planPosition.setCount
        }
    }

    override fun onBackPressed() {

        if (cancelWarningPopup == null) {
            cancelWarningPopup = PopupWorkoutCancel(this@WorkoutActivity, rvWorkoutExerciseList!!)
            cancelWarningPopup!!.popupListener = object : PopupWorkoutCancel.PopupListener {
                override fun onClose() {
                    workoutTimer.resume()
                }
            }
        }

        cancelWarningPopup!!.show()
        workoutTimer.pause()
    }

    override fun onUserLeaveHint() {
        super.onUserLeaveHint()
        pauseWorkout()
    }

    private fun pauseWorkout() {

        running = false
        ibWorkoutStopResume!!.setImageResource(R.drawable.ic_play)
        workoutTimer.pause()
        rlWorkoutPauseOverlay!!.visibility = View.VISIBLE

        workoutExerciseViewer!!.onWorkoutPaused()
    }

    private fun resumeWorkout() {

        running = true
        ibWorkoutStopResume!!.setImageResource(R.drawable.ic_pause)
        workoutTimer.resume()
        rlWorkoutPauseOverlay!!.visibility = View.GONE

        workoutExerciseViewer!!.onWorkoutResumed()
    }

    fun finishWorkout(bnFinish: CircularProgressButton) {

        bnFinish.startAnimation()

        val positionsLog = workoutExerciseViewer!!.workoutLogger.positionsLog

        val requestDtoWorkoutPositions = ArrayList<RequestDtoWorkoutPosition>()

        //Build request body workout position structure page internal logs
        for (i in 0 until positionsLog.size()) {
            val setLogs = positionsLog.get(positionsLog.keyAt(i))
            val requestDtoWorkoutSets = ArrayList<RequestDtoWorkoutSet>()
            for (j in 0 until setLogs.size()) {
                requestDtoWorkoutSets.add(RequestDtoWorkoutSet(setLogs.keyAt(j), setLogs.get(setLogs.keyAt(j))))
            }
            //Check if allEquipments sets where applied (or completed)
            for (s in 1..plan!!.getSetCountAt(i)) {
                var requestDtoWorkoutSet: RequestDtoWorkoutSet? = null
                for (requestDtoWorkoutSetCompleted in requestDtoWorkoutSets) {
                    if (requestDtoWorkoutSetCompleted.set == s) {
                        //Is schon da
                        requestDtoWorkoutSet = requestDtoWorkoutSetCompleted
                        break
                    }
                }
                if (requestDtoWorkoutSet == null) {
                    requestDtoWorkoutSet = RequestDtoWorkoutSet(s, 0)
                    requestDtoWorkoutSets.add(requestDtoWorkoutSet)
                }
            }
            requestDtoWorkoutPositions.add(RequestDtoWorkoutPosition(positionsLog.keyAt(i), requestDtoWorkoutSets))
        }

        //Determine dateTimeEnd
        val dateTimeEnd = DateUtils.addSecondsToDateTime(dateTimeStart!!, workoutTimer.getElapsedSeconds())

        if (planId != C.PLAN_ID_FITNESSTEST) {

            val requestBodyWorkoutSave = RequestBodyWorkoutSave(
                    CurrentUser.FIRE_ID,
                    planId,
                    dateTimeStart,
                    dateTimeEnd,
                    5,
                    "",
                    plannedFor,
                    requestDtoWorkoutPositions)


            ApiFunctions.saveWorkout(requestBodyWorkoutSave, Response.Listener { response ->
                val rbWorkout = ResponseUnpacker(RBWorkout::class.java).parse(response)

                if (rbWorkout == null) {
                    onSaveFailure(null)
                    return@Listener
                }

                onWorkoutSaved(rbWorkout)
            }, Response.ErrorListener { error -> onSaveFailure(error) })

        } else {

            val requestBodyWorkoutSaveFitnesstest = RequestBodyWorkoutSaveFitnesstest(
                    CurrentUser.FIRE_ID,
                    dateTimeStart,
                    dateTimeEnd,
                    requestDtoWorkoutPositions)

            ApiFunctions.saveFitnesstest(requestBodyWorkoutSaveFitnesstest, Response.Listener { response ->
                val responseBodySaveFitnesstest = ResponseUnpacker(ResponseBodySaveFitnesstest::class.java).parse(response)

                if (responseBodySaveFitnesstest?.plans == null || responseBodySaveFitnesstest.workout == null) {
                    onSaveFailure(null)
                    return@Listener
                }

                onFitnesstestSaved(responseBodySaveFitnesstest.plans, responseBodySaveFitnesstest.workout)
            }, Response.ErrorListener { error -> onSaveFailure(error) })
        }
    }

    private fun onSaveFailure(volleyError: VolleyError?) {

        if (volleyError != null) {
            Log.e(ApiUtils.LOG_TAG_ERROR, ApiUtils.parseVolleyError(volleyError))
        }
        Util.toastIt(this@WorkoutActivity, R.string.error_workout_save)
        workoutExerciseViewer!!.onBeforeWorkoutFinish()
        finish()
    }

    private fun onWorkoutSaved(rbWorkout: RBWorkout?) {

        if (rbWorkout == null) {
            onSaveFailure(null)
            return
        }

        //Persist new workout
        WorkoutService.getInstance(application).add(rbWorkout)

        workoutExerciseViewer!!.onBeforeWorkoutFinish()
        finish()
    }

    private fun onFitnesstestSaved(plans: List<RBPlan>, rbWorkout: RBWorkout) {

        //Persist newly generated allPlans
        PlanService.getInstance(application).addAll(plans)

        //Persist allEquipments (new) allExercises
        ExerciseService.getInstance(application).addAll(plans)

        //Persist workout
        WorkoutService.getInstance(application).add(rbWorkout)

        workoutExerciseViewer!!.onBeforeWorkoutFinish()
        finish()
    }

    fun getPbWorkoutProgress(): ProgressBar{
        return pbWorkoutProgress
    }

    fun getTvWorkoutTime(): TextView{
        return tvWorkoutTime
    }

    override fun onStop() {

        super.onStop()

        compositeDisposable.clear()
    }

    override fun onDestroy() {

        super.onDestroy()

        workoutTimer.destroy()
    }

    companion object {

        const val EXTRA_PLAN_ID = "EXTRA_PLAN_ID"
        const val EXTRA_PLAN_PLANNED_FOR = "EXTRA_PLAN_PLANNED_FOR"
    }
}
