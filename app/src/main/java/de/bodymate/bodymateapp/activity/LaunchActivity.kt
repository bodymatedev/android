package de.bodymate.bodymateapp.activity

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.Observer
import android.content.Intent
import android.os.Bundle
import android.support.v4.view.ViewPager
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.ImageView
import com.bumptech.glide.Glide
import de.bodymate.bodymateapp.R
import de.bodymate.bodymateapp.fragment.FragmentFunctionOne
import de.bodymate.bodymateapp.fragment.FragmentFunctionThree
import de.bodymate.bodymateapp.fragment.FragmentFunctionTwo
import de.bodymate.bodymateapp.gui.MyFragmentPagerAdapter
import kotlinx.android.synthetic.main.activity_launch.*

class LaunchActivity : AppCompatActivity() {

    private lateinit var dots:List<ImageView>
    private val currentDot:MutableLiveData<Int> = MutableLiveData()

    override fun onCreate(savedInstanceState: Bundle?) {

        setTheme(R.style.AppTheme)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_launch)

        Glide.with(this).asGif().load(R.drawable.placeholder_launch_gif_3).into(ivLaunchGif)

        bnLaunchRegister.setOnClickListener(onClickLetsGo)

        setupViewPager()
    }

    private fun setupViewPager(){

        dots = listOf(ivIndicatorOne, ivIndicatorTwo, ivIndicatorThree)

        val fragmentPagerAdapter = MyFragmentPagerAdapter(supportFragmentManager, FragmentFunctionOne.newInstance(), FragmentFunctionTwo.newInstance(), FragmentFunctionThree.newInstance())

        vpFunctions.adapter = fragmentPagerAdapter
        vpFunctions.offscreenPageLimit = fragmentPagerAdapter.maxOffscreenPageLimit

        vpFunctions.addOnPageChangeListener(object: ViewPager.OnPageChangeListener{
            override fun onPageScrolled(p0: Int, p1: Float, p2: Int) {}
            override fun onPageScrollStateChanged(p0: Int) {}
            override fun onPageSelected(page: Int) { currentDot.postValue(page) }
        })

        currentDot.observe(this, Observer {
            dots.forEach { it.isSelected = false }
            dots[it!!].isSelected = true
        })

        currentDot.postValue(0)
    }

    private val onClickLetsGo = View.OnClickListener {

        val intent = Intent(this, RegisterActivity::class.java)
        startActivity(intent)
        overridePendingTransition(R.anim.slide_in_from_right, R.anim.stay)
    }
}
