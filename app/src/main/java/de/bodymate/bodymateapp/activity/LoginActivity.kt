package de.bodymate.bodymateapp.activity

import android.content.Intent
import android.os.Bundle
import android.os.StrictMode
import android.support.annotation.StringRes
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.util.Patterns
import android.view.View
import com.android.volley.VolleyError
import com.facebook.AccessToken
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.firebase.FirebaseNetworkException
import com.google.firebase.auth.*
import de.bodymate.bodymateapp.CurrentUser
import de.bodymate.bodymateapp.FacebookInterface
import de.bodymate.bodymateapp.GoogleInterface
import de.bodymate.bodymateapp.R
import de.bodymate.bodymateapp.api.ApiFunctions
import de.bodymate.bodymateapp.api.ApiUtils
import de.bodymate.bodymateapp.api.requestbody.RequestBodyLogin
import de.bodymate.bodymateapp.api.response.RBUser
import de.bodymate.bodymateapp.data.service.*
import de.bodymate.bodymateapp.gui.MyTextWatcher
import de.bodymate.bodymateapp.gui.popup.PopupLoadingCredentials
import de.bodymate.bodymateapp.gui.popup.PopupPasswordReset
import de.bodymate.bodymateapp.notification.DeviceRegistrationService
import de.bodymate.bodymateapp.notification.NotificationSchedulerService
import de.bodymate.bodymateapp.util.C
import de.bodymate.bodymateapp.util.DateUtils
import de.bodymate.bodymateapp.util.Util
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.continue_with_email_button.*
import kotlinx.android.synthetic.main.google_facebook_buttons.*

class LoginActivity : AppCompatActivity() {

    private lateinit var mAuth: FirebaseAuth

    private lateinit var facebookInterface: FacebookInterface
    private lateinit var googleInterface: GoogleInterface

    private lateinit var loadingPopup: PopupLoadingCredentials

    private var loading = false
    private var emailInputVisible = false

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        //Enable strict mode for successful facebook requests
        val policy = StrictMode.ThreadPolicy.Builder().permitAll().build()
        StrictMode.setThreadPolicy(policy)

        mAuth = FirebaseAuth.getInstance()
        googleInterface = GoogleInterface.create(this)
        facebookInterface = FacebookInterface.create(this)

        initUi()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

        super.onActivityResult(requestCode, resultCode, data)

        //Call FB intern method only, if 'LOGIN' view is opened
        if (requestCode == FacebookInterface.RC_SIGN_IN) {
            facebookInterface.fbCallbackManager.onActivityResult(requestCode, resultCode, data)
        }

        // Result returned page launching the Intent page GoogleSignInApi.getSignInIntent(...);
        if (requestCode == GoogleInterface.RC_SIGN_IN) {
            googleInterface.onLoginResult(GoogleSignIn.getSignedInAccountFromIntent(data))
        }
    }

    private fun initUi() {

        tvFacebookButton.setText(R.string.loginWithFacebook)
        tvGoogleButton.setText(R.string.loginWithGoogle)
        tvEmailLoginButton.setText(R.string.loginWithEmail)

        bnEmailLogin.setOnClickListener(onClickLoginEmail)
        bnLogin.setOnClickListener(onClickLogin)
        bnFacebook.setOnClickListener(onClickLoginFacebook)
        bnGoogle.setOnClickListener(onClickLoginGoogle)
        tvForgotPassword.setOnClickListener(onClickForgotPassword)

        etLoginEmail.addTextChangedListener(onTextChangeEmail)
        etLoginPassword.addTextChangedListener(onTextChangePassword)

        loadingPopup = PopupLoadingCredentials(this, clEnterActivity)
    }

    private fun destroyUi() {

        bnEmailLogin.setOnClickListener(null)
        bnLogin.setOnClickListener(null)
        bnFacebook.setOnClickListener(null)
        bnGoogle.setOnClickListener(null)
        tvForgotPassword.setOnClickListener(null)

        etLoginEmail.removeTextChangedListener(onTextChangeEmail)
        etLoginPassword.removeTextChangedListener(onTextChangePassword)

        bnLogin.dispose()
    }

    private val onClickLoginEmail = View.OnClickListener { showEmailInput() }

    private val onClickLogin = View.OnClickListener {

        if( !isInputValid()){
            return@OnClickListener
        }

        val email = etLoginEmail.text.toString()
        val password = etLoginPassword.text.toString()

        //Update gui state
        setInProgress()

        //Create and display loading popup
        loadingPopup.show(PopupLoadingCredentials.METHOD_MAIL, PopupLoadingCredentials.USE_LOGIN)

        mAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener(this) { signInTask ->

            when {

                signInTask.isSuccessful -> onFirebaseLoginSuccess(signInTask.result!!.user)

                signInTask.exception != null -> {
                    if (signInTask.exception is FirebaseAuthInvalidCredentialsException || signInTask.exception is FirebaseAuthInvalidUserException) {
                        Util.toastIt(this@LoginActivity, R.string.login_refused)
                    } else {
                        Util.toastIt(this@LoginActivity, signInTask.exception!!.message.toString())
                    }
                    stopInProgress()
                }

                else -> {
                    Util.toastIt(this@LoginActivity, R.string.signin_unknown_error)
                    stopInProgress()
                }
            }
        }
    }

    private val onClickLoginFacebook = View.OnClickListener {

        //Hide the input fields for email login
        hideEmailInput()

        //Update gui state
        setInProgress()

        loadingPopup.show(PopupLoadingCredentials.METHOD_FACEBOOK, PopupLoadingCredentials.USE_LOGIN)

        facebookInterface.loginFacebook(object : FacebookInterface.FacebookLoginCallback {

            override fun onSuccess(accessToken: AccessToken, payload: FacebookInterface.FacebookLoginPayload) {

                val credential = FacebookAuthProvider.getCredential(accessToken.token)

                mAuth.signInWithCredential(credential).addOnCompleteListener(this@LoginActivity) { signInTask ->

                    when {

                        signInTask.isSuccessful -> onFirebaseLoginSuccess(signInTask.result!!.user)

                        signInTask.exception != null -> {

                            if (signInTask.exception is FirebaseAuthInvalidCredentialsException || signInTask.exception is FirebaseAuthInvalidUserException) {
                                Util.toastIt(this@LoginActivity, R.string.login_refused)
                            } else if( signInTask.exception is FirebaseNetworkException ) {
                                Util.toastIt(this@LoginActivity, R.string.error_connection)
                            }else{
                                Util.toastIt(this@LoginActivity, R.string.signin_unknown_error)
                            }
                            stopInProgress()
                        }

                        else -> {
                            Util.toastIt(this@LoginActivity, R.string.signin_unknown_error)
                            stopInProgress()
                        }
                    }
                }
            }

            override fun onFailure(@StringRes textId: Int) {
                Util.toastIt(this@LoginActivity, textId)
                stopInProgress()
            }
        })
    }

    private val onClickLoginGoogle = View.OnClickListener {

        //Hide the input fields for email login
        hideEmailInput()

        //Update gui state
        setInProgress()

        googleInterface.loginGoogle(object : GoogleInterface.GoogleLoginCallback {

            override fun onSuccess(googleAccount: GoogleSignInAccount) {

                loadingPopup.show(PopupLoadingCredentials.METHOD_GOOGLE, PopupLoadingCredentials.USE_LOGIN)

                val credential = GoogleAuthProvider.getCredential(googleAccount.idToken, null)

                mAuth.signInWithCredential(credential).addOnCompleteListener(this@LoginActivity) { signInTask ->

                    when {

                        signInTask.isSuccessful -> onFirebaseLoginSuccess(signInTask.result!!.user)

                        signInTask.exception != null -> {

                            if (signInTask.exception is FirebaseAuthInvalidCredentialsException || signInTask.exception is FirebaseAuthInvalidUserException) {
                                Util.toastIt(this@LoginActivity, R.string.login_refused)
                            } else if( signInTask.exception is FirebaseNetworkException ) {
                                Util.toastIt(this@LoginActivity, R.string.error_connection)
                            }else{
                                Util.toastIt(this@LoginActivity, R.string.signin_unknown_error)
                            }
                            stopInProgress()
                        }

                        else -> {
                            Util.toastIt(this@LoginActivity, R.string.signin_unknown_error)
                            stopInProgress()
                        }
                    }
                }
            }

            override fun onFailure(textId: Int) {
                Util.toastIt(this@LoginActivity, textId)
                stopInProgress()
            }

            override fun onConnectionFailed(textId: Int) {
                Util.toastIt(this@LoginActivity, textId)
                stopInProgress()
            }
        })
    }

    private val onClickForgotPassword = View.OnClickListener {

        val popupPasswordReset = PopupPasswordReset(this, clEnterActivity)
        popupPasswordReset.show()
    }

    private val onTextChangeEmail = MyTextWatcher(object : MyTextWatcher.OnTextChanged {
        override fun onTextChanged(newText: String) {
            tilLoginEmail.error = null
        }
    })

    private val onTextChangePassword = MyTextWatcher(object : MyTextWatcher.OnTextChanged {
        override fun onTextChanged(newText: String) {
            tilLoginPassword.error = null
        }
    })

    private fun showEmailInput(){

        emailInputVisible = true

        bnEmailLogin.visibility = View.GONE
        inclLoginFacebookGoogle.visibility = View.GONE
        inclLoginSeparator.visibility = View.GONE

        tilLoginEmail.visibility = View.VISIBLE
        tilLoginPassword.visibility = View.VISIBLE
        tvForgotPassword.visibility = View.VISIBLE
        bnLogin.visibility = View.VISIBLE
    }

    private fun hideEmailInput() {

        emailInputVisible = false

        bnEmailLogin.visibility = View.VISIBLE
        inclLoginFacebookGoogle.visibility = View.VISIBLE
        inclLoginSeparator.visibility = View.VISIBLE

        tilLoginEmail.visibility = View.GONE
        tilLoginPassword.visibility = View.GONE
        tvForgotPassword.visibility = View.GONE
        bnLogin.visibility = View.GONE

        tilLoginEmail.error = null
        tilLoginPassword.error = null

        etLoginEmail.text.clear()
        etLoginPassword.text.clear()
    }

    private fun onFirebaseLoginSuccess(fireUser: FirebaseUser) {

        //First step: Receive id token of granted login
        fireUser.getIdToken(true).addOnCompleteListener(this) { idTokenTask ->

            if (idTokenTask.isSuccessful) {

                val firebaseToken = idTokenTask.result!!.token!!

                ApiFunctions.loginKt(RequestBodyLogin(firebaseToken),
                        onResponse = { if (it != null) onLoginSuccess(it, firebaseToken) else onLoginFailed() },
                        onError = this::onLoginFailed)

            } else {
                Util.toastIt(this@LoginActivity, R.string.signin_unknown_error)
                stopInProgress()
            }
        }
    }

    private fun onLoginFailed(volleyError: VolleyError? = null){

        if( volleyError != null){
            Log.e(ApiUtils.LOG_TAG_ERROR, ApiUtils.parseVolleyError(volleyError))
        }

        Util.toastIt(this, R.string.signin_unknown_error)
        stopInProgress()
    }

    private fun onLoginSuccess(rbUser: RBUser, firebaseToken: String) {

        if (!rbUser.registrationCompleted.equals(C.TRUE, ignoreCase = true)) {

            //Registration was not yet completed

            val intent = Intent(this@LoginActivity, RegisterDetailActivity::class.java)

            val bundle = Bundle()
            if (rbUser.firstname != null && rbUser.firstname.isNotEmpty()) {
                bundle.putString(getString(R.string.bkey_firstname), rbUser.firstname)
            }
            if (rbUser.lastname != null && rbUser.lastname.isNotEmpty()) {
                bundle.putString(getString(R.string.bkey_lastname), rbUser.lastname)
            }
            if (rbUser.sex != null && (rbUser.sex.equals(C.MALE, ignoreCase = true) || rbUser.sex.equals(C.FEMALE, ignoreCase = true))) {
                bundle.putString(getString(R.string.bkey_gender), rbUser.sex)
            }
            if (rbUser.birthday != null && rbUser.birthday.isNotEmpty()) {
                bundle.putString(getString(R.string.bkey_birthday), DateUtils.sqlToAndroidDate(rbUser.birthday))
            }

            if (!bundle.isEmpty) {
                intent.putExtras(bundle)
            }

            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)
            finish()

            //With no special animation
            overridePendingTransition(0, 0)

            return
        }

        // Add or update the device id and token on the backend
        DeviceRegistrationService.registerDeviceForUser(firebaseToken)

        //Persist Exercises
        ExerciseService.getInstance(application).updateAll(rbUser.plans)

        //Persist Plans
        PlanService.getInstance(application).updateAll(rbUser.plans)

        //Persist Workouts
        WorkoutService.getInstance(application).update(rbUser.latestWorkout)

        //Persist Schedules
        ScheduleService.getInstance(application).updateAll(rbUser.schedules)

        //Persist Achievements
        AchievementService.getInstance(application).updateAll(rbUser.achievements)

        //Persist Equipments
        EquipmentService.getInstance(application).updateAll(rbUser.equipments)

        //Persist user
        UserService.getInstance(application).update(rbUser)
        CurrentUser.FIRE_ID = rbUser.fireId

        //Schedule workout notifications
        NotificationSchedulerService.scheduleWorkoutReminderNotifications(applicationContext, rbUser.schedules)

        enterMain()
    }

    private fun enterMain() {

        //Enter the MainActivity
        val intent = Intent(this, MainActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
        startActivity(intent)
        finish()

        //With no special animation
        overridePendingTransition(0, 0)
    }

    private fun isInputValid(): Boolean {

        return if (etLoginEmail.text.toString().trim().isEmpty()) {
            tilLoginEmail.error = getString(R.string.error_mandatory_field)
            false
        } else if (etLoginPassword.text.toString().trim().isEmpty()) {
            tilLoginPassword.error = getString(R.string.error_mandatory_field)
            false
        } else if (!Patterns.EMAIL_ADDRESS.matcher(etLoginEmail.text).matches()) {
            tilLoginEmail.error = getString(R.string.error_email_wrong_pattern)
            etLoginEmail.requestFocus()
            false
        } else {
            true
        }
    }

    private fun setInProgress() {

        //Update state
        loading = true
    }

    fun stopInProgress() {

        //Update state
        loading = false

        loadingPopup.close()
    }

    override fun onBackPressed() {

        if( loading ) return

        if( emailInputVisible ){

            hideEmailInput()
            return
        }

        super.onBackPressed()

        //Slide activity away to right
        overridePendingTransition(R.anim.stay, R.anim.slide_out_to_right)
    }

    override fun onDestroy() {

        super.onDestroy()

        loadingPopup.close()

        destroyUi()
    }
}
