package de.bodymate.bodymateapp.activity

import android.content.Intent
import android.os.Bundle
import android.os.StrictMode
import android.support.v7.app.AppCompatActivity
import android.text.method.LinkMovementMethod
import android.util.Log
import android.util.Patterns
import android.view.View
import com.android.volley.Response
import com.facebook.AccessToken
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.firebase.FirebaseNetworkException
import com.google.firebase.auth.*
import de.bodymate.bodymateapp.util.C
import de.bodymate.bodymateapp.FacebookInterface
import de.bodymate.bodymateapp.GoogleInterface
import de.bodymate.bodymateapp.R
import de.bodymate.bodymateapp.api.ApiFunctions
import de.bodymate.bodymateapp.api.ApiUtils
import de.bodymate.bodymateapp.api.ResponseUnpacker
import de.bodymate.bodymateapp.api.requestbody.RequestBodyRegister
import de.bodymate.bodymateapp.api.response.RBUser
import de.bodymate.bodymateapp.gui.MyTextWatcher
import de.bodymate.bodymateapp.gui.popup.PopupLoadingCredentials
import de.bodymate.bodymateapp.notification.DeviceRegistrationService
import de.bodymate.bodymateapp.util.DateUtils
import de.bodymate.bodymateapp.util.Util
import kotlinx.android.synthetic.main.activity_register.*
import kotlinx.android.synthetic.main.continue_with_email_button.*
import kotlinx.android.synthetic.main.google_facebook_buttons.*

class RegisterActivity : AppCompatActivity() {

    private lateinit var mAuth: FirebaseAuth
    private lateinit var facebookInterface: FacebookInterface
    private lateinit var googleInterface: GoogleInterface

    private var loading = false
    private var emailInputVisible = false

    private lateinit var loadingPopup: PopupLoadingCredentials

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        //Enable strict mode for successful facebook requests
        val policy = StrictMode.ThreadPolicy.Builder().permitAll().build()
        StrictMode.setThreadPolicy(policy)

        mAuth = FirebaseAuth.getInstance()
        facebookInterface = FacebookInterface.create(this)
        googleInterface = GoogleInterface.create(this)

        initUi()

        loadingPopup = PopupLoadingCredentials(this, flRegisterMain)
    }

    private fun initUi() {

        tvGoogleButton.setText(R.string.registerWithGoogle)
        tvFacebookButton.setText(R.string.registerWithFacebook)
        tvEmailLoginButton.setText(R.string.registerWithEmail)

        bnEmailRegister.setOnClickListener(onClickEmailRegistration)
        bnRegister.setOnClickListener(onClickRegisterEmail)
        bnGoogle.setOnClickListener(onClickRegisterGoogle)
        bnFacebook.setOnClickListener(onClickRegisterFacebook)
        tvAccountPresent.setOnClickListener(onClickLogin)
        tvLoginHere.setOnClickListener(onClickLogin)

        tvRegisterAgbs.movementMethod = LinkMovementMethod.getInstance()

        //Apply On-Text-Change listeners to remove error when user types again
        etRegisterEmail.addTextChangedListener(onTextChangeEmail)
        etRegisterPassword.addTextChangedListener(onTextChangePassword)
    }

    private fun destroyUi(){

        bnEmailRegister.setOnClickListener(null)
        bnRegister.setOnClickListener(null)
        bnGoogle.setOnClickListener(null)
        bnFacebook.setOnClickListener(null)
        tvAccountPresent.setOnClickListener(null)
        tvLoginHere.setOnClickListener(null)

        etRegisterEmail.removeTextChangedListener(onTextChangeEmail)
        etRegisterPassword.removeTextChangedListener(onTextChangePassword)

        bnRegister.dispose()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

        super.onActivityResult(requestCode, resultCode, data)

        //Call FB intern method only, if 'LOGIN' view is opened
        if (requestCode == FacebookInterface.RC_SIGN_IN) {
            facebookInterface.fbCallbackManager.onActivityResult(requestCode, resultCode, data)
        }

        // Result returned page launching the Intent page GoogleSignInApi.getSignInIntent(...);
        if (requestCode == GoogleInterface.RC_SIGN_IN) {
            googleInterface.onLoginResult(GoogleSignIn.getSignedInAccountFromIntent(data))
        }
    }

    private val onClickEmailRegistration = View.OnClickListener { showEmailInput() }

    private val onClickRegisterEmail = View.OnClickListener {

        if (!inputIsValid()) {
            return@OnClickListener
        }

        //Update gui state
        setInProgress()

        //Create and open loading popup
        loadingPopup.show(PopupLoadingCredentials.METHOD_MAIL, PopupLoadingCredentials.USE_REGISTER)

        //Retrieve input values
        val email = etRegisterEmail.text.toString().trim()
        val password = etRegisterPassword.text.toString().trim()

        //First step: Firebase registration
        mAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(this) { signUpTask ->

            when {

                signUpTask.isSuccessful ->

                    //Second step: Receive an id-token for the newly created user
                    signUpTask.result!!.user.getIdToken(true).addOnCompleteListener(this@RegisterActivity) { tokenTask ->

                        when {
                            tokenTask.isSuccessful && tokenTask.result!!.token != null -> {

                                val firebaseToken = tokenTask.result!!.token!!

                                //Build request body for api register request
                                val requestBodyRegister = RequestBodyRegister(firebaseToken, null, null, null, null, "")

                                //Complete registration by own backend registration
                                onFirebaseRegistrationSuccess(signUpTask.result!!, firebaseToken, requestBodyRegister)

                            }
                            tokenTask.exception != null -> {

                                if( tokenTask.exception is FirebaseNetworkException ) {
                                    Util.toastIt(this@RegisterActivity, R.string.error_connection)
                                }else{
                                    Util.toastIt(this@RegisterActivity, R.string.signup_unknown_error)
                                }
                                stopInProgress()
                            }
                            else -> {
                                Util.toastIt(this@RegisterActivity, R.string.signup_unknown_error)
                                stopInProgress()
                            }
                        }
                    }

                signUpTask.exception != null -> {

                    when {
                        signUpTask.exception is FirebaseAuthUserCollisionException -> Util.toastIt(this@RegisterActivity, R.string.reg_error_email_duplicate)
                        signUpTask is FirebaseNetworkException -> Util.toastIt(this@RegisterActivity, R.string.error_connection)
                        else -> Util.toastIt(this@RegisterActivity, R.string.signup_unknown_error)
                    }
                    stopInProgress()
                }

                else -> {
                    Util.toastIt(this@RegisterActivity, R.string.signup_unknown_error)
                    stopInProgress()
                }
            }
        }
    }

    private val onClickRegisterGoogle = View.OnClickListener {

        //Hide the input fields for email registration
        hideEmailInput()

        //Update gui state
        setInProgress()

        //First step: Login to google
        googleInterface.loginGoogle(object : GoogleInterface.GoogleLoginCallback {

            override fun onSuccess(googleAccount: GoogleSignInAccount) {

                //Show loading popup
                loadingPopup.show(PopupLoadingCredentials.METHOD_GOOGLE, PopupLoadingCredentials.USE_REGISTER)

                val credential = GoogleAuthProvider.getCredential(googleAccount.idToken, null)

                //Second step: Register Google user to firebase
                mAuth.signInWithCredential(credential).addOnCompleteListener(this@RegisterActivity) { signInTask ->

                    when {
                        signInTask.isSuccessful ->

                            //Third step: Receive firebase id token page newly created firebase user
                            signInTask.result!!.user.getIdToken(true).addOnCompleteListener(this@RegisterActivity) { tokenTask ->

                                when {
                                    tokenTask.isSuccessful -> {

                                        val firebaseToken = tokenTask.result!!.token!!

                                        //Create request body for api register request
                                        val requestBodyRegister = RequestBodyRegister(firebaseToken, "", "", "", "", "")

                                        if (googleAccount.photoUrl != null) {
                                            requestBodyRegister.picture = googleAccount.photoUrl!!.toString()
                                        }

                                        //Complete registration by own backend registration
                                        onFirebaseRegistrationSuccess(signInTask.result!!, firebaseToken, requestBodyRegister)

                                    }
                                    tokenTask.exception != null -> {

                                        if( tokenTask.exception is FirebaseNetworkException ) {
                                            Util.toastIt(this@RegisterActivity, R.string.error_connection)
                                        }else{
                                            Util.toastIt(this@RegisterActivity, R.string.signup_unknown_error)
                                        }
                                        stopInProgress()
                                    }
                                    else -> {
                                        Util.toastIt(this@RegisterActivity, R.string.signup_unknown_error)
                                        stopInProgress()
                                    }
                                }
                            }

                        signInTask.exception != null -> {

                            when {
                                signInTask.exception is FirebaseAuthUserCollisionException -> Util.toastIt(this@RegisterActivity, R.string.reg_error_email_duplicate)
                                signInTask is FirebaseNetworkException -> Util.toastIt(this@RegisterActivity, R.string.error_connection)
                                else -> Util.toastIt(this@RegisterActivity, R.string.signup_unknown_error)
                            }
                            stopInProgress()
                        }

                        else -> {
                            Util.toastIt(this@RegisterActivity, R.string.signup_unknown_error)
                            stopInProgress()
                        }
                    }
                }
            }

            override fun onFailure(textId: Int) {
                Util.toastIt(this@RegisterActivity, textId)
                stopInProgress()
            }

            override fun onConnectionFailed(textId: Int) {
                Util.toastIt(this@RegisterActivity, textId)
                stopInProgress()
            }
        })
    }

    private val onClickRegisterFacebook = View.OnClickListener {

        //Hide the input fields for email registration
        hideEmailInput()

        //Update gui state
        setInProgress()

        //First step: Login to Facebook
        facebookInterface.loginFacebook(object : FacebookInterface.FacebookLoginCallback {

            override fun onSuccess(accessToken: AccessToken, facebookPayload: FacebookInterface.FacebookLoginPayload) {

                //Show loading popup
                loadingPopup.show(PopupLoadingCredentials.METHOD_FACEBOOK, PopupLoadingCredentials.USE_REGISTER)

                val credential = FacebookAuthProvider.getCredential(accessToken.token)

                //Second step: Register Facebook user to Firebase
                mAuth.signInWithCredential(credential).addOnCompleteListener(this@RegisterActivity) { signInTask ->

                    when {
                        signInTask.isSuccessful ->

                            //Third step: Receive firebase id token page newly created firebase user
                            signInTask.result!!.user.getIdToken(true).addOnCompleteListener(this@RegisterActivity) { tokenTask ->

                                when {
                                    tokenTask.isSuccessful -> {

                                        val firebaseToken = tokenTask.result!!.token!!

                                        //Create request body for api register request
                                        val requestBodyRegister = RequestBodyRegister(firebaseToken, "", "", "", "", "")

                                        //Add optional fields (if present)
                                        if (facebookPayload.firstname != null) {
                                            requestBodyRegister.firstname = facebookPayload.firstname
                                        }
                                        if (facebookPayload.lastname != null) {
                                            requestBodyRegister.lastname = facebookPayload.lastname
                                        }
                                        if (facebookPayload.gender != null) {
                                            if (facebookPayload.gender.equals("male", ignoreCase = true)) {
                                                requestBodyRegister.sex = C.MALE
                                            } else if (facebookPayload.gender.equals("female", ignoreCase = true)) {
                                                requestBodyRegister.sex = C.FEMALE
                                            }
                                        }
                                        if (facebookPayload.birthday != null) {
                                            requestBodyRegister.birthday = DateUtils.facebookDateToSqlDate(facebookPayload.birthday)
                                        }

                                        if (facebookPayload.pictureUrl != null) {
                                            requestBodyRegister.picture = facebookPayload.pictureUrl
                                        }

                                        //Complete registration by own backend registration
                                        onFirebaseRegistrationSuccess(signInTask.result!!, firebaseToken, requestBodyRegister)

                                    }
                                    tokenTask.exception != null -> {

                                        if (tokenTask.exception is FirebaseNetworkException) {
                                            Util.toastIt(this@RegisterActivity, R.string.error_connection)
                                        } else {
                                            Util.toastIt(this@RegisterActivity, R.string.signup_unknown_error)
                                        }
                                        stopInProgress()
                                    }
                                    else -> {
                                        Util.toastIt(this@RegisterActivity, R.string.signup_unknown_error)
                                        stopInProgress()
                                    }
                                }
                            }
                        signInTask.exception != null -> {

                            when {
                                signInTask.exception is FirebaseAuthUserCollisionException -> Util.toastIt(this@RegisterActivity, R.string.reg_error_email_duplicate)
                                signInTask is FirebaseNetworkException -> Util.toastIt(this@RegisterActivity, R.string.error_connection)
                                else -> Util.toastIt(this@RegisterActivity, R.string.signup_unknown_error)
                            }
                            stopInProgress()
                        }
                        else -> {
                            Util.toastIt(this@RegisterActivity, R.string.signin_unknown_error)
                            stopInProgress()
                        }
                    }
                }
            }

            override fun onFailure(textId: Int) {
                Util.toastIt(this@RegisterActivity, textId)
                stopInProgress()
            }
        })
    }

    private fun onFirebaseRegistrationSuccess(registerTaskResult: AuthResult, firebaseToken:String, requestBodyRegister: RequestBodyRegister) {

        ApiFunctions.register(requestBodyRegister,
                Response.Listener { response ->
                    val rbUser = ResponseUnpacker(RBUser::class.java).parse(response)

                    if (rbUser != null) {

                        /*
                            Registration on own backend was completed successful
                         */

                        // Add or update the device id and token on the backend
                        DeviceRegistrationService.registerDeviceForUser(firebaseToken)

                        enterRegistrationDetail(rbUser)

                        //Destroy loading popup
                        stopInProgress()

                    } else {

                        //Response parsing failed
                        deleteNewFirebaseUser(registerTaskResult)
                        Util.toastIt(this@RegisterActivity, R.string.signup_unknown_error)
                        stopInProgress()
                    }
                }, Response.ErrorListener {

                    deleteNewFirebaseUser(registerTaskResult)

                    Log.e(ApiUtils.LOG_TAG_ERROR, ApiUtils.parseVolleyError(it))
                    Util.toastIt(this@RegisterActivity, R.string.signup_unknown_error)
                    stopInProgress()
                })
    }

    private val onClickLogin = View.OnClickListener {

        val intent = Intent(this, LoginActivity::class.java)
        startActivity(intent)
        overridePendingTransition(R.anim.slide_in_from_right, R.anim.stay)
    }

    private val onTextChangeEmail = MyTextWatcher(object : MyTextWatcher.OnTextChanged {
        override fun onTextChanged(newText: String) {
            tilRegisterEmail.error = null
        }
    })

    private val onTextChangePassword = MyTextWatcher(object : MyTextWatcher.OnTextChanged {
        override fun onTextChanged(newText: String) {
            tilRegisterPassword.error = null
        }
    })

    private fun showEmailInput(){

        emailInputVisible = true

        bnEmailRegister.visibility = View.GONE
        inclRegisterFacebookGoogle.visibility = View.GONE
        inclRegisterSeparator.visibility = View.GONE

        bnRegister.visibility = View.VISIBLE
        tilRegisterEmail.visibility = View.VISIBLE
        tilRegisterPassword.visibility = View.VISIBLE
    }

    private fun hideEmailInput() {

        emailInputVisible = false

        bnEmailRegister.visibility = View.VISIBLE
        inclRegisterFacebookGoogle.visibility = View.VISIBLE
        inclRegisterSeparator.visibility = View.VISIBLE

        tilRegisterEmail.visibility = View.GONE
        tilRegisterPassword.visibility = View.GONE
        bnRegister.visibility = View.GONE

        tilRegisterEmail.error = null
        tilRegisterPassword.error = null

        etRegisterEmail.text.clear()
        etRegisterPassword.text.clear()
    }

    private fun deleteNewFirebaseUser(registerTaskResult: AuthResult) {

        if (registerTaskResult.additionalUserInfo.isNewUser) {

            //Delete user page Firebase
            registerTaskResult.user.delete()
        } else {

            //Just uncache the user
            mAuth.signOut()
        }
    }

    override fun onBackPressed() {

        if( loading ) return

        if( emailInputVisible ){

            hideEmailInput()
            return
        }

        super.onBackPressed()

        overridePendingTransition(R.anim.stay, R.anim.slide_out_to_right)
    }

    override fun onDestroy() {

        super.onDestroy()

        loadingPopup.close()

        destroyUi()
    }

    private fun setInProgress() {

        //Update state
        loading = true
    }

    private fun stopInProgress() {

        //Update state
        loading = false

        loadingPopup.close()
    }

    private fun inputIsValid(): Boolean {

        val email = etRegisterEmail.text.toString().trim()
        val password = etRegisterPassword.text.toString().trim()

        //Check if mandatory fields are filled
        return when {
            email.isEmpty() -> {
                tilRegisterEmail.error = getString(R.string.error_mandatory_field)
                false
            }
            password.isEmpty() -> {
                tilRegisterPassword.error = getString(R.string.error_mandatory_field)
                false
            }
            !Patterns.EMAIL_ADDRESS.matcher(email).matches() -> {
                tilRegisterEmail.error = getString(R.string.error_email_wrong_pattern)
                false
            }
            password.length < 6 || password.length > 25 -> {
                tilRegisterPassword.error = getString(R.string.error_password_length)
                false
            }
            else -> true
        }
    }

    private fun enterRegistrationDetail(rbUser: RBUser) {

        val intent = Intent(this, RegisterDetailActivity::class.java)

        val bundle = Bundle()
        if (rbUser.firstname != null && !rbUser.firstname.isEmpty()) {
            bundle.putString(getString(R.string.bkey_firstname), rbUser.firstname)
        }
        if (rbUser.lastname != null && !rbUser.lastname.isEmpty()) {
            bundle.putString(getString(R.string.bkey_lastname), rbUser.lastname)
        }
        if (rbUser.sex != null && (rbUser.sex == C.MALE || rbUser.sex == C.FEMALE)) {
            bundle.putString(getString(R.string.bkey_gender), rbUser.sex)
        }
        if (rbUser.birthday != null && !rbUser.birthday.isEmpty()) {
            bundle.putString(getString(R.string.bkey_birthday), DateUtils.sqlToAndroidDate(rbUser.birthday))
        }

        if (!bundle.isEmpty) {
            intent.putExtras(bundle)
        }

        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)
        finish()
        overridePendingTransition(R.anim.slide_in_from_right, R.anim.stay)
    }
}
