package de.bodymate.bodymateapp.activity

import android.content.Intent
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import android.view.View
import com.google.firebase.auth.FirebaseAuth
import com.readystatesoftware.systembartint.SystemBarTintManager
import de.bodymate.bodymateapp.CurrentUser
import de.bodymate.bodymateapp.R
import de.bodymate.bodymateapp.data.service.*
import de.bodymate.bodymateapp.extensions.string
import de.bodymate.bodymateapp.util.Util
import kotlinx.android.synthetic.main.activity_settings.*

class SettingsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settings)

        // Set action bar
        setSupportActionBar(tbSettings)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = string(R.string.settings)
        supportActionBar?.setBackgroundDrawable(ColorDrawable(Util.color(this, R.color.colorPrimaryLight)))

        // Set status bar color
        val statusBarTintManager = SystemBarTintManager(this)
        statusBarTintManager.isStatusBarTintEnabled = true
        statusBarTintManager.setStatusBarTintColor(Util.color(this, R.color.colorPrimaryLight))

        //Apply click listeners
        tvSettingsLogout.setOnClickListener(onClickLogout)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        return when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private val onClickLogout = View.OnClickListener {

        CurrentUser.logout(application, onCompleted = {

            // Start Launch-Activity and delete backstack
            val intent = Intent(this, LaunchActivity::class.java)
            startActivity(intent)
            finish()
        })
    }

    override fun onBackPressed() {

        super.onBackPressed()

        //Slide activity away to right
        overridePendingTransition(R.anim.stay, R.anim.slide_out_to_right)
    }
}
