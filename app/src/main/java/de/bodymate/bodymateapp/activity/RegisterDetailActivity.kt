package de.bodymate.bodymateapp.activity

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import android.widget.ImageView
import com.android.volley.NoConnectionError
import com.android.volley.Response
import com.google.firebase.auth.FirebaseAuth
import de.bodymate.bodymateapp.CurrentUser
import de.bodymate.bodymateapp.R
import de.bodymate.bodymateapp.api.ApiFunctions
import de.bodymate.bodymateapp.api.ApiUtils
import de.bodymate.bodymateapp.api.ResponseUnpacker
import de.bodymate.bodymateapp.api.requestbody.RequestBodyRegisterDetail
import de.bodymate.bodymateapp.api.response.RBUser
import de.bodymate.bodymateapp.data.service.*
import de.bodymate.bodymateapp.fragment.FragmentRegisterDetailComplete
import de.bodymate.bodymateapp.fragment.FragmentRegisterDetailGeneral
import de.bodymate.bodymateapp.fragment.FragmentRegisterDetailGoal
import de.bodymate.bodymateapp.fragment.FragmentRegisterDetailScheduleEquipment
import de.bodymate.bodymateapp.gui.MyFragmentPagerAdapter
import de.bodymate.bodymateapp.notification.NotificationSchedulerService
import de.bodymate.bodymateapp.util.C
import de.bodymate.bodymateapp.util.DateUtils
import de.bodymate.bodymateapp.util.Time
import de.bodymate.bodymateapp.util.Util
import kotlinx.android.synthetic.main.activity_register_detail.*
import java.util.*


class RegisterDetailActivity : AppCompatActivity() {

    private val dots: MutableList<ImageView> = mutableListOf()

    private var leaveAppOnClick = false

    private var fireAuth: FirebaseAuth? = null

    private var fragmentGeneral: FragmentRegisterDetailGeneral? = null
    private var fragmentGoal: FragmentRegisterDetailGoal? = null
    private var fragmentScheduleEquipment: FragmentRegisterDetailScheduleEquipment? = null
    private var fragmentRegisterDetailComplete: FragmentRegisterDetailComplete? = null

    private var fragmentPagerAdapterTabless: MyFragmentPagerAdapter? = null

    private var currentSlide = 0

    private var firstname: String? = null
    private var lastname: String? = null
    private var birthday: String? = null
    private var gender: String? = null
    private var goal: Int = 0
    private var weekdays: Set<Int>? = null
    private var equipment: Set<Int>? = null

    private var inProgress = false

    override fun onCreate(savedInstanceState: Bundle?) {

        setTheme(R.style.AppTheme)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register_detail)

        dots.add(ivRegDetailViewPagerInd1)
        dots.add(ivRegDetailViewPagerInd2)
        dots.add(ivRegDetailViewPagerInd3)
        dots.add(ivRegDetailViewPagerInd4)

        tvRegisterDetailNext.setOnClickListener(onClickNext)
        tvRegisterDetailBack.setOnClickListener(onClickBack)

        fireAuth = FirebaseAuth.getInstance()

        if (savedInstanceState != null) {
            //Restore fragment instances
            fragmentGeneral = supportFragmentManager.getFragment(savedInstanceState, getString(R.string.bkey_frag_general)) as FragmentRegisterDetailGeneral?
            fragmentGoal = supportFragmentManager.getFragment(savedInstanceState, getString(R.string.bkey_frag_goal)) as FragmentRegisterDetailGoal?
            fragmentScheduleEquipment = supportFragmentManager.getFragment(savedInstanceState, getString(R.string.bkey_frag_sched_equip)) as FragmentRegisterDetailScheduleEquipment?
            fragmentRegisterDetailComplete = supportFragmentManager.getFragment(savedInstanceState, getString(R.string.bkey_frag_complete)) as FragmentRegisterDetailComplete?
        }

        unpackKnownUserData()
        setUpSlides()
    }

    override fun onSaveInstanceState(outState: Bundle) {

        super.onSaveInstanceState(outState)

        //Save current active slide page to persist
        outState.putInt(getString(R.string.bkey_curr_slide), currentSlide)

        //Save fragment instances
        supportFragmentManager.putFragment(outState, getString(R.string.bkey_frag_general), fragmentGeneral!!)
        supportFragmentManager.putFragment(outState, getString(R.string.bkey_frag_goal), fragmentGoal!!)
        supportFragmentManager.putFragment(outState, getString(R.string.bkey_frag_sched_equip), fragmentScheduleEquipment!!)
        supportFragmentManager.putFragment(outState, getString(R.string.bkey_frag_complete), fragmentRegisterDetailComplete!!)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle?) {

        super.onRestoreInstanceState(savedInstanceState)

        if (savedInstanceState != null) {

            //Retrieve selected slide index page dead instance
            val lastSelectedSlide = savedInstanceState.getInt(getString(R.string.bkey_curr_slide), -1)

            if (lastSelectedSlide != -1) {

                currentSlide = lastSelectedSlide
                for (dot in dots) {
                    dot.setImageResource(R.drawable.viewpager_dot)
                }
                dots[currentSlide].setImageResource(R.drawable.viewpager_dot_selected)
                tvRegisterDetailBack.setText(if (currentSlide == 0) R.string.cancel else R.string.back)
            }
        }
    }

    private fun unpackKnownUserData() {

        val bundle = intent.extras ?: return

        //Receive firstname
        firstname = bundle.getString(getString(R.string.bkey_firstname))

        //Receive lastname
        lastname = bundle.getString(getString(R.string.bkey_lastname))

        //Receive birthday
        birthday = bundle.getString(getString(R.string.bkey_birthday))

        //Receive gender
        gender = bundle.getString(getString(R.string.bkey_gender))
    }

    private fun setUpSlides() {

        fragmentPagerAdapterTabless = MyFragmentPagerAdapter(supportFragmentManager)

        //Create second slide fragment : General
        if (fragmentGeneral == null) {

            fragmentGeneral = FragmentRegisterDetailGeneral()

            val arguments = Bundle()

            if (firstname != null && firstname!!.isNotEmpty()) {
                arguments.putString(getString(R.string.bkey_firstname), firstname)
            }

            if (lastname != null && lastname!!.isNotEmpty()) {
                arguments.putString(getString(R.string.bkey_lastname), lastname)
            }

            if (birthday != null && birthday!!.isNotEmpty()) {
                arguments.putString(getString(R.string.bkey_birthday), birthday)
            }

            if (gender != null && gender!!.isNotEmpty()) {
                arguments.putString(getString(R.string.bkey_gender), gender)
            }

            if (!arguments.isEmpty) {
                fragmentGeneral!!.arguments = arguments
            }
        }

        fragmentPagerAdapterTabless!!.addFragment(fragmentGeneral!!)

        //Create third slide fragment : Goal definition
        if (fragmentGoal == null) {
            fragmentGoal = FragmentRegisterDetailGoal()
        }
        fragmentPagerAdapterTabless!!.addFragment(fragmentGoal!!)

        //Create fourth slide fragment : RespEquipment and schedule
        if (fragmentScheduleEquipment == null) {
            fragmentScheduleEquipment = FragmentRegisterDetailScheduleEquipment()
        }
        fragmentPagerAdapterTabless!!.addFragment(fragmentScheduleEquipment!!)

        //Create fifth slide fragment : Complete registration
        if (fragmentRegisterDetailComplete == null) {
            fragmentRegisterDetailComplete = FragmentRegisterDetailComplete()
        }
        fragmentPagerAdapterTabless!!.addFragment(fragmentRegisterDetailComplete!!)

        //Apply slide fragments to adapter
        vpRegisterDetail.adapter = fragmentPagerAdapterTabless
        vpRegisterDetail.offscreenPageLimit = fragmentPagerAdapterTabless!!.maxOffscreenPageLimit
    }

    private val onClickNext: View.OnClickListener = View.OnClickListener {

        var next = false

        //Check current form
        if (currentSlide == 0) {

            next = fragmentGeneral!!.checkForm()

            if (next) {

                //Grab user values
                firstname = fragmentGeneral!!.firstname
                lastname = fragmentGeneral!!.lastname
                birthday = fragmentGeneral!!.birthday
                gender = fragmentGeneral!!.gender

                //Write firstname to welcome letter
                fragmentRegisterDetailComplete!!.updateFirstname(firstname!!)
            }

        } else if (currentSlide == 1) {

            next = fragmentGoal!!.checkForm()

            if (next) {
                goal = fragmentGoal!!.selectedGoal
            }

        } else if (currentSlide == 2) {

            next = fragmentScheduleEquipment!!.checkForm()

            weekdays = fragmentScheduleEquipment!!.selectedWeekdays
            equipment = fragmentScheduleEquipment!!.getSelectedEquipment()

        } else if (currentSlide == 3) {

            onClickComplete()
            return@OnClickListener
        }

        if (next) {
            showNextSlide()
        }
    }

    fun showNextSlide() {

        if (currentSlide < fragmentPagerAdapterTabless!!.count - 1) {

            currentSlide++

            vpRegisterDetail.setCurrentItem(currentSlide, true)
            dots[currentSlide - 1].setImageResource(R.drawable.viewpager_dot)
            dots[currentSlide].setImageResource(R.drawable.viewpager_dot_selected)

            tvRegisterDetailBack.setText(R.string.back)
            tvRegisterDetailBack.visibility = View.VISIBLE

            if (vpRegisterDetail.currentItem == fragmentPagerAdapterTabless!!.count - 1) {
                //Arrived at the last page
                tvRegisterDetailNext.setText(R.string.complete_registration)
            }
        }
    }

    private val onClickBack: View.OnClickListener = View.OnClickListener {

        if (currentSlide > 0) {

            currentSlide--

            vpRegisterDetail.setCurrentItem(currentSlide, true)
            dots[currentSlide + 1].setImageResource(R.drawable.viewpager_dot)
            dots[currentSlide].setImageResource(R.drawable.viewpager_dot_selected)

            tvRegisterDetailBack.setText(R.string.back)
            tvRegisterDetailNext.setText(R.string.next)

            if (currentSlide == 0) {
                tvRegisterDetailBack.visibility = View.INVISIBLE
            }
        }
    }

    private fun onClickComplete() {

        setInProgress()

        val user = fireAuth!!.currentUser

        if (user == null) {
            Util.toastIt(this, R.string.error_register_detail)
            return
        }

        //Set 6 PM as initial workout time
        val daytimes = ArrayList<String>()
        for (i in weekdays!!.indices) {
            daytimes.add(Time.of(18, 0).toText(true))
        }

        user.getIdToken(true).addOnCompleteListener(this) { tokenTask ->

            if (tokenTask.isSuccessful) {

                val requestBodyRegisterDetail = RequestBodyRegisterDetail(
                        tokenTask.result!!.token, firstname, lastname, DateUtils.androidToSqlDate(birthday), gender, goal, weekdays, daytimes, equipment)

                ApiFunctions.registerDetail(requestBodyRegisterDetail,
                        Response.Listener { response ->
                            val rbUser = ResponseUnpacker(RBUser::class.java).parse(response)

                            if (rbUser != null) {

                                onRegisterDetailSuccess(rbUser)

                            } else {

                                //Response parsing failed
                                Util.toastIt(this@RegisterDetailActivity, R.string.error_register_detail)
                                stopInProgress()
                            }
                        },
                        Response.ErrorListener { error ->

                            if (error is NoConnectionError) {
                                Util.toastIt(this@RegisterDetailActivity, R.string.error_connection)
                            } else {
                                Util.toastIt(this@RegisterDetailActivity, R.string.error_register_detail)
                            }

                            Log.e(ApiUtils.LOG_TAG_ERROR, ApiUtils.parseVolleyError(error))
                            stopInProgress()
                        })
            } else {
                Util.toastIt(this@RegisterDetailActivity, R.string.error_register_detail)
                stopInProgress()
            }
        }
    }

    private fun setInProgress() {

        inProgress = true

        pbRegisterDetailLoading.visibility = View.VISIBLE

        tvRegisterDetailBack.isEnabled = false
        tvRegisterDetailBack.setTextColor(Util.color(this, R.color.white_text_disabled))
        tvRegisterDetailNext.isEnabled = false
        tvRegisterDetailNext.setTextColor(Util.color(this, R.color.white_text_disabled))
    }

    private fun stopInProgress() {

        inProgress = false

        pbRegisterDetailLoading.visibility = View.GONE

        tvRegisterDetailBack.isEnabled = true
        tvRegisterDetailBack.setTextColor(Util.color(this, android.R.color.white))
        tvRegisterDetailNext.isEnabled = true
        tvRegisterDetailNext.setTextColor(Util.color(this, android.R.color.white))
    }

    private fun onRegisterDetailSuccess(rbUser: RBUser) {

        //Persist user
        CurrentUser.FIRE_ID = rbUser.fireId
        UserService.getInstance(application).update(rbUser)

        //Persist allPlans (only fitnesstest)
        PlanService.getInstance(application).updateAll(rbUser.plans)

        //Persist allExercises
        ExerciseService.getInstance(application).updateAll(rbUser.plans)

        //Persist schedules
        ScheduleService.getInstance(application).updateAll(rbUser.schedules)

        //Persist equipment
        EquipmentService.getInstance(application).updateAll(rbUser.equipments)

        //Schedule workout notifications
        NotificationSchedulerService.scheduleWorkoutReminderNotifications(this.applicationContext, rbUser.schedules)

        //Enter the MainActivity
        val intent = Intent(this@RegisterDetailActivity, MainActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
        intent.putExtra(Util.text(this, R.string.bkey_fire_id), rbUser.fireId)
        intent.putExtra(Util.text(this, R.string.bkey_freshman), C.TRUE)
        startActivity(intent)
        finish()

        //With no special animation
        overridePendingTransition(0, 0)
    }

    override fun onBackPressed() {

        if (inProgress) {
            leaveAppOnClick = false
            return
        }

        //Leave APP
        if (leaveAppOnClick) {
            super.onBackPressed()
        } else {
            leaveAppOnClick = true
            Util.toastIt(this, R.string.register_detail_cancel)
            Timer().schedule(object : TimerTask() {
                override fun run() {
                    leaveAppOnClick = false
                }
            }, 1000)
        }
    }
}