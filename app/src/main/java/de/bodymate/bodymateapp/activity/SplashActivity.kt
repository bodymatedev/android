package de.bodymate.bodymateapp.activity

import android.arch.lifecycle.Lifecycle
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.content.Intent.FLAG_ACTIVITY_CLEAR_TOP
import android.content.Intent.FLAG_ACTIVITY_NEW_TASK
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import android.view.animation.AnimationUtils
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import de.bodymate.bodymateapp.CurrentUser
import de.bodymate.bodymateapp.R
import de.bodymate.bodymateapp.api.ApiFunctions
import de.bodymate.bodymateapp.api.ApiUtils
import de.bodymate.bodymateapp.api.requestbody.RequestBodyLogin
import de.bodymate.bodymateapp.api.response.RBUser
import de.bodymate.bodymateapp.data.service.*
import de.bodymate.bodymateapp.notification.DeviceRegistrationService
import de.bodymate.bodymateapp.notification.NotificationSchedulerService
import de.bodymate.bodymateapp.util.C
import de.bodymate.bodymateapp.util.DateUtils
import de.bodymate.bodymateapp.viewmodel.ViewModelActivitySplash
import kotlinx.android.synthetic.main.activity_splash.*



class SplashActivity : AppCompatActivity() {

    private lateinit var viewmodel: ViewModelActivitySplash
    private lateinit var fireAuth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        viewmodel = ViewModelProviders.of(this).get(ViewModelActivitySplash::class.java)

        fireAuth = FirebaseAuth.getInstance()

        //Animate logo
        val slideUpIn = AnimationUtils.loadAnimation(this, R.anim.logo_in)
        ivSplashLogo.startAnimation(slideUpIn)
        ivSplashLogo.visibility = View.VISIBLE
    }

    override fun onStart() {

        super.onStart()

        val fireUser = fireAuth.currentUser

        if( fireUser != null ) {
            handleCachedUser(fireUser)
        } else {
            doNotEnter()
        }
    }

    private fun handleCachedUser(fireUser: FirebaseUser) {

        //First step: Receive firebase id token page cached user
        fireUser.getIdToken(true)
                .addOnSuccessListener(viewmodel.tokenTask::postValue)
                .addOnCanceledListener { onAnyFailure() }
                .addOnFailureListener { onAnyFailure() }

        viewmodel.tokenTask.observe(this, Observer {

            //Check if this activity is still running (for extremely late callbacks...)
            if( this@SplashActivity.lifecycle.currentState.isAtLeast(Lifecycle.State.RESUMED) ) {

                if (it == null) {
                    onAnyFailure()
                    return@Observer
                }

                val firebaseToken = it.token!!

                //Login at backend
                ApiFunctions.loginKt(RequestBodyLogin(firebaseToken),

                        onResponse = { rbUser ->

                            //Check if this activity is still running (for extremely late callbacks...)
                            if( this@SplashActivity.lifecycle.currentState.isAtLeast(Lifecycle.State.RESUMED) ) {

                                // Add or update the device id and token on the backend
                                DeviceRegistrationService.registerDeviceForUser(firebaseToken)

                                if (rbUser != null) {
                                    onSilentLoginSuccess(rbUser)
                                } else {
                                    onAnyFailure()
                                }
                            }
                        },
                        onError = { error ->

                            //Check if this activity is still running (for extremely late callbacks...)
                            if( this@SplashActivity.lifecycle.currentState.isAtLeast(Lifecycle.State.RESUMED) ) {

                                Log.d(ApiUtils.LOG_TAG_ERROR, ApiUtils.parseVolleyError(error))
                                onAnyFailure()
                            }
                        }
                )
            }
        })
    }

    private fun onSilentLoginSuccess(rbUser: RBUser) {

        /*
    		Silent Login was successful
    	 */

        if (!rbUser.registrationCompleted.equals(C.TRUE, ignoreCase = true)) {

            //Registration was not completed yet
            onUncompletedRegistration(rbUser)
            return
        }

        //Persist Exercises
        ExerciseService.getInstance(application).updateAll(rbUser.plans)

        //Persist Plans
        PlanService.getInstance(application).updateAll(rbUser.plans)

        //Persist Workouts
        WorkoutService.getInstance(application).update(rbUser.latestWorkout)

        //Persist Schedules
        ScheduleService.getInstance(application).updateAll(rbUser.schedules)

        //Persist Achievements
        AchievementService.getInstance(application).updateAll(rbUser.achievements)

        //Persist Equipments
        EquipmentService.getInstance(application).updateAll(rbUser.equipments)

        //Persist user, and wait for finish to continue
        UserService.getInstance(application).update(rbUser)
        CurrentUser.FIRE_ID = rbUser.fireId

        //Schedule workout notifications
        NotificationSchedulerService.scheduleWorkoutReminderNotifications(this.applicationContext, rbUser.schedules)

        enter()
    }

    private fun onUncompletedRegistration(rbUser: RBUser) {

        val intent = Intent(this@SplashActivity, RegisterDetailActivity::class.java)

        val bundle = Bundle()

        if (rbUser.firstname != null && !rbUser.firstname.isEmpty()) {
            bundle.putString(getString(R.string.bkey_firstname), rbUser.firstname)
        }
        if (rbUser.lastname != null && !rbUser.lastname.isEmpty()) {
            bundle.putString(getString(R.string.bkey_lastname), rbUser.lastname)
        }
        if (rbUser.sex != null && (rbUser.sex.equals(C.MALE, ignoreCase = true) || rbUser.sex.equals(C.FEMALE, ignoreCase = true))) {
            bundle.putString(getString(R.string.bkey_gender), rbUser.sex)
        }
        if (rbUser.birthday != null && !rbUser.birthday.isEmpty()) {
            bundle.putString(getString(de.bodymate.bodymateapp.R.string.bkey_birthday), DateUtils.sqlToAndroidDate(rbUser.birthday))
        }

        if (!bundle.isEmpty) {
            intent.putExtras(bundle)
        }

        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)
        finish()
    }

    private fun onAnyFailure() {

        // Delete allEquipments local data
        AchievementService.getInstance(application).deleteAll()
        WorkoutService.getInstance(application).deleteAll()
        PlanService.getInstance(application).deleteAll()
        ExerciseService.getInstance(application).deleteAll()
        EquipmentService.getInstance(application).deleteAll()
        ScheduleService.getInstance(application).deleteAll()
        UserService.getInstance(application).deleteUser()

        // Firebase sign out
        fireAuth.signOut()
        CurrentUser.FIRE_ID = null

        doNotEnter()
    }

    private fun doNotEnter() {

        val intent = Intent(this, LaunchActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
        startActivity(intent)
        finish()
    }

    private fun enter(){

        val splashIntentExtras = intent.extras

        val enterIntent = Intent(this, MainActivity::class.java).apply {

            flags = FLAG_ACTIVITY_CLEAR_TOP or FLAG_ACTIVITY_NEW_TASK

            // If extras exist, just pass them down
            if( splashIntentExtras != null ) putExtras(splashIntentExtras)
        }

        startActivity(enterIntent)
        finish()
        overridePendingTransition(R.anim.slide_up_from_bottom, R.anim.stay)
    }

    override fun onBackPressed() { }
}
