package de.bodymate.bodymateapp.activity

import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import de.bodymate.bodymateapp.R
import de.bodymate.bodymateapp.extensions.string
import de.bodymate.bodymateapp.viewmodel.ViewModelActivityAchievement
import kotlinx.android.synthetic.main.activity_achievement.*

class AchievementActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_achievement)

        setSupportActionBar(tbAchievements)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = string(R.string.achievements)

        val viewModelAchievementActivity = ViewModelProviders.of(this).get(ViewModelActivityAchievement::class.java)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        return when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onBackPressed() {

        super.onBackPressed()

        //Slide activity away to right
        overridePendingTransition(R.anim.stay, R.anim.slide_out_to_right)
    }
}
