package de.bodymate.bodymateapp.api.requestbody

data class RequestEntityScheduling( val weekday: Int,
                                    val time: String )