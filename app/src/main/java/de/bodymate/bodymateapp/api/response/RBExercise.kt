package de.bodymate.bodymateapp.api.response

import com.google.gson.annotations.SerializedName

/**
 * Created by Leonard on 20.08.2018.
 */

data class RBExercise(val id: Int,
                      val name: String,
                      val type: String,
                      val description: String,
                      @SerializedName("video_url") val videoUrl: String?)