package de.bodymate.bodymateapp.api.requestbody

import com.google.gson.annotations.SerializedName

/**
 * Created by Leonard on 28.05.2018.
 */

data class RequestBodyRegisterDetail(@SerializedName("fire_id_token") val fireIdToken: String?,
                                     val firstname: String?,
                                     val lastname: String?,
                                     val birthday: String?,
                                     val sex: String?,
                                     val goal: Int,
                                     val weekdays: Set<Int>?,
                                     val daytimes: List<String>?,
                                     val equipment: Set<Int>?)