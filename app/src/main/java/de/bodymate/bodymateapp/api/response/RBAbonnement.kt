package de.bodymate.bodymateapp.api.response

import com.google.gson.annotations.SerializedName

/**
 * Created by Leonard on 20.08.2018.
 */

data class RBAbonnement(val id: Int,
                        @SerializedName("type") val abonnementType: RBAbonnementType?,
                        @SerializedName("booking_date") val bookingDate: String?,
                        @SerializedName("cancel_date") val cancelDate: String?,
                        val trainer: RBTrainer?)