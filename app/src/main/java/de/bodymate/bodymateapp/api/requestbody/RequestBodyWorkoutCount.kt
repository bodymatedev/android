package de.bodymate.bodymateapp.api.requestbody

import com.google.gson.annotations.SerializedName

data class RequestBodyWorkoutCount( @SerializedName("fire_id") val fireId: String )