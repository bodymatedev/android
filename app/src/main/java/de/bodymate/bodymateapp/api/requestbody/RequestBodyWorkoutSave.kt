package de.bodymate.bodymateapp.api.requestbody

import com.google.gson.annotations.SerializedName
import de.bodymate.bodymateapp.api.requestbody.dto.RequestDtoWorkoutPosition

data class RequestBodyWorkoutSave(@SerializedName("fire_id") val fireId: String?,
                                  @SerializedName("plan_id") val planId: Int,
                                  @SerializedName("datetime_start") val datetimeStart: String?,
                                  @SerializedName("datetime_end") val datetimeEnd: String?,
                                  val rating: Int,
                                  val review: String?,
                                  @SerializedName("planned_for") val plannedFor: String,
                                  val positions: List<RequestDtoWorkoutPosition>?)