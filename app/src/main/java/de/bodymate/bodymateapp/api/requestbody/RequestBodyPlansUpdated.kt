package de.bodymate.bodymateapp.api.requestbody

import com.google.gson.annotations.SerializedName

data class RequestBodyPlansUpdated(@SerializedName("fire_id") private val fireId:String?,
                                   @SerializedName("date_of_update") private val dateOfUpdate:String?)