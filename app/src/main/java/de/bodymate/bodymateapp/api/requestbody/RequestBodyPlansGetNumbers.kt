package de.bodymate.bodymateapp.api.requestbody

import com.google.gson.annotations.SerializedName

data class RequestBodyPlansGetNumbers(@SerializedName("fire_id") val fireId: String,
                                      @SerializedName("plan_ids") val planIds: List<Int>)