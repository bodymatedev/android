package de.bodymate.bodymateapp.api.response

/**
 * Created by Leonard on 20.08.2018.
 */

data class RBPlanSet(val set: Int, val reps: Int)