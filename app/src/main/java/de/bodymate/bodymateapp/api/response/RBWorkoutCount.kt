package de.bodymate.bodymateapp.api.response

data class RBWorkoutCount( val count: Int )