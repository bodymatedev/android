package de.bodymate.bodymateapp.api.requestbody.dto

data class RequestDtoWorkoutPosition(val position: Int, val sets: List<RequestDtoWorkoutSet>?)