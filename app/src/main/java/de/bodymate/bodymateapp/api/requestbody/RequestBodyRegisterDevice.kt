package de.bodymate.bodymateapp.api.requestbody

import com.google.gson.annotations.SerializedName

data class RequestBodyRegisterDevice(@SerializedName("fire_id_token") val fireIdToken:String,
                                     @SerializedName("registration_id") val registrationId:String,
                                     @SerializedName("registration_token") val registrationToken:String)
