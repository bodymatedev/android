package de.bodymate.bodymateapp.api.requestbody

import com.google.gson.annotations.SerializedName

/**
 * Created by Leonard on 24.05.2018.
 */

data class RequestBodyLogin(@SerializedName("fire_id_token") val fireIdToken: String?)