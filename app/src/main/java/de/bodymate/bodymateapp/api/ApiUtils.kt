package de.bodymate.bodymateapp.api

import android.util.Log
import com.android.volley.VolleyError
import com.google.gson.GsonBuilder

import org.json.JSONException
import org.json.JSONObject

import java.io.UnsupportedEncodingException
import java.lang.Exception


/**
 * Created by Leonard on 29.05.2018.
 */
object ApiUtils {

    const val LOG_TAG_ERROR = "API_ERROR"

    private val gson = GsonBuilder().create()

    fun serializeBody(bodyObject: Any): JSONObject? {

        return try {
            JSONObject(gson.toJson(bodyObject, bodyObject.javaClass))
        } catch (e: Exception) {
            null
        }
    }

    fun serializeBody(bodyObject: Any, onError: (VolleyError) -> Unit): JSONObject?{

        return try {
            JSONObject(gson.toJson(bodyObject, bodyObject.javaClass))
        } catch (e: JSONException) {
            onError(VolleyError("Gson error"))
            null
        }
    }

    fun parseVolleyError(volleyError: VolleyError?): String {

        if (volleyError?.networkResponse == null) {
            return "No error object found"
        }

        //getInstance response body and parse with appropriate encoding
        return if (volleyError.networkResponse.data != null) {
            try {
                String(volleyError.networkResponse.data, charset("UTF-8"))
            } catch (e: UnsupportedEncodingException) {
                "Failed to parse error body"
            }

        } else {
            "Empty error response body"
        }
    }

    fun logError(volleyError: VolleyError){
        Log.e(LOG_TAG_ERROR, parseVolleyError(volleyError))
    }

    fun logEmptyResponse(){
        Log.e(LOG_TAG_ERROR, "Received an empty response (but no error)")
    }
}
