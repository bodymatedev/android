package de.bodymate.bodymateapp.api.response

/**
 * Created by Leonard on 20.08.2018.
 */

data class RBPlanPosition(val position: Int, val sets: List<RBPlanSet>?, val exercise: RBExercise?)