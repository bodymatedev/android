package de.bodymate.bodymateapp.api

import com.android.volley.DefaultRetryPolicy
import com.android.volley.Request
import com.android.volley.Response
import de.bodymate.bodymateapp.MyApplication
import org.json.JSONArray
import org.json.JSONObject

/**
 * Created by Leonard on 24.05.2018.
 */
internal object ApiClient {

    private const val baseUrlLocal = "http://localhost:8080/"
    private const val baseUrl = "http://bodymatend.eu-central-1.elasticbeanstalk.com/"
    private const val METHOD = Request.Method.POST

    private val retryPolicy = DefaultRetryPolicy(30000, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)

    fun sendRequest(subUrl: String, body: JSONObject, onResponse: Response.Listener<JSONObject>, onError: Response.ErrorListener): AuthenticatedRequest {

        //Build json object request
        val request = AuthenticatedRequest(METHOD, baseUrl + subUrl, body, onResponse, onError)

        //Disable quick retries by volley
        request.retryPolicy = retryPolicy

        //Add request to request queue
        MyApplication.requestQueue!!.add(request)

        return request
    }

    fun sendRequestKt(subUrl: String, body: JSONObject, onSuccess: (Response.Listener<JSONObject>), errorListener: Response.ErrorListener): AuthenticatedRequest {

        //Build json object request
        val request = AuthenticatedRequest(METHOD, baseUrl + subUrl, body, Response.Listener{ onSuccess.onResponse(it) }, errorListener)

        //Disable quick retries by volley
        request.retryPolicy = retryPolicy

        //Add request to request queue
        MyApplication.requestQueue!!.add(request)

        return request
    }

    fun sendArrayRequest(subUrl: String, body: JSONObject, onResponse: Response.Listener<JSONArray>, onError: Response.ErrorListener): AuthenticatedArrayRequest {

        //Build json object request
        val request = AuthenticatedArrayRequest(METHOD, baseUrl + subUrl, body, onResponse, onError)

        //Disable quick retries by volley
        request.retryPolicy = retryPolicy

        //Add request to request queue
        MyApplication.requestQueue!!.add(request)

        return request
    }


}