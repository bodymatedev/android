package de.bodymate.bodymateapp.api.response

import com.google.gson.annotations.SerializedName

/**
 * Created by Leonard on 20.08.2018.
 */

data class RBAbonnementType(val name: String?,
                            val description: String?,
                            @SerializedName("price_per_month") val pricePerMonth: Float,
                            val currency: String?)
