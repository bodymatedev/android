package de.bodymate.bodymateapp.api.response

import com.google.gson.annotations.SerializedName

/**
 * Created by Leonard on 01.06.2018.
 *
 */

data class RBUser(@SerializedName("fire_id") val fireId: String,
                  val email: String,
                  val firstname: String?,
                  val lastname: String?,
                  val nickname: String?,
                  val birthday: String?,
                  val sex: String?,
                  val goal: Int,
                  @SerializedName("picture") val pictureUrl: String?,
                  @SerializedName("registration_completed") val registrationCompleted: String,
                  val plans: List<RBPlan>?,
                  @SerializedName("latest_workout") val latestWorkout: RBWorkout?,
                  val achievements: List<RBAchievement>?,
                  val equipments: List<RBEquipment>?,
                  val schedules: List<RBSchedule>?,
                  val abonnements: List<RBAbonnement>?,
                  @SerializedName("workout_accumulates") val workoutAccumulates: RBWorkoutAccumulates?)