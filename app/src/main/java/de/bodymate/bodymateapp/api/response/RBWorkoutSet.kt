package de.bodymate.bodymateapp.api.response

data class RBWorkoutSet(val position: Int, val set: Int, val reps: Int)