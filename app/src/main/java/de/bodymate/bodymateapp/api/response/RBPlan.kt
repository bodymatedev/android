package de.bodymate.bodymateapp.api.response

import com.google.gson.annotations.SerializedName

/**
 * Created by Leonard on 20.08.2018.
 */

data class RBPlan(val id: Int,
                  @SerializedName("generated") val generated: String,
                  val number: Int,
                  val revision: Int,
                  val positions: List<RBPlanPosition>?)
