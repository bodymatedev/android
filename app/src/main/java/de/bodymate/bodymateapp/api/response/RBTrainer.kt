package de.bodymate.bodymateapp.api.response

/**
 * Created by Leonard on 20.08.2018.
 */

data class RBTrainer(val id: Int, val firstname: String?, val lastname: String?)