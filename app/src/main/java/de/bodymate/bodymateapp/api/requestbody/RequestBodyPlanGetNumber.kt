package de.bodymate.bodymateapp.api.requestbody

import com.google.gson.annotations.SerializedName

data class RequestBodyPlanGetNumber(@SerializedName("fire_id") val fireId: String?,
                                    @SerializedName("plan_id") val planId: Int)