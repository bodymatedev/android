package de.bodymate.bodymateapp.api

import com.google.gson.GsonBuilder
import com.google.gson.JsonSyntaxException
import org.json.JSONArray
import org.json.JSONObject

/**
 * Created by Leonard on 10.06.2018.
 */

class ResponseUnpacker<T>(private val responseClazz: Class<T>) {

    fun parse(response: JSONObject?): T? {

        if( response == null ) return null

        return try {
            gson.fromJson(response.toString(), responseClazz)
        }catch (e: JsonSyntaxException){
            null
        }
    }

    companion object {
        private val gson = GsonBuilder().create()
    }
}
