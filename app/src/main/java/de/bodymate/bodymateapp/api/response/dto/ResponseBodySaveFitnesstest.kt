package de.bodymate.bodymateapp.api.response.entity.dto

import de.bodymate.bodymateapp.api.response.RBPlan
import de.bodymate.bodymateapp.api.response.RBWorkout

data class ResponseBodySaveFitnesstest(val workout: RBWorkout?, val plans: List<RBPlan>?)