package de.bodymate.bodymateapp.api.requestbody

import com.google.gson.annotations.SerializedName

data class RequestBodyGetWorkoutsPage(
        @SerializedName("fire_id") val fireId: String,
        val page: Int,
        val size: Int)