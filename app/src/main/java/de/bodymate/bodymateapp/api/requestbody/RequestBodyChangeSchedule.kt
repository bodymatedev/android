package de.bodymate.bodymateapp.api.requestbody

import com.google.gson.annotations.SerializedName

data class RequestBodyChangeSchedule(@SerializedName("fire_id") val fireId: String,
                                     @SerializedName("new_schedule") val newSchedule: List<RequestEntityScheduling> )