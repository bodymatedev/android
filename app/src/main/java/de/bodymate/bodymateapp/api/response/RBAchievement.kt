package de.bodymate.bodymateapp.api.response

/**
 * Created by Leonard on 20.08.2018.
 */

data class RBAchievement(val id: Int, val name: String?, val description: String?, val date: String?)