package de.bodymate.bodymateapp.api

import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest

import org.json.JSONObject

import java.util.HashMap

/**
 * Created by Leonard on 24.05.2018.
 */

class AuthenticatedRequest internal constructor(method: Int, url: String, jsonRequest: JSONObject, listener: Response.Listener<JSONObject>, errorListener: Response.ErrorListener) : JsonObjectRequest(method, url, jsonRequest, listener, errorListener) {

    private val headers: MutableMap<String, String>

    init {

        headers = HashMap()
        headers["X-Api-Key"] = "ebd200dfd0ee14a8503dd041b2200fd5"
        headers["Content-Type"] = "application/json"
    }

    override fun getHeaders(): Map<String, String> {
        return headers
    }
}
