package de.bodymate.bodymateapp.api

import com.android.volley.AuthFailureError
import com.android.volley.NetworkResponse
import com.android.volley.ParseError
import com.android.volley.Response
import com.android.volley.toolbox.HttpHeaderParser
import com.android.volley.toolbox.JsonRequest
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject

import java.io.UnsupportedEncodingException
import java.util.HashMap

class AuthenticatedArrayRequest internal constructor(method: Int, url: String, jsonRequest: JSONObject, listener: Response.Listener<JSONArray>, errorListener: Response.ErrorListener) : JsonRequest<JSONArray>(method, url, jsonRequest.toString(), listener, errorListener) {

    private val headers: MutableMap<String, String>

    init {

        headers = HashMap()
        headers["X-Api-Key"] = "ebd200dfd0ee14a8503dd041b2200fd5"
        headers["Content-Type"] = "application/json"
    }

    override fun getHeaders(): Map<String, String> {
        return headers
    }

    override fun parseNetworkResponse(response: NetworkResponse): Response<JSONArray> {
        return try {
            val jsonString = String(response.data, charset(HttpHeaderParser.parseCharset(response.headers, JsonRequest.PROTOCOL_CHARSET)))
            Response.success(JSONArray(jsonString), HttpHeaderParser.parseCacheHeaders(response))
        } catch (e: UnsupportedEncodingException) {
            Response.error(ParseError(e))
        } catch (e: JSONException) {
            Response.error(ParseError(e))
        }
    }
}
