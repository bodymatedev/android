package de.bodymate.bodymateapp.api.requestbody

import com.google.gson.annotations.SerializedName

/**
 * Created by Leonard on 29.05.2018.
 */

data class RequestBodyRegister(@SerializedName("fire_id_token") var fireIdToken: String? = null,
                               var firstname: String? = "",
                               var lastname: String? = "",
                               var sex: String? = "",
                               var birthday: String? = "",
                               var picture: String? = "")