package de.bodymate.bodymateapp.api.requestbody

import com.google.gson.annotations.SerializedName
import de.bodymate.bodymateapp.api.requestbody.dto.RequestDtoWorkoutPosition

data class RequestBodyWorkoutSaveFitnesstest(@SerializedName("fire_id") val fireId: String?,
                                             @SerializedName("start") val datetimeStart: String?,
                                             @SerializedName("end") val datetimeEnd: String?,
                                             val positions: List<RequestDtoWorkoutPosition>?)