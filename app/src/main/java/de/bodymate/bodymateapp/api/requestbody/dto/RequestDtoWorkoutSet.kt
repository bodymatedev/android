package de.bodymate.bodymateapp.api.requestbody.dto

data class RequestDtoWorkoutSet(val set: Int, val reps: Int)