package de.bodymate.bodymateapp.api.response

import com.google.gson.annotations.SerializedName

data class RBWorkoutAccumulates( @SerializedName("minutes_trained") val minutesTrained: Int,
                                 @SerializedName("workouts_completed") val workoutsCompleted: Int)