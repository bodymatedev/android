package de.bodymate.bodymateapp.api.response

/**
 * Created by Leonard on 20.08.2018.
 */

data class RBEquipment(val id: Int, val name: String, val specification: String?)