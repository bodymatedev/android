package de.bodymate.bodymateapp.api.response

import com.google.gson.annotations.SerializedName

data class RBWorkout(val id: Int,
                     @SerializedName("plan_id") val planId: Int,
                     @SerializedName("start") val datetimeStart: String,
                     @SerializedName("end") val datetimeEnd: String,
                     val rating: Int,
                     val review: String?,
                     @SerializedName("planned_for") val plannedFor: String,
                     val sets: List<RBWorkoutSet>?)