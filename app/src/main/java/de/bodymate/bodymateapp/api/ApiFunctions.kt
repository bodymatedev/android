package de.bodymate.bodymateapp.api

import com.android.volley.Response
import com.android.volley.VolleyError
import com.google.gson.GsonBuilder
import de.bodymate.bodymateapp.CurrentUser
import de.bodymate.bodymateapp.api.requestbody.*
import de.bodymate.bodymateapp.api.response.*
import de.bodymate.bodymateapp.exception.JsonSerializeException
import de.bodymate.bodymateapp.extensions.fromJsonExt
import de.bodymate.bodymateapp.extensions.toObject
import de.bodymate.bodymateapp.extensions.toObjectList
import org.json.JSONArray
import org.json.JSONObject
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException
import kotlin.coroutines.suspendCoroutine

/**
 * Created by Leonard on 24.05.2018.
 */

object ApiFunctions {


    fun loginKt(requestBodyLogin: RequestBodyLogin, onResponse: (RBUser?) -> Unit, onError: (VolleyError) -> Unit) {

        val body = ApiUtils.serializeBody(requestBodyLogin) ?: return

        ApiClient.sendRequestKt("user/login", body, Response.Listener { onResponse(ResponseUnpacker(RBUser::class.java).parse(it)) } , Response.ErrorListener(onError))
    }

    fun register(requestBodyRegister: RequestBodyRegister, responseListener: Response.Listener<JSONObject>, errorListener: Response.ErrorListener) {

        val body = ApiUtils.serializeBody(requestBodyRegister) ?: return

        ApiClient.sendRequest("user/register", body, responseListener, errorListener)
    }

    fun registerDetail(requestBodyRegisterDetail: RequestBodyRegisterDetail, responseListener: Response.Listener<JSONObject>, errorListener: Response.ErrorListener) {

        val body = ApiUtils.serializeBody(requestBodyRegisterDetail) ?: return

        ApiClient.sendRequest("user/register/detail", body, responseListener, errorListener)
    }

    fun registerDevice(requestBodyRegisterDevice: RequestBodyRegisterDevice, onResponse: (RBRegisterDevice?) -> Unit, onError: (VolleyError) -> Unit){

        val body = ApiUtils.serializeBody(requestBodyRegisterDevice) ?: return

        ApiClient.sendRequest("user/register/device", body,
                Response.Listener { onResponse(ResponseUnpacker(RBRegisterDevice::class.java).parse(it)) },
                Response.ErrorListener(onError))
    }

    fun getAllEquipments(responseListener: Response.Listener<JSONArray>, errorListener: Response.ErrorListener) {

        ApiClient.sendArrayRequest("equipment/allEquipments", JSONObject(), responseListener, errorListener)
    }

    fun saveFitnesstest(requestBodyWorkoutSaveFitnesstest: RequestBodyWorkoutSaveFitnesstest, responseListener: Response.Listener<JSONObject>, errorListener: Response.ErrorListener) {

        val body = ApiUtils.serializeBody(requestBodyWorkoutSaveFitnesstest)

        ApiClient.sendRequest("workout/save/fitnesstest", body!!, responseListener, errorListener)
    }

    fun saveWorkout(requestBodyWorkoutSave: RequestBodyWorkoutSave, responseListener: Response.Listener<JSONObject>, errorListener: Response.ErrorListener) {

        val body = ApiUtils.serializeBody(requestBodyWorkoutSave)

        ApiClient.sendRequest("workout/save", body!!, responseListener, errorListener)
    }

    suspend fun getPlanNumber(planId: Int): RBPlanNumber? {

        return executeJsonRequest("plan/get/number", RequestBodyPlanGetNumber(CurrentUser.FIRE_ID!!, planId))?.toObject()
    }

    fun getPlansUpdated(requestBodyPlansUpdated: RequestBodyPlansUpdated, onResponse: (RBPlansUpdated?) -> Unit, onError: (VolleyError) -> Unit){

        val body = ApiUtils.serializeBody(requestBodyPlansUpdated) ?: return

        ApiClient.sendRequest("plan/updated", body,
                Response.Listener { onResponse(ResponseUnpacker(RBPlansUpdated::class.java).parse(it)) },
                Response.ErrorListener(onError))
    }

    suspend fun getWorkoutsAsPage(requestBodyGetWorkoutsPage: RequestBodyGetWorkoutsPage): List<RBWorkout>? {

        return executeArrayRequest("workout/page", requestBodyGetWorkoutsPage)?.toObjectList()
    }

    suspend fun workoutCount(requestBodyWorkoutCount: RequestBodyWorkoutCount): RBWorkoutCount? {

        return executeJsonRequest("workout/count", requestBodyWorkoutCount)?.toObject()
    }

    suspend fun changeSchedule(requestBodyChangeSchedule: RequestBodyChangeSchedule): List<RBSchedule>? {

        return executeArrayRequest("user/schedule/change", requestBodyChangeSchedule)?.toObjectList<List<RBSchedule>>()
    }

    private suspend fun executeJsonRequest(subUrl: String, requestBody: Any) = suspendCoroutine<JSONObject?> { continuation ->

        val onError = Response.ErrorListener {
            ApiUtils.logError(it)
            continuation.resume(null)
        }

        val onResponse = Response.Listener<JSONObject> {
            continuation.resume(it)
        }

        val jsonBody = ApiUtils.serializeBody(requestBody)

        if( jsonBody == null ){
            onError.onErrorResponse(VolleyError("Failed to serialize request body to json format"))
            return@suspendCoroutine
        }

        ApiClient.sendRequest(subUrl, jsonBody, onResponse, onError)
    }

    private suspend fun executeArrayRequest(subUrl: String, requestBody: Any) = suspendCoroutine<JSONArray?> { continuation ->

        val onError = Response.ErrorListener {
            ApiUtils.logError(it)
            continuation.resume(null)
        }

        val onResponse = Response.Listener<JSONArray> {
            continuation.resume(it)
        }

        val jsonBody = ApiUtils.serializeBody(requestBody)

        if( jsonBody == null ){
            onError.onErrorResponse(VolleyError("Failed to serialize request body to json format"))
            return@suspendCoroutine
        }

        ApiClient.sendArrayRequest(subUrl, jsonBody, onResponse, onError)
    }
}