package de.bodymate.bodymateapp

import android.app.Application
import com.google.firebase.auth.FirebaseAuth
import de.bodymate.bodymateapp.data.service.*
import de.bodymate.bodymateapp.notification.NotificationSchedulerService
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

object CurrentUser {

    var FIRE_ID: String? = null

    fun logout(application: Application, onCompleted: () -> Unit){

        val scheduleService = ScheduleService.getInstance(application)

        GlobalScope.launch{

            NotificationSchedulerService.cancelAllWorkoutReminderNotification(application.applicationContext, scheduleService.all())

            // Delete all local data
            AchievementService.getInstance(application).deleteAll()
            WorkoutService.getInstance(application).deleteAll()
            PlanService.getInstance(application).deleteAll()
            ExerciseService.getInstance(application).deleteAll()
            EquipmentService.getInstance(application).deleteAll()
            scheduleService.deleteAll()
            UserService.getInstance(application).deleteUser()

            // Firebase sign out
            FirebaseAuth.getInstance().signOut()

            // Unset static Fire-Id
            FIRE_ID = null

        }.invokeOnCompletion {
            onCompleted()
        }
    }
}