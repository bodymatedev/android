package de.bodymate.bodymateapp

/**
 * Created by Leonard on 22.08.2018.
 */

object RunConfig {

    const val NETWORK_MODE_PROD = 1
    const val NETWORK_MODE_LOCAL = 2

    const val NETWORK_MODE = 1
}
