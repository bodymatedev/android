package de.bodymate.bodymateapp.fragment

import android.app.DatePickerDialog
import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import de.bodymate.bodymateapp.R
import de.bodymate.bodymateapp.gui.MyTextWatcher
import de.bodymate.bodymateapp.util.C
import de.bodymate.bodymateapp.util.DateUtils
import kotlinx.android.synthetic.main.f_register_detail_general.*
import lombok.NoArgsConstructor

/**
 * Created by Leonard on 08.04.2018.
 */
@NoArgsConstructor
class FragmentRegisterDetailGeneral : Fragment(), IRegisterFormFragment {

    private var mContext: Context? = null

    var gender = C.MALE
        private set

    private val myTextWatcherFirstname = MyTextWatcher(object : MyTextWatcher.OnTextChanged {
        override fun onTextChanged(newText: String) {
            tilRegisterFirstname.error = null
        }
    })

    private val myTextWatcherLastname = MyTextWatcher(object : MyTextWatcher.OnTextChanged {
        override fun onTextChanged(newText: String) {
            tilRegisterLastname.error = null
        }
    })

    private val myTextWatcherBirthday = MyTextWatcher(object : MyTextWatcher.OnTextChanged {
        override fun onTextChanged(newText: String) {
            tilRegisterBirthday.error = null
        }
    })

    private val onDateSetListener = DatePickerDialog.OnDateSetListener { _, year, monthOfYear, dayOfMonth ->
        etRegisterBirthday.setText(DateUtils.getAndroidDate(year, monthOfYear + 1, dayOfMonth))
        tilRegisterBirthday.error = null
    }

    val firstname: String
        get() = etRegisterFirstname.text.toString()

    val lastname: String
        get() = etRegisterLastname.text.toString()

    val birthday: String?
        get() = etRegisterBirthday.text.toString()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
            inflater.inflate(R.layout.f_register_detail_general, container, false)

    override fun onAttach(context: Context?) {

        super.onAttach(context)
        mContext = context
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        super.onViewCreated(view, savedInstanceState)

        if (arguments != null) {

            //Fill in firstname
            val firstname = arguments!!.getString(getString(R.string.bkey_firstname))
            if (firstname != null) {
                etRegisterFirstname.setText(firstname)
            }

            //Fill in lastname
            val lastname = arguments!!.getString(getString(R.string.bkey_lastname))
            if (lastname != null) {
                etRegisterLastname.setText(lastname)
            }

            //Fill in birthday
            val birthday = arguments!!.getString(getString(R.string.bkey_birthday))
            if (birthday != null) {
                etRegisterBirthday.setText(birthday)
            }

            //Set gender
            val gender = arguments!!.getString(getString(R.string.bkey_gender))
            if (gender != null) {
                this.gender = gender
                flipToGender(this.gender)
            }
        }

        etRegisterBirthday.setOnClickListener(onClickBirthday)
        ivRegisterMale.setOnClickListener(onClickMale)
        ivRegisterFemale.setOnClickListener(onClickFemale)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {

        super.onActivityCreated(savedInstanceState)

        //Restore selected gender
        if (savedInstanceState != null) {
            val savedSelectedGender = savedInstanceState.getString(getString(R.string.bkey_selected_gender))
            if (savedSelectedGender != null) {
                gender = savedSelectedGender
                flipToGender(gender)
            }
        }

        etRegisterFirstname.addTextChangedListener(myTextWatcherFirstname)
        etRegisterLastname.addTextChangedListener(myTextWatcherLastname)
        etRegisterBirthday.addTextChangedListener(myTextWatcherBirthday)
    }

    private val onClickBirthday = View.OnClickListener {

        val birthday = (it as TextView).text.toString()

        if (birthday.isNotEmpty()) {
            DateUtils.showDatePicker(mContext!!, birthday, onDateSetListener)
        } else {
            DateUtils.showDatePicker(mContext!!, onDateSetListener)
        }
    }

    private val onClickMale = View.OnClickListener {
        flipToGender(C.MALE)
    }

    private val onClickFemale = View.OnClickListener {
        flipToGender(C.FEMALE)
    }

    private fun flipToGender(gender: String) {

        val male = gender == C.MALE

        ivRegisterMale.setColorFilter(ContextCompat.getColor(mContext!!, if (male) R.color.colorPrimary else R.color.grey800), android.graphics.PorterDuff.Mode.SRC_IN)
        ivRegisterFemale.setColorFilter(ContextCompat.getColor(mContext!!, if (male) R.color.grey800 else R.color.colorPrimary), android.graphics.PorterDuff.Mode.SRC_IN)
        tvRegisterGender.setText(if (male) R.string.gender_male else R.string.gender_female)
        this.gender = gender
    }

    override fun checkForm(): Boolean {

        //Check firstname length
        if (etRegisterFirstname.text.toString().trim().isEmpty()) {
            tilRegisterFirstname.error = getString(R.string.error_mandatory_field)
            return false
        }

        if (etRegisterFirstname.text.toString().length > 30) {
            tilRegisterFirstname.error = getString(R.string.error_firstname_length)
            return false
        }

        //Check lastname length
        if (etRegisterLastname.text.toString().trim().isEmpty()) {
            tilRegisterLastname.error = getString(R.string.error_mandatory_field)
            return false
        }

        if (etRegisterLastname.text.toString().trim { it <= ' ' }.length > 60) {
            tilRegisterLastname.error = getString(R.string.error_lastname_length)
            return false
        }

        val birthday = etRegisterBirthday.text.toString()

        if (birthday.isNotEmpty()) {
            if (DateUtils.getAge(DateUtils.getCalendarDayFromAndroidDate(birthday)) < 16) {
                tilRegisterBirthday.error = getString(R.string.birthday_to_young)
                return false
            }
        } else {
            tilRegisterBirthday.error = getString(R.string.birthday_invalid)
            return false
        }

        return true
    }

    override fun onSaveInstanceState(outState: Bundle) {

        super.onSaveInstanceState(outState)

        //Save selected gender
        outState.putString(getString(R.string.bkey_selected_gender), gender)
    }

    override fun onStop() {

        super.onStop()

        etRegisterFirstname.removeTextChangedListener(myTextWatcherFirstname)
        etRegisterLastname.removeTextChangedListener(myTextWatcherLastname)
        etRegisterBirthday.removeTextChangedListener(myTextWatcherBirthday)
    }
}
