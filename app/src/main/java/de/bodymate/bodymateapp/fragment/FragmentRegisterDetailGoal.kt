package de.bodymate.bodymateapp.fragment

import android.animation.Animator
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.OvershootInterpolator
import de.bodymate.bodymateapp.R
import kotlinx.android.synthetic.main.f_register_detail_goal.*
import kotlinx.android.synthetic.main.goal_seek_bar.*
import kotlinx.android.synthetic.main.goalbar_end.*
import kotlinx.android.synthetic.main.goalbar_start.*

/**
 * Created by Leonard on 08.04.2018.
 */
class FragmentRegisterDetailGoal : Fragment(), IRegisterFormFragment {

    private val goalBarElements: MutableList<View> = mutableListOf()
    private val goalBarDots: MutableList<View> = mutableListOf()

    var selectedGoal = 4
        private set

    private var runningShrink = false
    private var runningGrow = false

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        return inflater.inflate(R.layout.f_register_detail_goal, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        super.onViewCreated(view, savedInstanceState)

        goalBarElements.addAll(listOf(vGoalBar0, vGoalBar1, vGoalBar2, vGoalBar3, vGoalBar4, vGoalBar5, vGoalBar6, vGoalBar7, vGoalBar8))
        goalBarDots.addAll(listOf(vGoalBarDot0, vGoalBarDot1, vGoalBarDot2, vGoalBarDot3, vGoalBarDot4, vGoalBarDot5, vGoalBarDot6, vGoalBarDot7, vGoalBarDot8))

        goalBarElements.forEach { it.setOnClickListener(onClickGoalBar) }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {

        super.onActivityCreated(savedInstanceState)

        if (savedInstanceState != null) {
            val lastSelectedGoal = savedInstanceState.getInt(getString(R.string.bkey_selected_goal), -1)
            if (lastSelectedGoal != -1) {
                selectedGoal = lastSelectedGoal
            }
        }

        goalBarDots[selectedGoal]
                .animate()
                .scaleXBy(DOT_SCALE)
                .scaleYBy(DOT_SCALE)
                .setInterpolator(OvershootInterpolator())
                .duration = SCALE_DURATION

        goalBarDots[selectedGoal].isSelected = true


        when {
            selectedGoal <= 2 -> tvRegisterDetailGoalDesc.text = getString(R.string.goal_maximal)
            selectedGoal <= 5 -> tvRegisterDetailGoalDesc.text = getString(R.string.goal_muscle)
            else -> tvRegisterDetailGoalDesc.text = getString(R.string.goal_endurance)
        }
    }

    private val onClickGoalBar = View.OnClickListener { goalBarElement ->
        val goalValue = Integer.valueOf(goalBarElement.tag.toString())
        select(goalValue)
    }

    private fun select(dotId: Int) {

        if (runningGrow || runningShrink || dotId == selectedGoal) {
            return
        }

        runningShrink = true
        goalBarDots[selectedGoal].isSelected = false

        goalBarDots[selectedGoal]
                .animate()
                .scaleXBy(-DOT_SCALE)
                .scaleYBy(-DOT_SCALE)
                .setDuration(SCALE_DURATION)
                .setListener(object : Animator.AnimatorListener {

                    override fun onAnimationEnd(animation: Animator) {
                        runningShrink = false
                    }

                    override fun onAnimationStart(animation: Animator) {}
                    override fun onAnimationCancel(animation: Animator) {}
                    override fun onAnimationRepeat(animation: Animator) {}
                })

        runningGrow = true
        goalBarDots[dotId].isSelected = true

        goalBarDots[dotId]
                .animate()
                .scaleXBy(DOT_SCALE)
                .scaleYBy(DOT_SCALE)
                .setInterpolator(OvershootInterpolator(SCALE_TENSION))
                .setDuration(SCALE_DURATION)
                .setListener(object : Animator.AnimatorListener {

                    override fun onAnimationEnd(animation: Animator) {
                        runningGrow = false
                    }

                    override fun onAnimationStart(animation: Animator) {}
                    override fun onAnimationCancel(animation: Animator) {}
                    override fun onAnimationRepeat(animation: Animator) {}
                })

        selectedGoal = dotId

        when {
            dotId <= 2 -> tvRegisterDetailGoalDesc.text = getString(R.string.goal_maximal)
            dotId <= 5 -> tvRegisterDetailGoalDesc.text = getString(R.string.goal_muscle)
            else -> tvRegisterDetailGoalDesc.text = getString(R.string.goal_endurance)
        }
    }

    override fun checkForm(): Boolean = true

    override fun onSaveInstanceState(outState: Bundle) {

        super.onSaveInstanceState(outState)

        //Save current selected goal
        outState.putInt(getString(R.string.bkey_selected_goal), selectedGoal)
    }

    companion object {

        private const val DOT_SCALE = .8f
        private const val SCALE_DURATION = 250L
        private const val SCALE_TENSION = 5f
    }
}
