package de.bodymate.bodymateapp.fragment

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import de.bodymate.bodymateapp.R
import de.bodymate.bodymateapp.activity.AchievementActivity
import de.bodymate.bodymateapp.activity.MainActivity
import de.bodymate.bodymateapp.activity.SettingsActivity
import de.bodymate.bodymateapp.extensions.string
import de.bodymate.bodymateapp.gui.RecyclerViewDecoratorHorizontal
import de.bodymate.bodymateapp.gui.adapter.AdapterProfileWeekdays
import de.bodymate.bodymateapp.gui.adapter.AdapterProfileWorkoutTimeline
import de.bodymate.bodymateapp.paging.Status
import de.bodymate.bodymateapp.viewmodel.ViewModelFragmentProfile
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.f_profile.*


/**
 * Created by Leonard on 23.03.2017.
 *
 */

class FragmentProfile : Fragment() {

    private lateinit var mContext: Context
    private lateinit var adapterWeekday: AdapterProfileWeekdays
    private lateinit var adapterTimeline: AdapterProfileWorkoutTimeline

    private val disposables = CompositeDisposable()

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        mContext = context!!
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        return inflater.inflate(R.layout.f_profile, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        super.onViewCreated(view, savedInstanceState)

        initWeekdayView()
        initWorkoutTimeline()

        ibSettings.setOnClickListener(onClickSettings)
        ivProfileAchievements.setOnClickListener(onClickAchievements)
        tvProfileAchievements.setOnClickListener(onClickAchievements)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {

        super.onActivityCreated(savedInstanceState)

        val vModel = ViewModelProviders.of(this).get(ViewModelFragmentProfile::class.java)

        // Subscribe to the current user
        vModel.user.observe(this, Observer {

            if( it != null ) {

                // Display the users name
                tvProfileName.text = if( it.firstname.isNullOrBlank().not() || it.lastname.isNullOrBlank().not() ) {
                    mContext.resources.getString(R.string.full_name, it.firstname, it.lastname)
                }else{
                    mContext.string(R.string.unknown)
                }

                if (it.minutesTrained != null && it.minutesTrained > 0) {
                    tvProfileMinutesTrained.visibility = View.VISIBLE
                    tvProfileMinutesTrained.text = mContext.resources.getString(R.string.minutes_trained, it.minutesTrained.toString())
                }

                // Select profile picture or default picture
                val glideReqBuilder = if( it.pictureUrl.isNullOrBlank()){
                    Glide.with(this@FragmentProfile).load(R.drawable.placeholder_profile_picture)
                } else {
                    Glide.with(this@FragmentProfile).load(it.pictureUrl)
                }

                // Display picture
                glideReqBuilder.apply(profilePictureGlideOptions).into(ivProfilePicture)
            }
        })

        // Select users schedule page local db
        vModel.schedules.observe(this, Observer {

            if( !it.isNullOrEmpty() ) {
                adapterWeekday.setSchedule(it)
            }else {
                tvProfileErrorSchedules.visibility = View.VISIBLE
                rvProfileWeekdays.visibility = View.INVISIBLE
            }
        })

        // Observe allEquipments workouts by date, and inform the paging adapter
        vModel.allWorkoutsByDate.observe(this, Observer(adapterTimeline::submitList))

        // Observe the network state of the workout pager
        vModel.networkStateWorkoutPaging.observe(this, Observer {
            if( it?.status == Status.RUNNING ){
                pbProfileTimelineLoading.visibility = View.VISIBLE
            }else{
                pbProfileTimelineLoading.visibility = View.INVISIBLE
            }
        })
    }

    private fun initWeekdayView() {

        adapterWeekday = AdapterProfileWeekdays(activity as MainActivity)

        val layoutManager = LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)

        rvProfileWeekdays.itemAnimator = DefaultItemAnimator()
        rvProfileWeekdays.layoutManager = layoutManager
        rvProfileWeekdays.addItemDecoration(RecyclerViewDecoratorHorizontal(24, includeTopBottom = false))
        rvProfileWeekdays.adapter = adapterWeekday
    }

    private fun initWorkoutTimeline(){

        adapterTimeline = AdapterProfileWorkoutTimeline(mContext)

        val layoutManager = LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)

        rvProfileTimeline.itemAnimator = DefaultItemAnimator()
        rvProfileTimeline.layoutManager = layoutManager
        rvProfileTimeline.addItemDecoration(RecyclerViewDecoratorHorizontal(16, includeTopBottom = false))
        rvProfileTimeline.adapter = adapterTimeline
    }

    private val onClickSettings = View.OnClickListener {

        val intent = Intent(mContext, SettingsActivity::class.java)
        startActivity(intent)
        activity?.overridePendingTransition(R.anim.slide_in_from_right, R.anim.stay)
    }

    private val onClickAchievements = View.OnClickListener {

        val intent = Intent(mContext, AchievementActivity::class.java)
        startActivity(intent)
        activity?.overridePendingTransition(R.anim.slide_in_from_right, R.anim.stay)
    }

    override fun onDestroy() {

        disposables.clear()
        super.onDestroy()
    }

    companion object {

        private val profilePictureGlideOptions = RequestOptions()
                .placeholder(R.drawable.placeholder_profile_picture)
                .circleCrop()
                .error(R.drawable.placeholder_profile_picture)
    }
}