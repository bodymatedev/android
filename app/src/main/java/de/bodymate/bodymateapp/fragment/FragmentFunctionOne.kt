package de.bodymate.bodymateapp.fragment

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView

import de.bodymate.bodymateapp.R
import de.bodymate.bodymateapp.extensions.string

class FragmentFunctionOne : Fragment() {

    companion object {
        fun newInstance() = FragmentFunctionOne()
    }

    private lateinit var mViewModel: FragmentFunctionOneViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.f_launch_function, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        mViewModel = ViewModelProviders.of(this).get(FragmentFunctionOneViewModel::class.java)
        view?.findViewById<TextView>(R.id.tvLaunchFunctionHeader)?.text = mViewModel.textHeader
        view?.findViewById<TextView>(R.id.tvLaunchFunctionDesc)?.text = mViewModel.textDescription
    }

    class FragmentFunctionOneViewModel(application: Application): AndroidViewModel(application) {

        val textHeader = application.applicationContext.string(R.string.function_plan)
        val textDescription = application.applicationContext.string(R.string.function_plan_description)
    }
}
