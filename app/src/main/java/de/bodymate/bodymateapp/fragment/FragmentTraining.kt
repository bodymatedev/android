package de.bodymate.bodymateapp.fragment

import android.app.NotificationManager
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.app.ActivityOptionsCompat
import android.support.v4.app.Fragment
import android.support.v4.util.Pair
import android.support.v7.widget.CardView
import android.support.v7.widget.DefaultItemAnimator
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.android.volley.VolleyError
import com.prolificinteractive.materialcalendarview.CalendarDay
import com.prolificinteractive.materialcalendarview.OnDateSelectedListener
import de.bodymate.bodymateapp.R
import de.bodymate.bodymateapp.activity.WorkoutActivity
import de.bodymate.bodymateapp.api.ApiFunctions
import de.bodymate.bodymateapp.api.ApiUtils
import de.bodymate.bodymateapp.data.repository.entity.RoomPlan
import de.bodymate.bodymateapp.data.repository.entity.RoomSchedule
import de.bodymate.bodymateapp.data.repository.entity.RoomWorkout
import de.bodymate.bodymateapp.data.service.PlanService
import de.bodymate.bodymateapp.data.service.ScheduleService
import de.bodymate.bodymateapp.data.service.WorkoutService
import de.bodymate.bodymateapp.extensions.string
import de.bodymate.bodymateapp.gui.RecyclerViewDecoratorVertical
import de.bodymate.bodymateapp.gui.SmoothScrollLayoutManager
import de.bodymate.bodymateapp.gui.adapter.AdapterTrainingTimeline
import de.bodymate.bodymateapp.gui.adapter.datamodel.TimelinePlan
import de.bodymate.bodymateapp.gui.calendar.CalendarEventDecoratorFactory
import de.bodymate.bodymateapp.gui.popup.PopupPlanDetail
import de.bodymate.bodymateapp.notification.WorkoutReminderPublisher
import de.bodymate.bodymateapp.util.*
import de.bodymate.bodymateapp.util.C.PLAN_ID_FITNESSTEST
import de.bodymate.bodymateapp.util.C.PLAN_ONE
import de.bodymate.bodymateapp.util.C.PLAN_TWO
import de.bodymate.bodymateapp.viewmodel.ViewModelFragmentTraining
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.subscribeBy
import kotlinx.android.synthetic.main.f_training.*
import kotlinx.coroutines.*
import java.util.*
import kotlin.coroutines.CoroutineContext

/**
 * Created by Leonard on 23.03.2017.
 */


@ExperimentalCoroutinesApi
class FragmentTraining : Fragment(), CoroutineScope {

    private lateinit var mJob: Job
    override val coroutineContext: CoroutineContext
        get() = mJob + Dispatchers.Main

    private var mContext: Context? = null

    private lateinit var vModel: ViewModelFragmentTraining
    private lateinit var adapterTrainingTimeline: AdapterTrainingTimeline
    private lateinit var smoothLayoutManager: SmoothScrollLayoutManager

    private val disposables = CompositeDisposable()

    val parentRootView: ViewGroup
        get() = activity!!.findViewById(android.R.id.content)

    override fun onAttach(context: Context?) {

        super.onAttach(context)
        mContext = context
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
            inflater.inflate(R.layout.f_training, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        super.onViewCreated(view, savedInstanceState)

        mJob = Job()

        //Initialize calendar top view
        initCalendar()

        //Initialize timeline view
        initTimeline()

        ivPlanLoadingError.setOnClickListener(onClickLoadingError)
        tvPlanLoadingError.setOnClickListener(onClickLoadingError)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {

        super.onActivityCreated(savedInstanceState)

        handleArguments()

        vModel = ViewModelProviders.of(this).get(ViewModelFragmentTraining::class.java)

        //Observe the latest workout (to keep the plan order correct)
        vModel.latest3Workouts.observe(this, Observer { roomWorkouts ->

            if (roomWorkouts != null && roomWorkouts.isNotEmpty()) {

                // Check if only the fitnesstest was completed
                if( roomWorkouts.size == 1 && roomWorkouts.first().planId == PLAN_ID_FITNESSTEST ){
                    onWorkoutsChanged(roomWorkouts)
                    return@Observer
                }

                val latestRealWorkout = roomWorkouts.firstOrNull { it.planId != PLAN_ID_FITNESSTEST }

                // No workout besides the fitnesstest was completed
                if( latestRealWorkout == null ){
                    onNoDataAvailable()
                    return@Observer
                }

                launch {

                    val latestPlanNumber = ApiFunctions.getPlanNumber(latestRealWorkout.planId)?.number

                    Log.d("COUROUTINES", "latestPlanNumber = $latestPlanNumber")

                    if( latestPlanNumber != null && ( latestPlanNumber == PLAN_ONE || latestPlanNumber == PLAN_TWO ) ){
                        onWorkoutsChanged(roomWorkouts, latestPlanNumber)
                    }else{
                        onWorkoutsChanged()
                    }
                }
            } else {
                onWorkoutsChanged()
            }
        })
    }

    private fun handleArguments(){

        // Check if the activity was launched through the workout reminder notification action "Start now"
        val startWorkoutNow = arguments?.getString(activity!!.string(R.string.notif_workout_reminder_action_start_now_extra_key)) ?: ""

        if( startWorkoutNow == C.TRUE ){

            considerStartingTodaysWorkout()

            //Remove the workout reminder notification
            (activity!!.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager)
                    .cancel(WorkoutReminderPublisher.NOTIFICATION_ID)
        }
    }

    private fun considerStartingTodaysWorkout(){

        val scheduleService = ScheduleService.getInstance(activity!!.application)
        val workoutService = WorkoutService.getInstance(activity!!.application)
        val planService = PlanService.getInstance(activity!!.application)

        launch {

            val deffWorkoutIsPlannedToday = async(Dispatchers.Default) {
                scheduleService.all().any { it.weekday == DateUtils.getWeekday() }
            }

            val deffWorkoutsCompletedToday = async(Dispatchers.Default) {
                workoutService.countTodays()
            }

            val deffHasPlansBesidesFitnesstest = async(Dispatchers.Default) {
                planService.hasPlansBesidesFitnesstest()
            }

            // Leave if:
            // - if there is now workout scheduled for today
            // - the user already completed any workout for today
            // - if the user has no plan besides the fitnesstest
            if( !deffWorkoutIsPlannedToday.await() || deffWorkoutsCompletedToday.await() > 0 || !deffHasPlansBesidesFitnesstest.await() ){
                this.cancel()
                return@launch
            }

            // Determine the plan id to start
            val latestWorkout = withContext(Dispatchers.Default) {
                workoutService.latest3AsCor().firstOrNull { it.planId != PLAN_ID_FITNESSTEST }
            }

            if( latestWorkout != null ){

                val latestPlanNumber = ApiFunctions.getPlanNumber(latestWorkout.planId)?.number

                if( latestPlanNumber != null && ( latestPlanNumber == PLAN_ONE || latestPlanNumber == PLAN_TWO ) ){

                    val nextPlan = withContext(Dispatchers.Default) {
                        planService.getPlanByNumberAsCor( if( latestPlanNumber == PLAN_ONE ) PLAN_TWO else PLAN_ONE )
                    }

                    startWorkout(nextPlan)
                }

            }else{

                // The user has plans, but never completed a workout before, so just start Plan 1
                startWorkout(planService.getPlanOneAsCor())
            }
        }
    }

    private fun startWorkout(plan: RoomPlan?){

        if( plan != null ){

            val startWorkoutIntent = Intent(activity!!, WorkoutActivity::class.java).apply {

                putExtra(WorkoutActivity.EXTRA_PLAN_ID, plan.id)
                putExtra(WorkoutActivity.EXTRA_PLAN_PLANNED_FOR, todayAsString())
            }

            startActivity(startWorkoutIntent)
        }
    }

    private fun onWorkoutsChanged(latestWorkouts: List<RoomWorkout>? = null, latestPlanNumber: Int = -1) {

        //Close allEquipments pending subscriptions
        disposables.clear()

        val disposableTimelineData = vModel.timeLineData.subscribeBy(
                onSuccess = {
                    updateTimeline(it, latestWorkouts, latestPlanNumber)
                    scrollToDay(cvWorkoutCalendar.selectedDate)
                },
                onError = {
                    Log.e(Tags.REACTIVE, it.message)
                    onNoDataAvailable()
                }
        )

        disposables.add(disposableTimelineData)
    }

    private fun initCalendar() {

        cvWorkoutCalendar.state().edit()
                .setMinimumDate(DateUtils.thisMonday())
                .setMaximumDate(DateUtils.getDayByWeekdayPlusWeekOffset(Calendar.SUNDAY, 2))
                .setFirstDayOfWeek(Calendar.MONDAY)
                .commit()

        cvWorkoutCalendar.currentDate = CalendarDay.today()
        cvWorkoutCalendar.selectedDate = CalendarDay.today()
        cvWorkoutCalendar.setOnDateChangedListener(onDateSelectedListener)
    }

    private fun initTimeline() {

        adapterTrainingTimeline = AdapterTrainingTimeline(this)
        smoothLayoutManager = SmoothScrollLayoutManager(mContext)
        rvTrainingTimeline.layoutManager = smoothLayoutManager
        rvTrainingTimeline.itemAnimator = DefaultItemAnimator()
        rvTrainingTimeline.addItemDecoration(RecyclerViewDecoratorVertical(adapterTrainingTimeline.itemCount, 0, false))
        rvTrainingTimeline.adapter = adapterTrainingTimeline
    }

    private fun fillCalendar(schedules: List<RoomSchedule>) {

        cvWorkoutCalendar.removeDecorators()
        cvWorkoutCalendar.addDecorator(CalendarEventDecoratorFactory.getDecorators(schedules, Util.color(mContext!!, R.color.colorPrimary)))
    }

    private fun scrollToDay(day: CalendarDay) {

        val nextTimelineItem = adapterTrainingTimeline.getTimelinePosition(day)
        if (nextTimelineItem > -1 && nextTimelineItem != smoothLayoutManager.findFirstCompletelyVisibleItemPosition()) {
            rvTrainingTimeline.smoothScrollToPosition(nextTimelineItem)
        }
    }

    private val onClickLoadingError = View.OnClickListener {

        pbWorkoutsLoading.visibility = View.VISIBLE
        ivPlanLoadingError.visibility = View.GONE
        tvPlanLoadingError.visibility = View.GONE
    }

    private fun updateTimeline(timeLineDataModel: ViewModelFragmentTraining.TimeLineDataModel, latestWorkouts: List<RoomWorkout>?, latestPlanNumber: Int) {

        pbWorkoutsLoading.visibility = View.GONE
        ivPlanLoadingError.visibility = View.GONE
        tvPlanLoadingError.visibility = View.GONE

        if (timeLineDataModel.plans.size > 1) {
            fillCalendar(timeLineDataModel.schedules)
        }

        adapterTrainingTimeline.updateView(timeLineDataModel.plans, timeLineDataModel.exercises, timeLineDataModel.schedules, latestWorkouts, latestPlanNumber)
    }

    private fun onNoDataAvailable(volleyError: VolleyError? = null) {

        if (volleyError != null) {
            Log.e(ApiUtils.LOG_TAG_ERROR, ApiUtils.parseVolleyError(volleyError))
        }

        pbWorkoutsLoading.visibility = View.GONE
        ivPlanLoadingError.visibility = View.VISIBLE
        tvPlanLoadingError.visibility = View.VISIBLE
    }

    fun onClickWorkoutStart(popupPlanDetail: PopupPlanDetail, timelinePlan: TimelinePlan) {

        popupPlanDetail.close()
        onClickWorkoutStart(null, timelinePlan)
    }

    fun onClickWorkoutStart(cvWorkoutTimelinePlan: CardView?, timelinePlan: TimelinePlan) {

        val intent = Intent(activity!!, WorkoutActivity::class.java).apply {

            putExtra(WorkoutActivity.EXTRA_PLAN_ID, timelinePlan.plan.id)
            putExtra(WorkoutActivity.EXTRA_PLAN_PLANNED_FOR, DateUtils.getSqlDateFromCalendarDay(timelinePlan.date))
        }

        if( cvWorkoutTimelinePlan != null ){

            /**
             * Start Workout activity with transition
             */

            val transitionNameCard = getString(R.string.transition_workout_card_to_activity)

            val sharedElement1:Pair<View, String> = Pair(cvWorkoutTimelinePlan, transitionNameCard)

            val options = ActivityOptionsCompat.makeSceneTransitionAnimation(activity!!, sharedElement1)

            ActivityCompat.startActivity(activity!!, intent, options.toBundle())

        }else {

            /**
             * Start workout activity without any transition
             */

            startActivity(intent)
        }
    }

    private val onDateSelectedListener = OnDateSelectedListener { _, date, _ -> scrollToDay(date) }

    override fun onStop() {
        super.onStop()
        disposables.clear()
    }

    override fun onDestroy() {
        super.onDestroy()
        mJob.cancel()
    }
}
