package de.bodymate.bodymateapp.fragment

/**
 * Created by Leonard on 08.04.2018.
 */
interface IRegisterFormFragment {

    fun checkForm(): Boolean
}
