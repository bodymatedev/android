package de.bodymate.bodymateapp.fragment

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import de.bodymate.bodymateapp.R
import de.bodymate.bodymateapp.activity.RegisterDetailActivity
import de.bodymate.bodymateapp.gui.adapter.AdapterEquipments
import de.bodymate.bodymateapp.gui.popup.PopupRegisterScheduleTooLittle
import de.bodymate.bodymateapp.gui.popup.PopupRegisterScheduleWarning
import de.bodymate.bodymateapp.util.C
import de.bodymate.bodymateapp.viewmodel.ViewModelFragmentRegisterDetailEquipments
import kotlinx.android.synthetic.main.f_register_detail_schedule_equipment.*
import kotlinx.android.synthetic.main.weekday_selector.*
import java.util.*
import kotlin.collections.HashSet

/**
 * Created by Leonard on 08.04.2018.
 */
class FragmentRegisterDetailScheduleEquipment : Fragment(), IRegisterFormFragment {

    private val weekdayViews: MutableList<FrameLayout> = mutableListOf()
    private val weekdayIndicators: MutableList<View> = mutableListOf()

    private lateinit var mViewModelEquipments: ViewModelFragmentRegisterDetailEquipments

    val selectedWeekdays: MutableSet<Int> = HashSet(Arrays.asList(0, 2, 4))

    private lateinit var adapterEquipments: AdapterEquipments
    private lateinit var registerDetailActivity: RegisterDetailActivity
    private lateinit var fragmentContainer: ViewGroup

    override fun onAttach(context: Context) {

        super.onAttach(context)

        registerDetailActivity = context as RegisterDetailActivity
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val view = inflater.inflate(R.layout.f_register_detail_schedule_equipment, container, false)
        this.fragmentContainer = container!!
        return view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {

        super.onActivityCreated(savedInstanceState)

        if (savedInstanceState != null) {

            selectedWeekdays.clear()

            week.forEachIndexed { i, day ->
                if (savedInstanceState.getString(day, C.FALSE) == C.TRUE) {
                    selectedWeekdays.add(i)
                }
            }

            weekdayIndicators.forEachIndexed { index, indicatorView ->

                if( selectedWeekdays.contains(index) ){
                    indicatorView.setBackgroundResource(R.drawable.weekday_circle_selected)
                }else{
                    indicatorView.setBackgroundResource(R.drawable.weekday_circle_unselected)
                }
            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        super.onViewCreated(view, savedInstanceState)

        weekdayIndicators.addAll(listOf(vWeekdayMonday, vWeekdayTuesday, vWeekdayWednesday, vWeekdayThursday, vWeekdayFriday, vWeekdaySaturday, vWeekdaySunday))
        weekdayViews.addAll(listOf(flWeekdayMonday, flWeekdayTuesday, flWeekdayWednesday, flWeekdayThursday, flWeekdayFriday, flWeekdaySaturday, flWeekdaySunday))

        //Select default weekdayViews
        selectedWeekdays.forEach {
            weekdayIndicators[it].setBackgroundResource(R.drawable.weekday_circle_selected)
        }

        //Apply click listener to allEquipments weekday views
        weekdayViews.forEach {
            it.setOnClickListener(onClickWeekday)
        }

        //Construct the recyclerview for the equipments to display in
        adapterEquipments = AdapterEquipments(ArrayList())
        rvRegisterDetailEquipments.layoutManager = LinearLayoutManager(registerDetailActivity)
        rvRegisterDetailEquipments.itemAnimator = DefaultItemAnimator()
        rvRegisterDetailEquipments.adapter = adapterEquipments

        //Get viewmodel instance
        mViewModelEquipments = ViewModelProviders.of(this).get(ViewModelFragmentRegisterDetailEquipments::class.java)

        //Subscribe to equipment list
        mViewModelEquipments.equipmentList.observe(this, Observer { respEquipments ->
            if (respEquipments != null && respEquipments.isNotEmpty()) {
                adapterEquipments.setEquipments(respEquipments)
                pBRegisterDetailEquipmentsLoading.visibility = View.GONE
                rvRegisterDetailEquipments.visibility = View.VISIBLE
            }
        })
    }

    fun getSelectedEquipment(): Set<Int> = adapterEquipments.selectedEquipmentIds ?: HashSet()

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)

        week.forEachIndexed { index, day ->
            outState.putString(day, if (selectedWeekdays.contains(index)) C.TRUE else C.FALSE)
        }
    }

    private val onClickWeekday = View.OnClickListener {

        val weekday = Integer.valueOf(it.tag.toString())

        if (selectedWeekdays.contains(weekday)) {
            selectedWeekdays.remove(weekday)
            weekdayIndicators[weekday].setBackgroundResource(R.drawable.weekday_circle_unselected)
        } else {
            selectedWeekdays.add(weekday)
            weekdayIndicators[weekday].setBackgroundResource(R.drawable.weekday_circle_selected)
        }
    }

    override fun checkForm(): Boolean {

        return when {
            selectedWeekdays.isEmpty() -> {
                showWarningPopupNone()
                false
            }
            selectedWeekdays.size == 1 -> {
                showWarningPopupTooLittle()
                false
            }
            else -> true
        }
    }

    private fun showWarningPopupNone(){
        PopupRegisterScheduleWarning.show(registerDetailActivity, fragmentContainer)
    }

    private fun showWarningPopupTooLittle(){
        PopupRegisterScheduleTooLittle(registerDetailActivity, fragmentContainer, onClickNext = {
            registerDetailActivity.showNextSlide()
        }).show()
    }

    companion object {

        private const val MONDAY = "mon"
        private const val TUESDAY = "tue"
        private const val WEDNESDAY = "wed"
        private const val THURSDAY = "thu"
        private const val FRIDAY = "fri"
        private const val SATURDAY = "sat"
        private const val SUNDAY = "sun"

        private var week: Set<String> = setOf(MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY, SATURDAY, SUNDAY)
    }
}
