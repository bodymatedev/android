package de.bodymate.bodymateapp.fragment

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import de.bodymate.bodymateapp.R

/**
 * Created by Leonard on 23.03.2017.
 *
 */

class FragmentTrainer : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
            inflater.inflate(R.layout.f_trainer, container, false)
}
