package de.bodymate.bodymateapp.fragment

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.squareup.phrase.Phrase
import de.bodymate.bodymateapp.R
import kotlinx.android.synthetic.main.f_register_detail_complete.*

/**
 * Created by Leonard on 08.04.2018.
 */
class FragmentRegisterDetailComplete : Fragment(), IRegisterFormFragment {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
            inflater.inflate(R.layout.f_register_detail_complete, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        super.onViewCreated(view, savedInstanceState)

        updateFirstname()
    }

    fun updateFirstname(firstname: String = "") {

        val welcomeLetter = Phrase.from(resources, R.string.welcome_letter)
                .putOptional("name", firstname)
                .format()

        tvRegisterDetailLetter.text = welcomeLetter
    }

    override fun checkForm(): Boolean = false
}
