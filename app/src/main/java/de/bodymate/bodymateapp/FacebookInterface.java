package de.bodymate.bodymateapp;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v7.app.AppCompatActivity;

import de.bodymate.bodymateapp.util.Util;
import lombok.Data;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.annotations.SerializedName;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

/**
 * Created by Leonard on 19.02.2017.
 *
 */

public final class FacebookInterface{

	public static final int RC_SIGN_IN = 64206;

	private AppCompatActivity activity;

	private CallbackManager fbCallbackManager;

	private FacebookLoginCallback activityLoginCallback;

	@NonNull
	public static FacebookInterface create(@NotNull AppCompatActivity activity){

		return new FacebookInterface(activity);
	}

	public void loginFacebook(@NotNull FacebookLoginCallback activityLoginCallback){

		this.activityLoginCallback = activityLoginCallback;

		final Collection<String> permissions = Arrays.asList("email", "user_birthday", "public_profile");
		LoginManager.getInstance().logInWithReadPermissions(activity, permissions);
	}

	private FacebookInterface(final AppCompatActivity activity){

		this.activity = activity;

		fbCallbackManager = CallbackManager.Factory.create();

		LoginManager.getInstance().registerCallback(fbCallbackManager,
				new FacebookCallback <LoginResult>(){
					@Override
					public void onSuccess(LoginResult loginResult) {
						loginCallback(loginResult.getAccessToken());
					}

					@Override
					public void onCancel(){
						activityLoginCallback.onFailure(R.string.facebook_login_cancel);
					}

					@Override
					public void onError(FacebookException exception){

						activityLoginCallback.onFailure( (Util.INSTANCE.deviceOffline(activity)
							? R.string.error_connection_client
							: R.string.facebook_login_error ) );
					}
				}
		);
	}

	//Fetch user data with provided Facebook-Access-Token
	private void loginCallback(AccessToken accessToken){

		final FacebookLoginPayload payload = fetchDataByToken(accessToken);

		if( payload == null ){

			activityLoginCallback.onFailure(R.string.facebook_login_error);
			return;
		}

		activityLoginCallback.onSuccess(accessToken, payload);
	}

	//Get the actual RespUser Data Details of the Facebook RespUser
	@Nullable
	private FacebookLoginPayload fetchDataByToken(final AccessToken accessToken){

		final String graphFieldKey = "fields";
		final String graphFieldValues = "id,first_name,last_name,email,gender,birthday,picture.type(large)";
		final RawFacebookResponse rawResponse = new RawFacebookResponse();

		final GraphRequest graphRequest = GraphRequest.newMeRequest(accessToken, new GraphRequest.GraphJSONObjectCallback(){
			@Override
			public void onCompleted(JSONObject userData, GraphResponse response){
				if( response.getError() == null ){
					rawResponse.setRawResponse(userData.toString());
				}
			}
		});

		Bundle graphParameters = new Bundle();
		graphParameters.putString(graphFieldKey, graphFieldValues);
		graphRequest.setParameters(graphParameters);
		graphRequest.executeAndWait();

		return rawResponse.toPayloadObject();
	}

	@Contract(pure = true)
	public CallbackManager getFbCallbackManager(){
		return this.fbCallbackManager;
	}

	@Data
	public static final class FacebookLoginPayload{

		public String id;
		@SerializedName("first_name")
		public String firstname;
		@SerializedName("last_name")
		public String lastname;
		public String email;
		public String birthday;
		public String gender;
		public String pictureUrl;
	}

	private final class RawFacebookResponse{

		private String rawResponse = null;

		void setRawResponse(String responseJsonString) {
			this.rawResponse = responseJsonString;
		}

		@Nullable
		FacebookLoginPayload toPayloadObject(){

			if( rawResponse != null && rawResponse.trim().length() > 0){
				GsonBuilder gsonBuilder = new GsonBuilder();
				gsonBuilder.registerTypeAdapter(FacebookLoginPayload.class, facebookLoginPayloadDeserializer);
				return gsonBuilder.create().fromJson(rawResponse, FacebookLoginPayload.class);
			}
			return null;
		}
	}

	private JsonDeserializer<FacebookLoginPayload> facebookLoginPayloadDeserializer = new JsonDeserializer<FacebookLoginPayload>() {

		private static final String JSON_KEY_PICTURE = "picture";
		private static final String JSON_KEY_DATA = "data";
		private static final String JSON_KEY_URL = "url";

		@NonNull
		@Override
		public FacebookLoginPayload deserialize(@NotNull JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {

			//Deserialize allEquipments fields except "pictureUrl"
			FacebookLoginPayload facebookLoginPayload = new Gson().fromJson(json.toString(), FacebookLoginPayload.class);

			facebookLoginPayload.pictureUrl = json.getAsJsonObject()
					.getAsJsonObject(JSON_KEY_PICTURE)
					.getAsJsonObject(JSON_KEY_DATA)
					.get(JSON_KEY_URL).getAsString();

			return facebookLoginPayload;
		}
	};

	public interface FacebookLoginCallback{
		void onSuccess(@NonNull AccessToken accessToken, @NonNull FacebookLoginPayload payload);
		void onFailure(@StringRes int textId);
	}
}
