package de.bodymate.bodymateapp.paging.datasource

import android.app.Application
import android.arch.lifecycle.MutableLiveData
import android.arch.paging.PositionalDataSource
import android.util.Log
import de.bodymate.bodymateapp.CurrentUser
import de.bodymate.bodymateapp.api.ApiFunctions
import de.bodymate.bodymateapp.api.requestbody.RequestBodyGetWorkoutsPage
import de.bodymate.bodymateapp.api.requestbody.RequestBodyWorkoutCount
import de.bodymate.bodymateapp.api.response.RBWorkout
import de.bodymate.bodymateapp.data.service.WorkoutService
import de.bodymate.bodymateapp.paging.NetworkState
import de.bodymate.bodymateapp.paging.NetworkState.Companion.LOADED
import de.bodymate.bodymateapp.paging.NetworkState.Companion.LOADING
import de.bodymate.bodymateapp.paging.NetworkState.Companion.error
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.util.concurrent.Executor

class DataSourceWorkouts( application: Application,
                          private val retryExecutor: Executor) : PositionalDataSource<RBWorkout>() {

    private val workoutService = WorkoutService.getInstance(application)

    val networkState: MutableLiveData<NetworkState> = MutableLiveData()

    private var retry: (() -> Unit)? = null

    fun retryAllFailed() {
        val prevRetry = retry
        retry = null
        prevRetry?.let {
            retryExecutor.execute {
                it.invoke()
            }
        }
    }

    override fun loadRange(params: LoadRangeParams, callback: LoadRangeCallback<RBWorkout>) {

        Log.d(LOG_TAG_WORKOUT_PAGING, "loadRange: Requesting page ${params.startPosition} with load size ${params.loadSize}")

        networkState.postValue(NetworkState.LOADING)

        GlobalScope.launch {

            val workoutsPage = ApiFunctions.getWorkoutsAsPage(RequestBodyGetWorkoutsPage(CurrentUser.FIRE_ID!!, params.startPosition, params.loadSize))

            if( workoutsPage == null ){
                networkState.postValue(error("Failed to load workouts"))
                return@launch
            }

            networkState.postValue(NetworkState.LOADED)

            Log.d(LOG_TAG_WORKOUT_PAGING, "loadRange: Received ${workoutsPage.size} workout objects")

            if( workoutsPage.isNotEmpty() ){

                // Send them to the list pager
                callback.onResult(workoutsPage)

                // Persist workouts local
                workoutService.addAll(workoutsPage)
            }
        }
    }

    override fun loadInitial(params: LoadInitialParams, callback: LoadInitialCallback<RBWorkout>) {

        GlobalScope.launch(Dispatchers.Main) {

            networkState.postValue(LOADING)

            val totalWorkoutCount = ApiFunctions.workoutCount(RequestBodyWorkoutCount(CurrentUser.FIRE_ID!!))?.count

            if( totalWorkoutCount == null ){
                networkState.postValue(error("Could not get a valid response"))
                return@launch
            }

            if( totalWorkoutCount == 0 ){
                networkState.postValue(LOADED)
                return@launch
            }

            val page = computeInitialLoadPosition(params, totalWorkoutCount)
            val pageSize = computeInitialLoadSize(params, page, totalWorkoutCount)

            Log.d(LOG_TAG_WORKOUT_PAGING, "loadInitial: Requesting page $page with size $pageSize")

            val workoutsPage = ApiFunctions.getWorkoutsAsPage(RequestBodyGetWorkoutsPage(CurrentUser.FIRE_ID!!, 0, pageSize))

            if( workoutsPage == null ){
                networkState.postValue(error("Failed to load workouts"))
                return@launch
            }

            networkState.postValue(NetworkState.LOADED)

            Log.d(LOG_TAG_WORKOUT_PAGING, "loadInitial: Received ${workoutsPage.size} workout objects")

            if( workoutsPage.isNotEmpty() ){

                // Send them to the list pager
                callback.onResult(workoutsPage, page, totalWorkoutCount)

                // Persist workouts local
                workoutService.addAll(workoutsPage)
            }
        }
    }

    companion object {

        const val LOG_TAG_WORKOUT_PAGING = "PagingWorkout"

        const val DEFAULT_PAGE_SIZE_WORKOUTS = 4
    }
}