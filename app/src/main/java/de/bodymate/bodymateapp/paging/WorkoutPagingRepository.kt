package de.bodymate.bodymateapp.paging

import android.app.Application
import android.arch.lifecycle.Transformations
import android.arch.paging.LivePagedListBuilder
import android.arch.paging.PagedList
import de.bodymate.bodymateapp.api.response.RBWorkout
import de.bodymate.bodymateapp.paging.datasource.DataSourceWorkouts
import de.bodymate.bodymateapp.paging.factory.DataSourceFactoryWorkouts
import java.util.concurrent.Executors

class WorkoutPagingRepository(val application: Application) {

    fun workouts(): Listing<RBWorkout>{

        val fetchExecutor = Executors.newFixedThreadPool(5)

        val dataSourceFactoryWorkouts = DataSourceFactoryWorkouts(application, fetchExecutor)

        // Construct the LiveData object with the the datasource-factory and the defined paging list configuration
        val allWorkoutsByDate = LivePagedListBuilder(dataSourceFactoryWorkouts, PAGING_CONFIG)
                .setFetchExecutor(fetchExecutor)
                .build()

        return Listing(
                pagedList = allWorkoutsByDate,
                networkState = Transformations.switchMap(dataSourceFactoryWorkouts.sourceLiveData) {
                    it.networkState
                },
                retry = { dataSourceFactoryWorkouts.sourceLiveData.value?.retryAllFailed() }
        )
    }

    companion object {

        // Paging list configuration
        private val PAGING_CONFIG = PagedList.Config.Builder()
                .setPageSize(DataSourceWorkouts.DEFAULT_PAGE_SIZE_WORKOUTS)
                .setInitialLoadSizeHint(DataSourceWorkouts.DEFAULT_PAGE_SIZE_WORKOUTS)
                .setEnablePlaceholders(true)
                .build()
    }
}