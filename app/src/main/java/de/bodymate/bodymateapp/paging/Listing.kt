package de.bodymate.bodymateapp.paging

import android.arch.lifecycle.LiveData
import android.arch.paging.PagedList

data class Listing<T>(
        // the LiveData of paged lists for the UI to observe
        val pagedList: LiveData<PagedList<T>>,
        // represents the network request status to show to the user
        val networkState: LiveData<NetworkState>,
        // retries any failed requests.
        val retry: () -> Unit)