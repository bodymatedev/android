package de.bodymate.bodymateapp.paging.factory

import android.app.Application
import android.arch.lifecycle.MutableLiveData
import android.arch.paging.DataSource
import de.bodymate.bodymateapp.api.response.RBWorkout
import de.bodymate.bodymateapp.paging.datasource.DataSourceWorkouts
import java.util.concurrent.Executor

class DataSourceFactoryWorkouts(
        val application: Application,
        val retryExecutor: Executor): DataSource.Factory<Int, RBWorkout>() {

    val sourceLiveData = MutableLiveData<DataSourceWorkouts>()

    override fun create(): DataSource<Int, RBWorkout> {
        val source = DataSourceWorkouts(application, retryExecutor)
        sourceLiveData.postValue(source)
        return source
    }
}