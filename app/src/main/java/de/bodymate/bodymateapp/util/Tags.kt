package de.bodymate.bodymateapp.util

object Tags {

    const val REACTIVE = "ReactiveX"
    const val NOTIFICATION = "Notification"
}
