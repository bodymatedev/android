package de.bodymate.bodymateapp.util

/**
 * Created by leonard on 26.05.2017.
 */

object C {

    const val TRUE = "x"
    const val FALSE = ""
    const val PLAN_ID_FITNESSTEST = 1
    const val TYPE_REPS = "REPS"
    const val TYPE_TIME = "TIME"
    const val PLAN_ONE = 1
    const val PLAN_TWO = 2
    const val MALE = "m"
    const val FEMALE = "w"

    const val MONDAY = 0
    const val TUESDAY = 1
    const val WEDNESDAY = 2
    const val THURSDAY = 3
    const val FRIDAY = 4
    const val SATURDAY = 5
    const val SUNDAY = 6
}