package de.bodymate.bodymateapp.util

import org.jetbrains.annotations.Contract

/**
 * Created by Leonard on 13.03.2018.
 *
 * 24 Stunden Format Zeitangabe
 *
 */

class Time private constructor(val hourOfDay: Int, val minute: Int, val seconds: Int) {

    private val hourOfDayText: String
        get() = hourOfDay.toString()

    private val minuteText: String
        get() = if (minute >= 10) minute.toString() else "0" + minute.toString()

    private val secondsText: String
        get() = if (seconds >= 10) seconds.toString() else "0" + seconds.toString()

    override fun equals(other: Any?): Boolean {

        return other is Time && calculateSum(this) == calculateSum((other as Time?)!!)
    }

    fun toText(printSeconds: Boolean): String {

        var output = ""

        output += "$hourOfDayText:"
        output += minuteText

        if( printSeconds )
            output += ":$secondsText"

        return output
    }

    override fun hashCode(): Int {
        var result = hourOfDay
        result = 31 * result + minute
        result = 31 * result + seconds
        return result
    }

    companion object {

        @Contract("null -> null")
        fun of(sqlTime: String?): Time? {

            return if (sqlTime == null || sqlTime.isEmpty()) {
                null
            }else {
                try {
                    Time(Integer.valueOf(sqlTime.substring(0, 2)), Integer.valueOf(sqlTime.substring(4, 6)), 0)
                } catch (ex: NumberFormatException) {
                    null
                }
            }
        }

        fun of(hourOfDay: Int, minute: Int): Time {
            return Time(hourOfDay, minute, 0)
        }

        fun of(hourOfDay: Int, minute: Int, seconds: Int): Time {
            return Time(hourOfDay, minute, seconds)
        }

        private fun calculateSum(time: Time): Int {
            return time.hourOfDay + time.minute + time.seconds
        }
    }
}
