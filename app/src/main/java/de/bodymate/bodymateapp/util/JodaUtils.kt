package de.bodymate.bodymateapp.util

import de.bodymate.bodymateapp.api.response.RBSchedule
import de.bodymate.bodymateapp.data.repository.entity.RoomSchedule
import org.jetbrains.annotations.Nullable
import org.joda.time.*
import org.joda.time.format.DateTimeFormat
import java.util.*

private val locale = Locale.GERMANY
private val timeZone = TimeZone.getTimeZone("Europe/Berlin")
private val dateTimeZone = DateTimeZone.forTimeZone(timeZone)
private val formatDateTime = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss")
private val formatDate = DateTimeFormat.forPattern("yyyy-MM-dd")
private val formatTime = DateTimeFormat.forPattern("HH:mm:ss")
private val formatTimeShort = DateTimeFormat.forPattern("HH:mm")

/**
 * Basic creator methods
 */

fun date(sqlDate: String): LocalDate?{
    return try {
        formatDate.parseLocalDate(sqlDate)
    }catch (e: IllegalArgumentException){ null }
}

fun time(sqlTime: String): LocalTime?{
    return try {
        formatTime.parseLocalTime(sqlTime)
    }catch (e: IllegalArgumentException){ null }
}

fun timeShort(timeWithMinutesAndSeconds: String): LocalTime? {
    return try {
        formatTimeShort.parseLocalTime(timeWithMinutesAndSeconds)
    }catch (e: IllegalArgumentException){ null }
}

fun dateTime(sqlDateTime: String): LocalDateTime?{
    return try {
        formatDateTime.parseLocalDateTime(sqlDateTime)
    }catch (e: IllegalArgumentException){ null }
}

fun today(): LocalDate = LocalDate.now(dateTimeZone)

fun todayAsString(): String = LocalDate.now(dateTimeZone).toString(formatDate)

/**
 *  Weekday methods
 */
fun dateTimeNextWeekday(weekday: Int, time: LocalTime?): LocalDateTime?{

    if( time == null ) return null

    val today = LocalDateTime.now().withTime(time.hourOfDay, time.minuteOfHour, time.secondOfMinute, time.millisOfSecond)

    return if( today.dayOfWeek <= jodaWeekday(weekday) ){
        today.withDayOfWeek(jodaWeekday(weekday))
    }else{
        today.plusWeeks(1).withDayOfWeek(jodaWeekday(weekday))
    }
}

private fun jodaWeekday(backendWeekday: Int): Int = backendWeekday + 1

/**
 *  Methods for user schedules
 */

fun time(schedule: RoomSchedule): LocalTime?{
    return try {
        formatTime.parseLocalTime(schedule.time)
    }catch (e: IllegalArgumentException){ null }
}

fun time(schedule: RBSchedule): LocalTime?{
    return try {
        formatTime.parseLocalTime(schedule.time)
    }catch (e: IllegalArgumentException){ null }
}

/**
 * Extension functions
 */

fun LocalTime.toPrint(): String{
    var print = hourOfDay.toString()
    if( minuteOfHour > 0 ){
        print = print.plus(":").plus(minuteOfHour.toString())
    }
    return print
}

fun LocalTime.toPrintExtended(): String{
    var print = hourOfDay.toString()
    print = if( minuteOfHour > 0 ){
        print.plus(":").plus(minuteOfHour.toString())
    }else{
        print.plus(":").plus("00")
    }
    return print
}

fun LocalDateTime.timeInMillis(): Long{
    val cal = Calendar.getInstance(timeZone, locale)
    cal.time = toDate()
    return cal.timeInMillis
}

fun LocalDate.isToday(): Boolean = this == today()