package de.bodymate.bodymateapp.util

import android.app.DatePickerDialog
import android.content.Context
import com.prolificinteractive.materialcalendarview.CalendarDay
import org.jetbrains.annotations.Contract
import org.joda.time.Minutes
import org.joda.time.format.DateTimeFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by Leonard on 13.03.2018.
 *
 */

object DateUtils {

    val LOCALE = Locale.GERMANY!!
    private val dateFormatAndroid = SimpleDateFormat("dd.MM.yyyy", LOCALE)
    private val dateFormatSQL = SimpleDateFormat("yyyy-MM-dd", LOCALE)
    private val dateFormatFacebook = SimpleDateFormat("MM/dd/yyyy", LOCALE)
    private val dateTimeFormatSQL = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", LOCALE)
    private val timeFormatSQL = SimpleDateFormat("HH:mm:ss", LOCALE)

    private val jodaDateTime = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss")

    fun isToday(day: CalendarDay): Boolean {
        return day == CalendarDay.today()
    }

    fun yesterday(): CalendarDay {

        val calendar = Calendar.getInstance(LOCALE)
        calendar.add(Calendar.DATE, -1)
        return CalendarDay.from(calendar)
    }

    fun isTomorrow(day: CalendarDay): Boolean {

        val c = Calendar.getInstance(LOCALE)
        c.add(Calendar.DATE, 1)
        return CalendarDay.from(c.time) == day
    }

    fun thisMonday(): CalendarDay? {
        return CalendarDay.from(forWeekday(Calendar.MONDAY))
    }

    fun getDayByWeekdayPlusWeekOffset(day: Int, offsetWeeks: Int): CalendarDay? {
        return CalendarDay.from(forWeekday(day, offsetWeeks * 7))
    }

    private fun forWeekday(day: Int, offsetDays: Int = 0): Calendar {

        val cal = Calendar.getInstance(LOCALE)
        cal.add(Calendar.DATE, offsetDays)
        cal.set(Calendar.DAY_OF_WEEK, day)
        return cal
    }

    /**
     * @param weekday The weekday page 0 to 6
     * @param daytime Time of format HH:mm:ss
     */
    fun getNext(weekday: Int, daytime: String): Calendar? {

        val calTime = Calendar.getInstance(LOCALE)

        try {
            calTime.time = timeFormatSQL.parse(daytime)
        }catch (pe:ParseException){
            return null
        }

        val calNext = Calendar.getInstance(LOCALE)

        val javaWeekday = weekdayToJavaWeekday(weekday)

        if( javaWeekday == -1 ) return null

        while (calNext.get(Calendar.DAY_OF_WEEK) != javaWeekday){
            calNext.add(Calendar.DATE, 1)
        }

        calNext.set(Calendar.HOUR_OF_DAY, calTime.get(Calendar.HOUR_OF_DAY))
        calNext.set(Calendar.MINUTE, calTime.get(Calendar.MINUTE))
        calNext.set(Calendar.SECOND, calTime.get(Calendar.SECOND))
        calNext.set(Calendar.MILLISECOND, 0)

        return calNext
    }

    fun getWeekday(day: CalendarDay): Int {

        val calendar = Calendar.getInstance(DateUtils.LOCALE)
        calendar.time = day.date

        return when(calendar.get(Calendar.DAY_OF_WEEK)){
            Calendar.MONDAY -> 0
            Calendar.TUESDAY -> 1
            Calendar.WEDNESDAY -> 2
            Calendar.THURSDAY -> 3
            Calendar.FRIDAY -> 4
            Calendar.SATURDAY -> 5
            Calendar.SUNDAY -> 6
            else -> throw IllegalStateException()
        }
    }

    fun getWeekday(): Int = getWeekday(CalendarDay.today())

    fun getWeekdayText(day: CalendarDay): String {

        val calendar = Calendar.getInstance(LOCALE)
        calendar.time = day.date
        return SimpleDateFormat("EEEE", LOCALE).format(day.date)
    }

    fun formatToOutput(calendarDay: CalendarDay): String{

        return dateFormatAndroid.format(calendarDay.date)
    }

    fun formatToDayAndMonth(day: CalendarDay): String {

        return SimpleDateFormat("dd.MM", LOCALE).format(day.date)
    }

    fun now(): String {
        return SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH).format(Calendar.getInstance().time)
    }

    fun showDatePicker(mContext: Context, androidDate: String, onDateSetListener: DatePickerDialog.OnDateSetListener) {

        val calendarDay = androidDateToCalendarDay(androidDate)

        if (calendarDay == null) {
            showDatePicker(mContext, onDateSetListener)
            return
        }

        DatePickerDialog(mContext, onDateSetListener, calendarDay.year, calendarDay.month, calendarDay.day).show()
    }

    fun showDatePicker(mContext: Context, onDateSetListener: DatePickerDialog.OnDateSetListener) {

        DatePickerDialog(mContext, onDateSetListener, 1994, 1, 1).show()
    }

    @Contract("null -> null")
    fun getCalendarDayFromAndroidDate(androidDate: String?): CalendarDay? {

        if (androidDate == null || androidDate.isEmpty()) {
            return null
        }

        return try {
            CalendarDay.from(dateFormatAndroid.parse(androidDate))
        } catch (e: ParseException) {
            null
        }
    }

    fun getAndroidDate(year: Int, monthOfYear: Int, dayOfMonth: Int): String {

        val month = if (monthOfYear < 10) "0" + monthOfYear.toString() else monthOfYear.toString()
        val day = if (dayOfMonth < 10) "0" + dayOfMonth.toString() else dayOfMonth.toString()
        return day + "." + month + "." + year.toString()
    }

    @Contract("null -> null")
    fun androidDateToCalendarDay(androidDate: String?): CalendarDay? {

        if (androidDate == null || androidDate.isEmpty()) {
            return null
        }

        return try {
            CalendarDay.from(dateFormatAndroid.parse(androidDate))
        } catch (e: ParseException) {
            null
        }
    }

    fun getAge(date: CalendarDay?): Int {

        if (date == null) {
            return -1
        }

        val now = Calendar.getInstance()
        val dob = Calendar.getInstance()

        var age: Int

        dob.time = date.date

        if (dob.after(now)) {
            return -1
        }

        age = now.get(Calendar.YEAR) - dob.get(Calendar.YEAR)

        //If birth preserved is greater than todays preserved (after 2 days adjustment of leap year) then decrement age one year
        if (dob.get(Calendar.DAY_OF_YEAR) - now.get(Calendar.DAY_OF_YEAR) > 3 || dob.get(Calendar.MONTH) > now.get(Calendar.MONTH)) {
            age--

            //If birth preserved and todays preserved are of same month and birth day of month is greater than todays day of month then decrement age
        } else if (dob.get(Calendar.MONTH) == now.get(Calendar.MONTH) && dob.get(Calendar.DAY_OF_MONTH) > now.get(Calendar.DAY_OF_MONTH)) {
            age--
        }

        return age
    }

    fun facebookDateToSqlDate(fbDate: String?): String {

        if (fbDate == null || fbDate.isEmpty()) {
            return ""
        }

        return try {
            dateFormatSQL.format(dateFormatFacebook.parse(fbDate))
        } catch (e: ParseException) {
            ""
        }
    }

    @Contract("null -> !null")
    fun sqlToAndroidDate(sqlDate: String?): String {

        if (sqlDate == null || sqlDate.isEmpty()) {
            return ""
        }

        return try {
            dateFormatAndroid.format(dateFormatSQL.parse(sqlDate))
        } catch (e: ParseException) {
            ""
        }
    }

    fun androidToSqlDate(androidDate: String?): String{
        return dateFormatSQL.format(dateFormatAndroid.parse(androidDate))
    }

    fun addSecondsToDateTime(dateTime: String, seconds: Int): String? {

        val calendar = getCalendar(dateTimeFormatSQL, dateTime) ?: return null

        calendar.add(Calendar.SECOND, seconds)

        return dateTimeFormatSQL.format(calendar.time)
    }

    private fun getCalendar(format: SimpleDateFormat, source: String?): Calendar? {

        return try {
            val calendar = Calendar.getInstance(LOCALE)
            calendar.time = format.parse(source)
            calendar
        } catch (e: ParseException) {
            e.printStackTrace()
            null
        }
    }

    fun getDateFromSqlDateTime(sqlDatTime: String?): CalendarDay {

        return CalendarDay.from(getCalendar(dateTimeFormatSQL, sqlDatTime)) ?: throw de.bodymate.bodymateapp.exception.ParseException()
    }

    fun getTimeFromSqlDateTime(sqlDateTime: String): Time? {

        val calendar = getCalendar(dateTimeFormatSQL, sqlDateTime) ?: return null
        return Time.of(calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE))
    }

    fun getSqlDateFromCalendarDay(calendarDay: CalendarDay): String?{
        return dateFormatSQL.format(calendarDay.date)
    }

    fun getCalendarDayFromSqlDate(sqlDate: String) : CalendarDay{

        return CalendarDay.from(getCalendar(dateFormatSQL, sqlDate)) ?: throw de.bodymate.bodymateapp.exception.ParseException()
    }

    fun getDateFromSqlDate(sqlDate: String?) : Date?{

        if( sqlDate == null ) return null

        return try {
            dateFormatSQL.parse(sqlDate)
        }catch (e:ParseException){
            null
        }
    }

    @Contract(pure = true)
    fun weekdayToJavaWeekday(weekday: Int): Int {

        return when (weekday) {
            0 -> Calendar.MONDAY
            1 -> Calendar.TUESDAY
            2 -> Calendar.WEDNESDAY
            3 -> Calendar.THURSDAY
            4 -> Calendar.FRIDAY
            5 -> Calendar.SATURDAY
            6 -> Calendar.SUNDAY
            else -> -1
        }
    }

    @Contract("null -> false")
    fun isValidSqlDate(sqlDate: String?): Boolean {

        return validate(sqlDate, dateFormatSQL)
    }

    private fun validate(sqlDate: String?, dateFormatSql: SimpleDateFormat): Boolean {

        if (sqlDate == null || sqlDate.isEmpty()) return false

        return try {
            dateFormatSql.parse(sqlDate)
            true
        } catch (e: ParseException) {
            false
        }
    }

    fun daysPastFromNow(dateInPast:Date):Int{

        val dateToday = Date()

        if( !dateToday.after(dateInPast) ) return 0

        val diff:Long = dateToday.time - dateInPast.time

        return (diff / (1000*60*60*24)).toInt()
    }

    fun getAsDate(datetime: String): Date?{

        return try {
            dateTimeFormatSQL.parse(datetime)
        }catch (pe: ParseException){
            null
        }
    }

    fun getMinutesBetween(dateTimeStart: String?, dateTimeEnd: String?): Int{

        if( dateTimeStart == null || dateTimeEnd == null ) return 0

        val startDate = jodaDateTime.parseDateTime(dateTimeStart)
        val endDate = jodaDateTime.parseDateTime(dateTimeEnd)

        return Minutes.minutesBetween(startDate, endDate).minutes
    }

    fun manipulateDateByAdd(date: Date, field: Int, amount: Int): Date{

        val cal = Calendar.getInstance(LOCALE)
        cal.time = date
        cal.add(field, amount)
        return cal.time
    }

    fun makeTime(time: String): Date?{

        val calTime = Calendar.getInstance(LOCALE)

        return try {
            calTime.time = timeFormatSQL.parse(time)
            calTime.time
        }catch (pe:ParseException){
            return null
        }
    }
}
