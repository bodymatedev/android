package de.bodymate.bodymateapp.util

import android.content.Context
import android.content.pm.ApplicationInfo
import android.content.pm.PackageManager
import android.content.res.ColorStateList
import android.graphics.Point
import android.graphics.drawable.ColorDrawable
import android.net.ConnectivityManager
import android.support.annotation.ColorRes
import android.support.annotation.DimenRes
import android.support.annotation.LayoutRes
import android.support.annotation.StringRes
import android.support.v4.content.ContextCompat
import android.view.*
import android.view.ViewGroup.LayoutParams.WRAP_CONTENT
import android.widget.PopupWindow
import android.widget.Toast
import de.bodymate.bodymateapp.R
import jp.wasabeef.blurry.Blurry

/**
 * Created by Leonard on 10.03.2017.
 *
 */

object Util {

    fun toastIt(context: Context, @StringRes textId: Int) {

        val toast = Toast.makeText(context, textId, Toast.LENGTH_SHORT)
        toast.setGravity(Gravity.BOTTOM, 0, 20)
        toast.show()
    }

    fun toastIt(context: Context, text: CharSequence) {

        val toast = Toast.makeText(context, text, Toast.LENGTH_SHORT)
        toast.setGravity(Gravity.BOTTOM, 0, 20)
        toast.show()
    }

    fun deviceOffline(context: Context): Boolean {

        val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val netInfo = cm.activeNetworkInfo
        return netInfo == null || !netInfo.isConnected
    }

    fun getSystemLayoutInflater(context: Context): LayoutInflater {
        return context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
    }

    fun inflateView(context: Context, layoutID: Int): View {
        return Util.getSystemLayoutInflater(context).inflate(layoutID, null)
    }

    private fun getSystemWindowsManager(context: Context): WindowManager =
        context.getSystemService(Context.WINDOW_SERVICE) as WindowManager

    private fun getDisplaySize(context: Context): Point {

        val displaySize = Point()
        Util.getSystemWindowsManager(context).defaultDisplay.getSize(displaySize)
        return displaySize
    }

    private fun buildSimplePopup(context: Context, layout: Int): PopupWindow {

        //Inflate content view
        val popupView = Util.inflateView(context, layout)

        //Create popup
        val popup = PopupWindow(popupView, WRAP_CONTENT, WRAP_CONTENT)
        popup.isOutsideTouchable = true
        popup.isFocusable = true
        popup.animationStyle = R.style.popupAnimationStyle

        return popup
    }

    private fun buildSimplePopupWidth(context: Context, layout: Int, width: Double): PopupWindow{

        //Inflate content view
        val popupView = Util.inflateView(context, layout)

        //Calculate outbound dimensions
        val lWidth = (getDisplaySize(context).x * width).toInt()

        //Create popup
        val popup = PopupWindow(popupView, lWidth, WRAP_CONTENT)
        popup.isOutsideTouchable = true
        popup.isFocusable = true
        popup.animationStyle = R.style.popupAnimationStyle

        return popup
    }

    private fun buildSimplePopupHeight(context: Context, layout: Int, height: Double): PopupWindow{

        //Inflate content view
        val popupView = Util.inflateView(context, layout)

        //Calculate outbound dimensions
        val lHeight = (getDisplaySize(context).y * height).toInt()

        //Create popup
        val popup = PopupWindow(popupView, WRAP_CONTENT, lHeight)
        popup.isOutsideTouchable = true
        popup.isFocusable = true
        popup.animationStyle = R.style.popupAnimationStyle

        return popup
    }

    private fun buildSimplePopup(context: Context, layout: Int, width: Double, height: Double): PopupWindow {

        //Inflate content view
        val popupView = Util.inflateView(context, layout)

        //Calculate outbound dimensions
        val lWidth = (getDisplaySize(context).x * width).toInt()
        val lHeight = (getDisplaySize(context).y * height).toInt()

        //Create popup
        val popup = PopupWindow(popupView, lWidth, lHeight)
        popup.isOutsideTouchable = true
        popup.isFocusable = true
        popup.animationStyle = R.style.popupAnimationStyle

        return popup
    }

    fun showDimPopup(context: Context, parent: ViewGroup, @LayoutRes layout: Int): PopupWindow {

        val popup = buildSimplePopup(context, layout)
        popup.showAtLocation(parent.rootView, Gravity.CENTER, 0, 0)
        dimBehindPopup(context, popup)
        return popup
    }

    fun showDimPopupWidth(context: Context, parent: ViewGroup, @LayoutRes layout: Int, width: Double): PopupWindow {

        val popup = buildSimplePopupWidth(context, layout, width)
        popup.showAtLocation(parent.rootView, Gravity.CENTER, 0, 0)
        dimBehindPopup(context, popup)
        return popup
    }

    fun showDimPopupHeight(context: Context, parent: ViewGroup, @LayoutRes layout: Int, height: Double): PopupWindow {

        val popup = buildSimplePopupHeight(context, layout, height)
        popup.showAtLocation(parent.rootView, Gravity.CENTER, 0, 0)
        dimBehindPopup(context, popup)
        return popup
    }

    fun showDimPopup(context: Context, parent: ViewGroup, @LayoutRes layout: Int, width: Float, height: Float): PopupWindow {

        val popup = buildSimplePopup(context, layout, width.toDouble(), height.toDouble())
        popup.showAtLocation(parent.rootView, Gravity.CENTER, 0, 0)
        dimBehindPopup(context, popup)
        return popup
    }

    private fun dimBehindPopup(context: Context, popup: PopupWindow) {

        //Dim the background
        popup.setBackgroundDrawable(ColorDrawable())
        val container = popup.contentView.parent as View
        val layoutParams = container.layoutParams as WindowManager.LayoutParams
        layoutParams.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND
        layoutParams.dimAmount = .8f
        Util.getSystemWindowsManager(context).updateViewLayout(container, layoutParams)
    }

    fun showBlurPopup(context: Context, parent: ViewGroup, @LayoutRes layout: Int, width: Float, height: Float): PopupWindow {

        val popup = buildSimplePopup(context, layout, width.toDouble(), height.toDouble())
        popup.showAtLocation(parent.rootView, Gravity.CENTER, 0, 0)
        Blurry.with(context).radius(10).sampling(5).onto(parent)
        popup.setOnDismissListener { Blurry.delete(parent) }
        return popup
    }

    fun dimen(context: Context, @DimenRes dimen: Int): Int =
            context.resources.getDimension(dimen).toInt()

    fun color(context: Context, @ColorRes color: Int): Int =
            ContextCompat.getColor(context, color)

    fun colorPrimary(context: Context): Int =
            ContextCompat.getColor(context, R.color.colorPrimary)

    fun text(context: Context, @StringRes stringRes: Int): String =
            context.getString(stringRes)

    fun getOneColorStateList(mContext: Context, @ColorRes colorRes: Int): ColorStateList {

        val states = arrayOf(intArrayOf(android.R.attr.state_enabled), intArrayOf(-android.R.attr.state_enabled), intArrayOf(-android.R.attr.state_checked), intArrayOf(android.R.attr.state_pressed))

        val color = color(mContext, colorRes)
        val colors = intArrayOf(color, color, color, color)
        return ColorStateList(states, colors)
    }

    fun getApplicationName(context: Context): String {

        val packageManager = context.packageManager
        var applicationInfo: ApplicationInfo? = null

        try {
            applicationInfo = packageManager.getApplicationInfo(context.applicationInfo.packageName, 0)
        } catch (e: PackageManager.NameNotFoundException) {
            e.printStackTrace()
        }

        return if (applicationInfo != null) packageManager.getApplicationLabel(applicationInfo).toString() else "BodyMate"
    }
}
