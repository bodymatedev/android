package de.bodymate.bodymateapp.util

import android.os.Handler
import android.os.SystemClock
import android.text.format.DateUtils

/**
 * Created by Leonard on 31.12.2017.
 *
 */

open class Timer {

    @Volatile
    internal var running = false
    @Volatile
    @get:Synchronized
    var isPaused = false
        private set
    internal val handler: Handler = Handler()

    @get:Synchronized
    var elapsedTime: Int = 0
        internal set

    internal var realTimeStart: Long = 0
    internal var elapsedInPause: Long = 0
    internal var strBuilder = StringBuilder(8)

    var tickListener: TickListener? = null

    private val tickRunnable = object : Runnable {
        override fun run() {
            if (running) {
                update(SystemClock.elapsedRealtime())
                handler.postDelayed(this, 1000)
            } else if (isPaused) {
                elapsedInPause += 1000
                handler.postDelayed(this, 1000)
            }
        }
    }

    open fun start() {

        realTimeStart = SystemClock.elapsedRealtime()
        running = true
        tickRunnable.run()
    }

    open fun pause() {

        running = false
        isPaused = true
    }

    open fun resume() {

        running = true
        isPaused = false
    }

    open fun reset() {

        running = false
        isPaused = false
        realTimeStart = 0
        elapsedTime = 0
        elapsedInPause = 0
    }

    @Synchronized
    open fun update(now: Long) {

        if (tickListener != null) {
            elapsedTime = ((now - realTimeStart - elapsedInPause) / 1000).toInt()
            tickListener!!.onTick(DateUtils.formatElapsedTime(strBuilder, elapsedTime.toLong()))
        }
    }

    @Synchronized
    override fun toString(): String {
        return DateUtils.formatElapsedTime(strBuilder, elapsedTime.toLong())
    }

    @Synchronized
    fun getElapsedSeconds(): Int {
        return elapsedTime
    }

    @Synchronized
    fun destroy() {
        tickListener = null
    }

    interface TickListener {
        fun onTick(time: String)
    }
}
