package de.bodymate.bodymateapp.util

import android.os.SystemClock
import android.text.format.DateUtils

/**
 * Created by Leonard on 02.01.2018.
 *
 */

class ExerciseTimer(@field:Volatile private var goal: Int) : Timer() {

    @Volatile
    private var countdown: Int = COUNTDOWN_STANDARD
    @Volatile
    private var countdownRunning = false
    var eventListener: ExerciseTimerEventListener? = null

    private val runnableCountdown = object : Runnable {
        override fun run() {
            if (countdownRunning) {
                val elapsedSinceStart = ((SystemClock.elapsedRealtime() - this@ExerciseTimer.realTimeStart) / 1000).toInt()
                if (elapsedSinceStart < countdown) {
                    val countdownRemaining = countdown - elapsedSinceStart
                    eventListener!!.onTick(countdownRemaining.toString())
                    handler.postDelayed(this, 1000)
                } else {
                    countdownRunning = false
                    eventListener!!.onTimerStart()
                    super@ExerciseTimer.start()
                }
            }
        }
    }

    val isRunning: Boolean
        @Synchronized get() = if (countdown == 0) running else running || countdownRunning

    override fun start() {

        if (eventListener == null) {
            return
        }

        if (countdown == 0 && goal == 0) {
            super.start()
        } else {
            countdownRunning = true
            eventListener!!.onCountDownStart()
            realTimeStart = SystemClock.elapsedRealtime()
            runnableCountdown.run()
        }
    }

    override fun pause() {
        if (!isRunning) {
            return
        }
        super.pause()
        if (!countdownRunning) {
            eventListener!!.onPause()
        } else {
            reset()
        }
    }

    override fun resume() {
        super.resume()
        eventListener!!.onResume()
    }

    override fun reset() {
        super.reset()
        if (eventListener != null) {
            eventListener!!.onReset(countdownRunning)
        }
        countdownRunning = false
    }

    @Synchronized
    override fun update(now: Long) {

        if (eventListener != null) {

            elapsedTime = (now - realTimeStart - elapsedInPause).toInt() / 1000

            eventListener!!.onTick(DateUtils.formatElapsedTime(strBuilder, elapsedTime.toLong()))

            if (goal > 0 && elapsedTime == goal) {
                eventListener!!.onGoalReached()
            }
        }
    }

    @Synchronized
    fun addSecond() {
        if (!isRunning) {
            elapsedTime++
            realTimeStart -= 1000
        }
    }

    @Synchronized
    fun subtractSecond() {
        if (!isRunning && elapsedTime > 0) {
            elapsedTime--
            realTimeStart += 1000
        }
    }

    @Synchronized
    fun hasListener(): Boolean {
        return eventListener != null
    }

    interface ExerciseTimerEventListener : Timer.TickListener {

        fun onCountDownStart()
        fun onTimerStart()
        fun onGoalReached()
        fun onPause()
        fun onResume()
        fun onReset(duringCountdown: Boolean)
    }

    companion object {

        private const val COUNTDOWN_STANDARD = 5

        fun create(goal: Int): ExerciseTimer {
            return ExerciseTimer(goal)
        }
    }
}
