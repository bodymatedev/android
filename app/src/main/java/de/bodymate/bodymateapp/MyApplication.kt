package de.bodymate.bodymateapp

import android.app.Application

import com.android.volley.RequestQueue
import com.android.volley.toolbox.Volley

import org.jetbrains.annotations.Contract

/**
 * Created by Leonard on 30.05.2018.
 */
class MyApplication : Application() {

    override fun onCreate() {
        super.onCreate()

        requestQueue = Volley.newRequestQueue(applicationContext)
    }

    companion object {

        var requestQueue: RequestQueue? = null
            private set
    }
}
