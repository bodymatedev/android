package de.bodymate.bodymateapp.exception

import java.lang.RuntimeException

class JsonSerializeException: RuntimeException("Failed to serialize the give object")