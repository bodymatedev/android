package de.bodymate.bodymateapp.exception

import java.lang.RuntimeException

class ParseException : RuntimeException("Failed parse the given date/time")