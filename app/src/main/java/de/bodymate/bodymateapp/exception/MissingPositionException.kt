package de.bodymate.bodymateapp.exception

import java.lang.RuntimeException

class MissingPositionException : RuntimeException("Unknown plan position")