package de.bodymate.bodymateapp.exception

import java.lang.RuntimeException

class MissingSetException : RuntimeException("Unknown position set")