package de.bodymate.bodymateapp.exception

class EntityFormatException : RuntimeException {

    constructor() : super("Failed to format entity(s)")

    constructor(message: String) : super(message)
}