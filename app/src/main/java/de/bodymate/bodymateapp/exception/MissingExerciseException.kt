package de.bodymate.bodymateapp.exception

class MissingExerciseException : RuntimeException("Cant find exercise")