package de.bodymate.bodymateapp

import android.util.SparseArray
import android.util.SparseIntArray

import de.bodymate.bodymateapp.data.internentity.Plan

/**
 * Created by leonard on 24.12.2017.
 *
 */

class WorkoutLogger(plan: Plan) {

    val positionsLog: SparseArray<SparseIntArray> = SparseArray()

    init {

        //Initialize allEquipments set's to 0
        for (i in 1..plan.positions.size) {
            val logsSet = SparseIntArray()
            for (j in 1 until plan.getSetCountAt(i - 1)) {
                logsSet.put(j, 0)
            }
            positionsLog.put(i, logsSet)
        }
    }

    fun logReps(adapterPosition: Int, adapterSet: Int, reps: Int) {

        positionsLog.get(adapterPosition + 1).put(adapterSet + 1, reps)
    }

    fun informAboutAdditionalSet(adapterPosition: Int, newSetId: Int) {

        positionsLog.get(adapterPosition + 1).put(newSetId, 0)
    }

    fun getRepSum(planPosition: Int): Int {

        val reps = positionsLog.get(planPosition) ?: return 0

        var repSum = 0

        for (i in 0 until positionsLog.get(planPosition).size()) {
            repSum += reps.get(i)
        }
        return repSum
    }
}
