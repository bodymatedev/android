package de.bodymate.bodymateapp.gui;

import android.content.Context;
import android.graphics.PointF;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.LinearSmoothScroller;
import android.support.v7.widget.RecyclerView;

/**
 * Created by Leonard on 16.03.2018.
 *
 */

public class SmoothScrollLayoutManager extends LinearLayoutManager {

	public SmoothScrollLayoutManager(Context context) {
		super(context, VERTICAL, false);
	}

	@Override
	public void smoothScrollToPosition(RecyclerView recyclerView, RecyclerView.State state, int position) {

		RecyclerView.SmoothScroller smoothScroller = new TopSnappedSmoothScroller(recyclerView.getContext());
		smoothScroller.setTargetPosition(position);
		startSmoothScroll(smoothScroller);
	}

	private class TopSnappedSmoothScroller extends LinearSmoothScroller {
		TopSnappedSmoothScroller(Context context) {
			super(context);
		}

		@Override
		public PointF computeScrollVectorForPosition(int targetPosition) {
			return SmoothScrollLayoutManager.this
					.computeScrollVectorForPosition(targetPosition);
		}

		@Override
		protected int getVerticalSnapPreference() {
			return SNAP_TO_START;
		}
	}
}
