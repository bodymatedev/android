package de.bodymate.bodymateapp.gui

import android.graphics.Rect
import android.support.v7.widget.RecyclerView
import android.view.View

class RecyclerViewDecoratorHorizontal(private val spacing: Int, private val includeTopBottom: Boolean) : RecyclerView.ItemDecoration() {

    override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {

        val position = parent.getChildAdapterPosition(view)

        outRect.left = if (position > 0) spacing / 2 else 0
        outRect.right = spacing / 2

        if (includeTopBottom) {
            outRect.top = spacing
            outRect.bottom = outRect.top
        }
    }
}