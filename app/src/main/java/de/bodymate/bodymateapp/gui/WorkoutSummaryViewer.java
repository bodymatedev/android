package de.bodymate.bodymateapp.gui;

import android.content.Context;
import android.widget.ListView;

import java.util.ArrayList;

import de.bodymate.bodymateapp.gui.adapter.AdapterWorkoutSummary;
import de.bodymate.bodymateapp.gui.adapter.datamodel.WorkoutSummary;
import de.bodymate.bodymateapp.data.internentity.Exercise;
import de.bodymate.bodymateapp.data.internentity.Plan;

/**
 * Created by Leonard on 21.01.2018.
 *
 */

public final class WorkoutSummaryViewer {

    private final Context context;

    private AdapterWorkoutSummary adapterWorkoutSummary;
    private ArrayList<WorkoutSummary> workoutSummaries;

    public WorkoutSummaryViewer(Plan plan, Context context){

        this.context = context;

        //Generate initial ArrayList of WorkoutSummaries
        workoutSummaries = new ArrayList<>();
        for(int i = 0; i < plan.getPositions().size(); i++ ){

            Exercise exercise = plan.getExerciseAt(i);
            int repsTotalGoal = 0;
            for(int j = 0; j < plan.getPosition(i).getSetCount(); j++){
                repsTotalGoal += plan.getSetsAt(i).get(j).getReps();
            }
            workoutSummaries.add(new WorkoutSummary(i, exercise.getName(), 0, 0, plan.getPosition(i).getSetCount(), repsTotalGoal, exercise.getType()));

        }
    }

    public void display(ListView listView){

        adapterWorkoutSummary = new AdapterWorkoutSummary(workoutSummaries, context);
        listView.setAdapter(adapterWorkoutSummary);
    }

    void updateRepSumFor(int planPosition, int newRepSum) {

        workoutSummaries.get(planPosition).setRepSum(newRepSum);
    }

    void updateSetCountFor(int adapterPosition, int newSetCount){

        workoutSummaries.get(adapterPosition).setSetCount(newSetCount);
    }

    void updateView() {

        if( adapterWorkoutSummary != null ) {
            adapterWorkoutSummary.notifyDataSetChanged();
        }
    }
}
