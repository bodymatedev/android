package de.bodymate.bodymateapp.gui;

import static com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade;

import android.content.Context;
import android.net.Uri;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.ui.SimpleExoPlayerView;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;

import org.jetbrains.annotations.Nullable;

import de.bodymate.bodymateapp.R;

/**
 * Created by Leonard on 04.02.2018.
 *
 */


public class ExerciseVideoPlayer {

    private Context mContext;
    private int exerciseId;

    private SimpleExoPlayer player;

    private RelativeLayout controllerOverlay;
    private SimpleExoPlayerView exoPlayerView;
    private ProgressBar exoLoading;

    private RequestOptions glideReqOpt;

    public ExerciseVideoPlayer(Context context){

        mContext = context;

        glideReqOpt = new RequestOptions()
            .placeholder(R.drawable.placeholder_thumbnail)
            .error(R.drawable.placeholder_thumbnail);
    }

    public void init(final ImageView thumbnail,
                     final RelativeLayout controllerOverlay,
                     final ImageButton playDummy,
                     final SimpleExoPlayerView exoPlayerView,
                     final ProgressBar exoLoading,
                     final String videoURL){

        this.controllerOverlay = controllerOverlay;
        this.exoPlayerView = exoPlayerView;
        this.exoLoading = exoLoading;

        //Hide exoplayer view
        exoPlayerView.setVisibility(View.INVISIBLE);

        //Load thumbnail
        Glide.with(mContext)
            .load("https://i.ytimg.com/vi/vAROcMVP7dw/maxresdefault.jpg")
            .apply(glideReqOpt)
            .transition(withCrossFade())
            .into(thumbnail);

        //Display dummy play button
        controllerOverlay.setVisibility(View.VISIBLE);

        //Set action listener to dummy play button
        playDummy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if( player == null ) {
                    thumbnail.setVisibility(View.GONE);
                    exoPlayerView.setVisibility(View.VISIBLE);
                    loadExternalVideo(videoURL);
                }else{
                    player.setPlayWhenReady(true);
                    exoPlayerView.setUseController(true);
                    exoPlayerView.setControllerShowTimeoutMs(3000);
                }

                controllerOverlay.setVisibility(View.GONE);
            }
        });
    }

    private void loadExternalVideo(@Nullable final String videoURL){

        if( videoURL == null ){
            return;
        }

        //Set up and load video resource
        DefaultBandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();

        TrackSelection.Factory videoTrackSelectionFactory = new AdaptiveTrackSelection.Factory(bandwidthMeter);
        TrackSelector trackSelector = new DefaultTrackSelector(videoTrackSelectionFactory);

        DefaultDataSourceFactory dataSourceFactory = new DefaultDataSourceFactory(
                mContext,
                Util.getUserAgent(mContext, de.bodymate.bodymateapp.util.Util.INSTANCE.getApplicationName(mContext))
        );
        MediaSource videoSource = new ExtractorMediaSource.Factory(dataSourceFactory)
                .createMediaSource(Uri.parse(videoURL));

        player = ExoPlayerFactory.newSimpleInstance(mContext, trackSelector);

        exoPlayerView.setPlayer(player);
        exoPlayerView.setUseController(true);
        exoPlayerView.setControllerShowTimeoutMs(3000);
        player.setPlayWhenReady(true);
        player.prepare(videoSource);

        player.addListener(new Player.DefaultEventListener() {
            @Override
            public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
                if( playbackState == Player.STATE_ENDED ) {
                    player.setPlayWhenReady(false);
                    player.seekTo(0);
                    controllerOverlay.setVisibility(View.VISIBLE);
                    exoPlayerView.setUseController(false);
                }
                if(playbackState == Player.STATE_BUFFERING){
                    exoLoading.setVisibility(View.VISIBLE);
                }else{
                    exoLoading.setVisibility(View.INVISIBLE);
                }
            }
        });
    }

    public void pause(){
        if( player != null ){
            player.setPlayWhenReady(false);
        }
    }

    public void release(){
        if( player != null ){
            player.release();
        }
    }
}
