package de.bodymate.bodymateapp.gui.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import de.bodymate.bodymateapp.R
import de.bodymate.bodymateapp.activity.MainActivity
import de.bodymate.bodymateapp.data.repository.entity.RoomSchedule
import de.bodymate.bodymateapp.gui.popup.PopupChangeSchdule

class AdapterProfileWeekdays(val activity: MainActivity): RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val weekdays: MutableList<Int> = mutableListOf()
    private val schedules: MutableList<RoomSchedule> = mutableListOf()
    private lateinit var parentViewGroup: ViewGroup

    fun setSchedule(schedules: List<RoomSchedule>){

        weekdays.clear()
        weekdays.addAll(schedules.map { it.weekday })

        this.schedules.clear()
        this.schedules.addAll(schedules)

        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

        parentViewGroup = parent

        return when(viewType) {

            VIEW_TYPE_CHIP_CHANGE -> ViewHolderChipChange(LayoutInflater.from(parent.context).inflate(R.layout.card_chip_weekday_change, parent, false))
            VIEW_TYPE_CHIP_WEEKDAY -> ViewHolderChipWeekday(LayoutInflater.from(parent.context).inflate(R.layout.card_chip_weekday, parent, false))
            else -> null!!
        }
    }

    override fun getItemCount(): Int = 1 + weekdays.size

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, adapterPosition: Int) {

        val viewType = getItemViewType(adapterPosition)

        when( viewType ){

            VIEW_TYPE_CHIP_CHANGE -> {

                (viewHolder as ViewHolderChipChange).bind(
                        onClick = {
                            PopupChangeSchdule(activity, parentViewGroup).show(schedules)
                        }
                )
            }
            VIEW_TYPE_CHIP_WEEKDAY -> {

                (viewHolder as ViewHolderChipWeekday).bind(weekdays[adapterPosition - 1])
            }
        }
    }

    override fun getItemViewType(adapterPosition: Int): Int =
            if( adapterPosition == 0 ) VIEW_TYPE_CHIP_CHANGE else VIEW_TYPE_CHIP_WEEKDAY

    class ViewHolderChipChange(itemView: View) : RecyclerView.ViewHolder(itemView){

        fun bind(onClick: () -> Unit){
            itemView.setOnClickListener { onClick() }
        }
    }

    class ViewHolderChipWeekday(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private val tvChipWeekday: TextView = itemView.findViewById(R.id.tvChipWeekday)

        fun bind(weekday: Int){
            tvChipWeekday.setText(getWeekdayString(weekday))
        }
    }

    companion object {

        const val VIEW_TYPE_CHIP_CHANGE = 0
        const val VIEW_TYPE_CHIP_WEEKDAY = 1

        fun getWeekdayString(weekday: Int): Int =
                when (weekday){
                    0 -> R.string.monday
                    1 -> R.string.tuesday
                    2 -> R.string.wednesday
                    3 -> R.string.thursday
                    4 -> R.string.friday
                    5 -> R.string.saturday
                    6 -> R.string.sunday
                    else -> -1
        }
    }
}