package de.bodymate.bodymateapp.gui.popup

import android.app.Application
import android.app.TimePickerDialog
import android.content.Context
import android.support.constraint.ConstraintLayout
import android.support.v7.widget.CardView
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.CompoundButton
import android.widget.PopupWindow
import android.widget.TextView
import br.com.simplepass.loading_button_lib.customViews.CircularProgressButton
import de.bodymate.bodymateapp.CurrentUser
import de.bodymate.bodymateapp.R
import de.bodymate.bodymateapp.api.ApiFunctions
import de.bodymate.bodymateapp.api.requestbody.RequestBodyChangeSchedule
import de.bodymate.bodymateapp.api.requestbody.RequestEntityScheduling
import de.bodymate.bodymateapp.data.repository.entity.RoomSchedule
import de.bodymate.bodymateapp.data.service.ScheduleService
import de.bodymate.bodymateapp.extensions.string
import de.bodymate.bodymateapp.util.*
import kotlinx.coroutines.*


class PopupChangeSchdule(private val context: Context, private val rootView: ViewGroup) {

    private lateinit var popup: PopupWindow

    private val weekdays: MutableList<CheckBox> = mutableListOf()
    private val times: MutableList<TextView> = mutableListOf()

    private lateinit var cpbSaveSchedules: CircularProgressButton
    private lateinit var tvError: TextView

    private var job: Job = Job()

    private val scheduleService: ScheduleService by lazy {
        ScheduleService.getInstance(context.applicationContext as Application)
    }

    fun show(currentSchedule: List<RoomSchedule>){

        //Inflate content view
        val popupView = Util.inflateView(context, R.layout.popup_change_schedule)

        //Create popup
        popup = PopupWindow(popupView, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, true)

        val clPopupChangeSchedule = popupView.findViewById<ConstraintLayout>(R.id.clPopupChangeSchedule)
        clPopupChangeSchedule.setOnClickListener(onClickCancel)

        val cvPopupChangeSchedule = popupView.findViewById<CardView>(R.id.cvPopupChangeSchedule)
        cvPopupChangeSchedule.setOnClickListener {  }

        val tvChangeScheduleCancel = popupView.findViewById<TextView>(R.id.tvChangeScheduleCancel)
        tvChangeScheduleCancel.setOnClickListener(onClickCancel)

        cpbSaveSchedules = popupView.findViewById(R.id.cpbSaveSchedules)
        cpbSaveSchedules.setOnClickListener(onClickSave)

        tvError = popupView.findViewById(R.id.tvChangeScheduleError)

        // Add all CheckBoxes to the list
        popupView.findViewById<CheckBox>(R.id.cbChangeScheduleMonday).apply { weekdays.add(this) }
        popupView.findViewById<CheckBox>(R.id.cbChangeScheduleTuesday).apply { weekdays.add(this) }
        popupView.findViewById<CheckBox>(R.id.cbChangeScheduleWednesday).apply { weekdays.add(this) }
        popupView.findViewById<CheckBox>(R.id.cbChangeScheduleThursday).apply { weekdays.add(this) }
        popupView.findViewById<CheckBox>(R.id.cbChangeScheduleFriday).apply { weekdays.add(this) }
        popupView.findViewById<CheckBox>(R.id.cbChangeScheduleSaturday).apply { weekdays.add(this) }
        popupView.findViewById<CheckBox>(R.id.cbChangeScheduleSunday).apply { weekdays.add(this) }

        // Apply the onCheckedChangeListener to all weekday checkboxes
        weekdays.forEach { it.setOnCheckedChangeListener(onCheckChanged) }

        // Add all Time Views to the list
        popupView.findViewById<TextView>(R.id.tvChangeScheduleTimeMonday).let { times.add(it) }
        popupView.findViewById<TextView>(R.id.tvChangeScheduleTimeTuesday).let { times.add(it) }
        popupView.findViewById<TextView>(R.id.tvChangeScheduleTimeWednesday).let { times.add(it) }
        popupView.findViewById<TextView>(R.id.tvChangeScheduleTimeThursday).let { times.add(it) }
        popupView.findViewById<TextView>(R.id.tvChangeScheduleTimeFriday).let { times.add(it) }
        popupView.findViewById<TextView>(R.id.tvChangeScheduleTimeSaturday).let { times.add(it) }
        popupView.findViewById<TextView>(R.id.tvChangeScheduleTimeSunday).let { times.add(it) }

        // Apply the onClickListener to all Time Views
        times.forEach { it.setOnClickListener(onClickTime) }

        fillViewsToCurrentSchedule(currentSchedule)

        popup.showAtLocation(rootView, Gravity.NO_GRAVITY, 0, 0)
    }

    /**
     * @param currentSchedule The current user's schedules
     */
    private fun fillViewsToCurrentSchedule(currentSchedule: List<RoomSchedule>){

        weekdays.forEach { it.isChecked = false }
        times.forEach { it.visibility = View.INVISIBLE }

        currentSchedule.forEach {
            weekdays[it.weekday].isChecked = true
            times[it.weekday].text = time(it)?.toPrintExtended() ?: "00:00"
            times[it.weekday].visibility = View.VISIBLE
        }
    }

    private val onClickTime = View.OnClickListener {

        val time = timeShort((it as TextView).text.toString())!!

        TimePickerDialog(context,
                TimePickerDialog.OnTimeSetListener {
                    _, hour, minute ->
                    it.text = Time.of(hour, minute).toText(false)
                }, time.hourOfDay, time.minuteOfHour, true)
                .apply {
                    setTitle(context.string(R.string.select_workout_time))
                    show()
                }
    }

    private val onCheckChanged = CompoundButton.OnCheckedChangeListener { checkBox, checked ->

        tvError.visibility = View.GONE

        val weekdayIndex = weekdays.indexOf(checkBox)

        times[weekdayIndex].visibility = if( checked ){
            View.VISIBLE
        }else{
            View.INVISIBLE
        }

        cpbSaveSchedules.isEnabled = weekdaysSelected() >= 2

        cpbSaveSchedules.backgroundTintList = if( weekdaysSelected() < 2 ){
            Util.getOneColorStateList(context, R.color.grey400)
        }else{
            Util.getOneColorStateList(context, R.color.colorPrimaryLight)
        }
    }

    private val onClickSave = View.OnClickListener {

        tvError.visibility = View.GONE

        // Build network request body
        val schedulings = weekdays.filter { it.isChecked }.mapIndexed { weekday, _ ->
            RequestEntityScheduling(weekday, times[weekday].text.toString().plus(":00"))
        }

        cpbSaveSchedules.startAnimation()

        job = GlobalScope.launch(Dispatchers.Main) {

            val newSchedule = withContext(Dispatchers.Default) {
                ApiFunctions.changeSchedule(RequestBodyChangeSchedule(CurrentUser.FIRE_ID!!, schedulings))
            }

            // Check for api error
            if( newSchedule.isNullOrEmpty() ){
                onNetworkError()
                return@launch
            }

            // Update local DB
            withContext(Dispatchers.Default) {
                scheduleService.updateAll(newSchedule)
            }

            // Dismiss this popup
            close()
        }
    }

    private fun weekdaysSelected(): Int = weekdays.count { it.isChecked }

    private fun onNetworkError(){

        cpbSaveSchedules.setBackgroundResource(R.drawable.button_round_green_lite)
        cpbSaveSchedules.revertAnimation()

        tvError.visibility = View.VISIBLE

    }

    private val onClickCancel = View.OnClickListener { close() }

    fun close(){

        job.cancel()
        popup.dismiss()
        cpbSaveSchedules.stopAnimation()
        cpbSaveSchedules.dispose()
    }
}