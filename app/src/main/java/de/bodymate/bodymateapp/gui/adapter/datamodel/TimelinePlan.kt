package de.bodymate.bodymateapp.gui.adapter.datamodel

import android.content.Context
import com.prolificinteractive.materialcalendarview.CalendarDay
import de.bodymate.bodymateapp.R
import de.bodymate.bodymateapp.data.internentity.Plan
import de.bodymate.bodymateapp.util.DateUtils
import de.bodymate.bodymateapp.util.DateUtils.formatToDayAndMonth
import de.bodymate.bodymateapp.util.DateUtils.isToday
import de.bodymate.bodymateapp.util.DateUtils.isTomorrow
import de.bodymate.bodymateapp.util.Time
import de.bodymate.bodymateapp.util.Util.text

/**
 * Created by Leonard on 12.03.2018.
 *
 */

class TimelinePlan(private val context: Context, val plan: Plan, val date: CalendarDay, val time: Time?, val type: Int) {

    val weekday: String
        get() = when {
            isToday(date) -> text(context, R.string.today)
            isTomorrow(date) -> text(context, R.string.tomorrow)
            else -> DateUtils.getWeekdayText(date)
        }

    val dateWithoutYear: String
        get() = if (isToday(date)) {
            ""
        } else {
            formatToDayAndMonth(date)
        }

    companion object {
        const val TYPE_STARTABLE = 0
        const val TYPE_STARTABLE_YESTERDAY = 1
        const val TYPE_STARTABLE_FORWARD = 2
        const val TYPE_COMPLETED = 3
        const val TYPE_VIEW_ONLY = 4
        const val TYPE_LAST = 5
    }
}
