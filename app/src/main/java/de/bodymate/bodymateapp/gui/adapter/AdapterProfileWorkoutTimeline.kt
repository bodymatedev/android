package de.bodymate.bodymateapp.gui.adapter

import android.arch.paging.PagedListAdapter
import android.content.Context
import android.support.v7.util.DiffUtil
import android.view.ViewGroup
import de.bodymate.bodymateapp.api.response.RBWorkout
import de.bodymate.bodymateapp.data.repository.entity.RoomWorkout
import de.bodymate.bodymateapp.gui.viewholder.ViewHolderProfileWorkoutTimeline

class AdapterProfileWorkoutTimeline(val context: Context) : PagedListAdapter<RBWorkout, ViewHolderProfileWorkoutTimeline>(WORKOUT_COMPARATOR){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolderProfileWorkoutTimeline
            = ViewHolderProfileWorkoutTimeline(parent)

    override fun onBindViewHolder(vieHolder: ViewHolderProfileWorkoutTimeline, adapterPosition: Int) {
        vieHolder.bindTo(getItem(adapterPosition), context)
    }

    companion object {

        /**
         * This diff callback informs the PagedListAdapter how to compute list differences when new
         * PagedLists arrive.
         */
        val WORKOUT_COMPARATOR = object: DiffUtil.ItemCallback<RBWorkout>(){

            override fun areItemsTheSame(oldWorkout: RBWorkout, newWorkout: RBWorkout): Boolean {
                return oldWorkout.id == newWorkout.id
            }

            override fun areContentsTheSame(oldWorkout: RBWorkout, newWorkout: RBWorkout): Boolean {
                return oldWorkout == newWorkout
            }
        }
    }
}