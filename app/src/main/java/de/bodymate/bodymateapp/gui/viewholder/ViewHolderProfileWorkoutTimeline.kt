package de.bodymate.bodymateapp.gui.viewholder

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.github.florent37.fiftyshadesof.FiftyShadesOf
import de.bodymate.bodymateapp.R
import de.bodymate.bodymateapp.api.response.RBWorkout
import de.bodymate.bodymateapp.util.DateUtils

class ViewHolderProfileWorkoutTimeline(parent : ViewGroup) : RecyclerView.ViewHolder(
        LayoutInflater.from(parent.context).inflate(R.layout.card_profile_workout_timeline, parent, false)
) {

    private val tvProfileTimelinePlanName = itemView.findViewById<TextView>(R.id.tvProfileTimelinePlanName)
    private val tvProfileTimelineMinutes = itemView.findViewById<TextView>(R.id.tvProfileTimelineMinutes)
    private val tvProfileTimelineDate = itemView.findViewById<TextView>(R.id.tvProfileTimelineDate)

    private val vLoadingCover = itemView.findViewById<View>(R.id.vProfileTimelineLoadingCover)
    private var loadingShades: FiftyShadesOf? = null

    fun bindTo(workout: RBWorkout?, context: Context){

        if( loadingShades == null ){
            loadingShades = FiftyShadesOf.with(context).on(vLoadingCover)
        }

        if( workout == null ){
            vLoadingCover.visibility = View.VISIBLE
            loadingShades?.start()
        }else{
            vLoadingCover.visibility = View.GONE
            loadingShades?.start()
        }

        tvProfileTimelinePlanName.text = workout?.id?.toString()

        val minutesTrained = DateUtils.getMinutesBetween(workout?.datetimeStart, workout?.datetimeEnd)
        tvProfileTimelineMinutes.text = context.getString(R.string.workout_timeline_minutes, minutesTrained.toString())

        tvProfileTimelineDate.text = workout?.plannedFor
    }
}