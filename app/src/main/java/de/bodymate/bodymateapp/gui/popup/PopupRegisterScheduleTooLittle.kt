package de.bodymate.bodymateapp.gui.popup

import android.view.ViewGroup
import android.widget.Button
import de.bodymate.bodymateapp.R
import de.bodymate.bodymateapp.activity.RegisterDetailActivity
import de.bodymate.bodymateapp.util.Util

/**
 * Created by Leonard on 23.04.2018.
 */

class PopupRegisterScheduleTooLittle(private val registerDetailActivity: RegisterDetailActivity, private val root: ViewGroup, val onClickNext: () -> Unit) {

    fun show() {

        val popup = Util.showDimPopup(registerDetailActivity, root, R.layout.schedule_warning_one)

        val bnScheduleWarningSetNewWeekdays = popup.contentView.findViewById<Button>(R.id.bnScheduleWarningSetNewWeekdays)

        bnScheduleWarningSetNewWeekdays.setOnClickListener {
            popup.dismiss()
            bnScheduleWarningSetNewWeekdays.setOnClickListener(null)
        }

        val bnScheduleWarningIgnore = popup.contentView.findViewById<Button>(R.id.bnScheduleWarningIgnore)

        bnScheduleWarningIgnore.setOnClickListener {
            onClickNext()
            popup.dismiss()
            bnScheduleWarningIgnore.setOnClickListener(null)
        }
    }
}
