package de.bodymate.bodymateapp.gui.calendar

import android.graphics.Canvas
import android.graphics.Paint

import com.prolificinteractive.materialcalendarview.spans.DotSpan

/**
 * Created by leonard on 13.06.2017.
 *
 */

internal class MyDotSpan(private val color: Int, private val dotCount: Int) : DotSpan() {

    override fun drawBackground(canvas: Canvas, paint: Paint, left: Int, right: Int, top: Int, baseline: Int, bottom: Int, charSequence: CharSequence?, start: Int, end: Int, lineNum: Int) {

        if (dotCount < 1) return

        val colorOld = paint.color
        paint.color = color

        when (dotCount) {
            1 -> canvas.drawCircle(((left + right) / 2).toFloat(), bottom + RADIUS, RADIUS, paint)
            2 -> {
                canvas.drawCircle(((left + right) / 2 - 7).toFloat(), bottom + RADIUS, RADIUS, paint)
                canvas.drawCircle(((left + right) / 2 + 7).toFloat(), bottom + RADIUS, RADIUS, paint)
            }
            3 -> {
                canvas.drawCircle(((left + right) / 2 - 12).toFloat(), bottom + RADIUS, RADIUS, paint)
                canvas.drawCircle(((left + right) / 2).toFloat(), bottom + RADIUS, RADIUS, paint)
                canvas.drawCircle(((left + right) / 2 + 12).toFloat(), bottom + RADIUS, RADIUS, paint)
            }
            else -> {
                canvas.drawCircle(((left + right) / 2 - 12).toFloat(), bottom + RADIUS, RADIUS, paint)
                canvas.drawCircle(((left + right) / 2).toFloat(), bottom + RADIUS, RADIUS, paint)
                canvas.drawCircle(((left + right) / 2 + 12).toFloat(), (bottom + 5).toFloat(), 3f, paint)
            }
        }

        paint.color = colorOld
    }

    companion object {

        const val ONE = 1
        //const val TWO = 2

        private const val RADIUS = 5f
    }
}
