package de.bodymate.bodymateapp.gui.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import de.bodymate.bodymateapp.R;
import de.bodymate.bodymateapp.api.response.RBEquipment;

/**
 * Created by Leonard on 30.05.2018.
 */
public class AdapterEquipments extends RecyclerView.Adapter<AdapterEquipments.ViewHolderEquipments>{

	private List<RBEquipment> rbEquipments;
	private final Set<Integer> selectedEquipmentIds;

	public AdapterEquipments(List<RBEquipment> rbEquipments){

		this.rbEquipments = rbEquipments;
		selectedEquipmentIds = new HashSet<>();
	}

	@NonNull
	@Override
	public ViewHolderEquipments onCreateViewHolder(@NonNull ViewGroup parent, int viewType){

		return new AdapterEquipments.ViewHolderEquipments(LayoutInflater.from(parent.getContext())
				.inflate(R.layout.equipment_selectable, parent, false));
	}

	@Override
	public void onBindViewHolder(@NonNull ViewHolderEquipments holder, int position){

		StringBuilder nameBuilder = new StringBuilder(rbEquipments.get(position).getName());

		final String spec = rbEquipments.get(position).getSpecification();
		if( spec != null && spec.length() > 0 ){
			nameBuilder.append(" - ").append(spec);
		}

		holder.tvEquipmentSelectableName.setText(nameBuilder);
	}

	@Override
	public int getItemCount(){
		return rbEquipments.size();
	}

	public void setEquipments(List<RBEquipment> rbEquipments){
		this.rbEquipments = rbEquipments;
		notifyDataSetChanged();
	}

	public Set<Integer> getSelectedEquipmentIds(){
		return selectedEquipmentIds;
	}

	final class ViewHolderEquipments extends RecyclerView.ViewHolder{

		TextView tvEquipmentSelectableName;
		CheckBox cbEquipmentSelectable;

		ViewHolderEquipments(View itemView){

			super(itemView);

			tvEquipmentSelectableName = itemView.findViewById(R.id.tvEquipmentSelectableName);
			cbEquipmentSelectable = itemView.findViewById(R.id.cbEquipmentSelectable);

			cbEquipmentSelectable.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener(){
				@Override
				public void onCheckedChanged(CompoundButton buttonView, boolean isChecked){
					if( isChecked ){
						selectedEquipmentIds.add(rbEquipments.get(getAdapterPosition()).getId());
					}else{
						selectedEquipmentIds.remove(rbEquipments.get(getAdapterPosition()).getId());
					}
				}
			});
		}
	}
}
