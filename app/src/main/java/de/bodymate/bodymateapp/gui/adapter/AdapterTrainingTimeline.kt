package de.bodymate.bodymateapp.gui.adapter

import android.content.Context
import android.support.annotation.DrawableRes
import android.support.v7.widget.CardView
import android.support.v7.widget.RecyclerView
import android.util.SparseArray
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.prolificinteractive.materialcalendarview.CalendarDay
import com.prolificinteractive.materialcalendarview.CalendarDay.today
import de.bodymate.bodymateapp.util.C
import de.bodymate.bodymateapp.util.C.PLAN_ID_FITNESSTEST
import de.bodymate.bodymateapp.R
import de.bodymate.bodymateapp.gui.adapter.datamodel.TimelinePlan
import de.bodymate.bodymateapp.gui.adapter.datamodel.TimelinePlan.Companion.TYPE_COMPLETED
import de.bodymate.bodymateapp.gui.adapter.datamodel.TimelinePlan.Companion.TYPE_LAST
import de.bodymate.bodymateapp.gui.adapter.datamodel.TimelinePlan.Companion.TYPE_STARTABLE
import de.bodymate.bodymateapp.gui.adapter.datamodel.TimelinePlan.Companion.TYPE_STARTABLE_FORWARD
import de.bodymate.bodymateapp.gui.adapter.datamodel.TimelinePlan.Companion.TYPE_STARTABLE_YESTERDAY
import de.bodymate.bodymateapp.gui.adapter.datamodel.TimelinePlan.Companion.TYPE_VIEW_ONLY
import de.bodymate.bodymateapp.data.internentity.Plan
import de.bodymate.bodymateapp.data.repository.entity.RoomExercise
import de.bodymate.bodymateapp.data.repository.entity.RoomSchedule
import de.bodymate.bodymateapp.data.repository.entity.RoomWorkout
import de.bodymate.bodymateapp.fragment.FragmentTraining
import de.bodymate.bodymateapp.gui.popup.PopupPlanDetail
import de.bodymate.bodymateapp.util.DateUtils
import de.bodymate.bodymateapp.util.Time
import de.bodymate.bodymateapp.util.Util
import java.util.*

/**
 * Created by Leonard on 12.03.2018.
 */

class AdapterTrainingTimeline(private val fragmentTraining: FragmentTraining) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val mContext: Context? = fragmentTraining.context
    private var timelinePlans: List<TimelinePlan> = emptyList()
    private lateinit var exercises: List<RoomExercise>

    fun updateView(plans: List<Plan>, exercises: List<RoomExercise>, schedules: List<RoomSchedule>, latestWorkouts: List<RoomWorkout>?, latestPlanNumber: Int) {

        this.exercises = exercises

        if (plans.size > 1 || plans.size == 1 && plans[0].id != 1) {

            timelinePlans = generateTimelinePlans(plans, schedules, latestWorkouts, latestPlanNumber)

        } else if (plans.size == 1 && plans[0].id == 1) {

            //Only the fitnesstest is present
            timelinePlans = listOf(TimelinePlan(mContext!!, plans[0], today(), Time.of(18, 0), TYPE_STARTABLE))
        }

        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

        return when (viewType) {

            TYPE_STARTABLE, TYPE_STARTABLE_YESTERDAY, TYPE_STARTABLE_FORWARD ->
                VhTimeLineStartable(LayoutInflater.from(parent.context).inflate(R.layout.card_workout_timeline_startable, parent, false))

            TYPE_COMPLETED ->
                VhTimeLineCompleted(LayoutInflater.from(parent.context).inflate(R.layout.card_workout_timeline_completed, parent, false))

            TYPE_VIEW_ONLY ->
                VhTimeLine(LayoutInflater.from(parent.context).inflate(R.layout.card_workout_timeline, parent, false))

            else -> ViewHolderTimelineTail(LayoutInflater.from(parent.context).inflate(R.layout.card_workout_timeline_tail, parent, false))
        }
    }

    override fun onBindViewHolder(vHolder: RecyclerView.ViewHolder, adapterPosition: Int) {

        val itemViewType = getItemViewType(adapterPosition)

        when (itemViewType) {

            TYPE_STARTABLE, TYPE_STARTABLE_YESTERDAY, TYPE_STARTABLE_FORWARD -> {

                val vhPlanStartable = vHolder as VhTimeLineStartable
                val timelinePlan = timelinePlans[adapterPosition]

                val cvWorkoutTimelinePlan = vhPlanStartable.itemView.findViewById<CardView>(R.id.cvWorkoutTimelinePlan)

                vhPlanStartable.bind(timelinePlan.weekday,
                        timelinePlan.plan.getDisplayName(mContext!!),
                        timelinePlan.plan.displayPicture,
                        adapterPosition,
                        onClickCard,
                        View.OnClickListener { fragmentTraining.onClickWorkoutStart(cvWorkoutTimelinePlan, timelinePlan) })

                when (itemViewType) {
                    TYPE_STARTABLE -> vhPlanStartable.tvTrainingTimelinePlanWeekday.setTextColor(Util.color(mContext, R.color.colorPrimary))
                    TYPE_STARTABLE_YESTERDAY -> vhPlanStartable.tvWorkoutTimelineBottom.setText(R.string.workout_catch_up)
                    else -> vhPlanStartable.tvWorkoutTimelineBottom.setText(R.string.workout_forward)
                }
            }
            TYPE_VIEW_ONLY -> {

                val timelinePlan = timelinePlans[adapterPosition]

                (vHolder as VhTimeLine).bind(timelinePlan.weekday,
                        timelinePlan.dateWithoutYear,
                        timelinePlan.plan.getDisplayName(mContext!!),
                        timelinePlan.plan.displayPicture,
                        adapterPosition,
                        onClickCard)
            }

            TYPE_COMPLETED -> {

                val timelinePlan = timelinePlans[adapterPosition]

                (vHolder as VhTimeLineCompleted).bind(timelinePlan.weekday, timelinePlan.plan.displayPicture, timelinePlan.plan.getDisplayName(mContext!!))
            }
            TYPE_LAST -> {

            }
        }
    }

    override fun getItemCount(): Int {
        return timelinePlans.size + 1
    }

    override fun getItemViewType(adapterPosition: Int): Int {

        return if (adapterPosition == timelinePlans.size) {
            TYPE_LAST
        } else {
            timelinePlans[adapterPosition].type
        }
    }

    private fun generateTimelinePlans(plans: List<Plan>, schedules: List<RoomSchedule>, latestWorkouts: List<RoomWorkout>?, lastTrainedPlanNumber: Int): List<TimelinePlan> {

        val timeline = ArrayList<TimelinePlan>()

        //Today
        val calCounter = Calendar.getInstance(DateUtils.LOCALE)
        calCounter.time = today().date

        //Sunday in 2 weeks
        val calEnd = Calendar.getInstance(DateUtils.LOCALE)
        calEnd.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY)
        calEnd.add(Calendar.DATE, 14)

        val currentPlans = SparseArray<Plan>()

        plans.forEach {
            if (it.number == C.PLAN_ONE) {
                currentPlans.append(C.PLAN_ONE, it)
            } else if (it.number == C.PLAN_TWO) {
                currentPlans.append(C.PLAN_TWO, it)
            }
        }

        var yesterdayCompleted = false
        var considerOfferingForwardPlan = true

        latestWorkouts?.filter { roomWorkout -> !DateUtils.getCalendarDayFromSqlDate(roomWorkout.plannedFor).isBefore(DateUtils.yesterday()) }?.forEach {

            if(DateUtils.yesterday() == DateUtils.getCalendarDayFromSqlDate(it.plannedFor) || it.planId == PLAN_ID_FITNESSTEST ){
                yesterdayCompleted = true
            }

            if( DateUtils.getCalendarDayFromSqlDate(it.plannedFor).isAfter(today()) ){
                considerOfferingForwardPlan = false
            }

            val plan = plans.find { plan -> plan.id == it.planId }!!
            val plannedFor: CalendarDay = if (plan.id == PLAN_ID_FITNESSTEST) DateUtils.getDateFromSqlDateTime(it.datetimeStart) else DateUtils.getCalendarDayFromSqlDate(it.plannedFor)

            timeline.add(TimelinePlan(mContext!!,
                    plan,
                    plannedFor,
                    DateUtils.getTimeFromSqlDateTime(it.datetimeStart),
                    TYPE_COMPLETED))

            calCounter.time = DateUtils.getCalendarDayFromSqlDate(it.plannedFor).date
            calCounter.add(Calendar.DATE, 1)
        }

        val scheduleYesterday = getScheduleForDate(schedules, DateUtils.yesterday())

        //Check if yesterday was a workout day and no workout was completed
        if (scheduleYesterday != null && !yesterdayCompleted) {

            val planNumber: Int = if (lastTrainedPlanNumber == C.PLAN_ONE || lastTrainedPlanNumber == C.PLAN_TWO) {
                if (lastTrainedPlanNumber == C.PLAN_ONE) C.PLAN_TWO else C.PLAN_ONE
            } else {
                C.PLAN_ONE
            }

            //Allow the user to start a workout that was originally planned for yesterday
            timeline.add(TimelinePlan(mContext!!,
                    currentPlans.get(planNumber),
                    DateUtils.yesterday(),
                    Time.of(scheduleYesterday.time),
                    TYPE_STARTABLE_YESTERDAY))
        }

        var currentPlanNumber: Int

        currentPlanNumber = if (lastTrainedPlanNumber == C.PLAN_ONE || lastTrainedPlanNumber == C.PLAN_TWO) {
            if (lastTrainedPlanNumber == C.PLAN_ONE) C.PLAN_TWO else C.PLAN_ONE
        } else {
            C.PLAN_ONE
        }

        while (calCounter.before(calEnd)) {

            val day = CalendarDay.from(calCounter.time)
            val schedule = getScheduleForDate(schedules, day)

            if (schedule == null) {
                calCounter.add(Calendar.DATE, 1)
                continue
            }

            val type: Int

            if (day == today()) {
                type = TYPE_STARTABLE
            } else if (considerOfferingForwardPlan && day.isAfter(today())) {
                considerOfferingForwardPlan = false
                type = TYPE_STARTABLE_FORWARD
            } else {
                type = TYPE_VIEW_ONLY
            }

            //Add view-only plan card fot future
            timeline.add(TimelinePlan(mContext!!,
                    currentPlans.get(currentPlanNumber),
                    day,
                    Time.of(schedule.time),
                    type))

            currentPlanNumber = if (currentPlanNumber == C.PLAN_ONE) C.PLAN_TWO else C.PLAN_ONE
            calCounter.add(Calendar.DATE, 1)
        }

        return timeline
    }

    private val onClickCard = View.OnClickListener {

        val adapterPosition = it.tag as Int

        val popupPlanDetail = PopupPlanDetail(mContext!!, fragmentTraining.parentRootView)

        val timelinePlan = timelinePlans[adapterPosition]

        if (timelinePlan.type == TYPE_STARTABLE || timelinePlan.type == TYPE_STARTABLE_FORWARD || timelinePlan.type == TYPE_STARTABLE_YESTERDAY) {

            lateinit var buttonText: String

            when {
                timelinePlan.type == TYPE_STARTABLE -> buttonText = Util.text(mContext, R.string.workout_start)
                timelinePlan.type == TYPE_STARTABLE_YESTERDAY -> buttonText = Util.text(mContext, R.string.workout_catch_up)
                timelinePlan.type == TYPE_STARTABLE_FORWARD -> buttonText = Util.text(mContext, R.string.workout_forward)
            }

            popupPlanDetail.show(timelinePlan.plan, exercises, View.OnClickListener { fragmentTraining.onClickWorkoutStart(popupPlanDetail, timelinePlan) }, buttonText)

        } else {
            popupPlanDetail.show(timelinePlan.plan, exercises, null, null)
        }
    }

    private fun getScheduleForDate(schedules: List<RoomSchedule>, day: CalendarDay): RoomSchedule? {

        val weekday = DateUtils.getWeekday(day)

        for (schedule in schedules) {
            if (schedule.weekday == weekday) {
                return schedule
            }
        }
        return null
    }


    fun getTimelinePosition(dateClicked: CalendarDay): Int {

        var nearest = -1

        for (i in timelinePlans.indices) {
            if (timelinePlans[i].date == dateClicked || timelinePlans[i].date.isAfter(dateClicked)) {
                return i
            } else {
                nearest = i
            }
        }
        return nearest
    }

    internal class VhTimeLine(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private val tvTrainingTimelinePlanWeekday: TextView = itemView.findViewById(R.id.tvTrainingTimelinePlanWeekday)
        private val tvTrainingTimelinePlanDate: TextView = itemView.findViewById(R.id.tvTrainingTimelinePlanDate)
        private val ivWorkoutTimelinePicture: ImageView = itemView.findViewById(R.id.ivWorkoutTimelinePicture)
        private val tvTrainingTimelinePlanName: TextView = itemView.findViewById(R.id.tvWorkoutTimelinePlanName)
        private val cvTrainingTimelineEntity: CardView = itemView.findViewById(R.id.cvWorkoutTimelinePlan)

        fun bind(weekday: String, date: String, planName: String, @DrawableRes picture: Int, cardTag: Int, onClickCard: View.OnClickListener){

            tvTrainingTimelinePlanWeekday.text = weekday
            tvTrainingTimelinePlanDate.text = date
            tvTrainingTimelinePlanName.text = planName
            ivWorkoutTimelinePicture.setImageResource(picture)
            cvTrainingTimelineEntity.tag = cardTag
            cvTrainingTimelineEntity.setOnClickListener(onClickCard)
        }
    }

    internal class VhTimeLineStartable(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val tvTrainingTimelinePlanWeekday: TextView = itemView.findViewById(R.id.tvTrainingTimelinePlanWeekday)
        private val ivWorkoutTimelinePicture: ImageView = itemView.findViewById(R.id.ivWorkoutTimelinePicture)
        private val tvTrainingTimelinePlanName: TextView = itemView.findViewById(R.id.tvWorkoutTimelinePlanName)
        private val cvTrainingTimelineEntity: CardView = itemView.findViewById(R.id.cvWorkoutTimelinePlan)
        val tvWorkoutTimelineBottom: TextView = itemView.findViewById(R.id.tvWorkoutTimelineBottom)

        fun bind(weekday: String, planName: String, @DrawableRes picture: Int, cardTag: Int, onClickCard: View.OnClickListener, onClickButton: View.OnClickListener){

            tvTrainingTimelinePlanWeekday.text = weekday
            tvTrainingTimelinePlanName.text = planName
            ivWorkoutTimelinePicture.setImageResource(picture)
            cvTrainingTimelineEntity.tag = cardTag
            cvTrainingTimelineEntity.setOnClickListener(onClickCard)
            tvWorkoutTimelineBottom.setOnClickListener(onClickButton)
        }
    }

    internal class VhTimeLineCompleted(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private val tvTrainingTimelinePlanWeekday: TextView = itemView.findViewById(R.id.tvTrainingTimelinePlanWeekday)
        private val ivWorkoutTimelinePicture: ImageView = itemView.findViewById(R.id.ivWorkoutTimelinePicture)
        private val tvTrainingTimelinePlanName: TextView = itemView.findViewById(R.id.tvWorkoutTimelinePlanName)

        fun bind(weekday: String, @DrawableRes picture: Int, planName: String) {

            tvTrainingTimelinePlanWeekday.text = weekday
            ivWorkoutTimelinePicture.setImageResource(picture)
            tvTrainingTimelinePlanName.text = planName
        }
    }

    internal class ViewHolderTimelineTail(itemView: View) : RecyclerView.ViewHolder(itemView)
}
