package de.bodymate.bodymateapp.gui

import android.graphics.Rect
import android.support.v7.widget.RecyclerView
import android.view.View

/**
 * Created by leonard on 01.05.2017.
 */

class RecyclerViewDecoratorVertical(private val itemCount: Int, private val spacing: Int, private val includeEdges: Boolean) : RecyclerView.ItemDecoration() {

    override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {

        val position = parent.getChildAdapterPosition(view)

        outRect.top = if (position > 0) spacing / 2 else spacing
        outRect.bottom = if (position < itemCount - 1) spacing / 2 else spacing

        if (includeEdges) {
            outRect.left = spacing
            outRect.right = outRect.left
        }
    }
}