package de.bodymate.bodymateapp.gui

import android.content.Context
import android.support.v7.widget.LinearLayoutManager

/**
 * Created by leonard on 05.11.2017.
 *
 */

class LinearLayoutManagerWorkout internal constructor(context: Context, orientation: Int) : LinearLayoutManager(context, orientation, false) {

    var scrollable = true

    override fun canScrollHorizontally(): Boolean = scrollable && super.canScrollHorizontally()

    override fun canScrollVertically(): Boolean = scrollable && super.canScrollVertically()
}