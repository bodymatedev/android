package de.bodymate.bodymateapp.gui.adapter.datamodel;

import org.jetbrains.annotations.Contract;

/**
 * Created by Leonard on 21.01.2018.
 *
 */

public final class WorkoutSummary {

    private final int positionInPlan;
    private final String exerciseName;
    private int setCount, repSum;
    private final String type;
    private int setsGoal, repsTotalGoal;

    public WorkoutSummary(int positionInPlan, String exerciseName, int setCount, int repSum, int setsGoal, int repsTotalGoal, String type){

        this.positionInPlan = positionInPlan;
        this.exerciseName = exerciseName;
        this.setCount = setCount;
        this.repSum = repSum;
        this.setsGoal = setsGoal;
        this.repsTotalGoal = (repsTotalGoal > 0) ? repsTotalGoal: Integer.MAX_VALUE;
        this.type = type;
    }

    @Contract(pure = true)
    public int getPositionInPlan(){
        return positionInPlan;
    }

    @Contract(pure = true)
    public String getExerciseName() {
        return exerciseName;
    }

    @Contract(pure = true)
    public int getSetCount() {
        return setCount;
    }

    public void setSetCount(int newSetCount){
        this.setCount = newSetCount;
    }

    @Contract(pure = true)
    public int getRepSum() {
        return repSum;
    }

    @Contract(pure = true)
    public String getType(){
        return type;
    }

    @Contract( pure = true )
    public int getSetsGoal(){
        return setsGoal;
    }

    @Contract( pure = true )
    public int getRepsTotalGoal(){
        return repsTotalGoal;
    }

    public void setRepSum(int repSum){
        this.repSum = repSum;
    }
}
