package de.bodymate.bodymateapp.gui.popup

import android.view.ViewGroup
import android.widget.ImageView
import android.widget.PopupWindow
import android.widget.TextView
import br.com.simplepass.loading_button_lib.customViews.CircularProgressButton
import de.bodymate.bodymateapp.R
import de.bodymate.bodymateapp.activity.WorkoutActivity
import de.bodymate.bodymateapp.util.Util

/**
 * Created by Leonard on 27.01.2018.
 *
 */

class PopupWorkoutCancel(private val workoutActivity: WorkoutActivity, private val root: ViewGroup) {

    private lateinit var popup: PopupWindow

    var popupListener: PopupListener? = null

    fun show() {

        popup =  Util.showDimPopupWidth(workoutActivity, root, R.layout.popup_workout_cancel, .9)

        val popupView = popup.contentView

        val tvWorkoutReject = popupView.findViewById<TextView>(R.id.tvWorkoutReject)
        val bnWorkoutCancelSave = popupView.findViewById<CircularProgressButton>(R.id.bnWorkoutCancelSave)
        val ivPopupClose = popupView.findViewById<ImageView>(R.id.ivPopupClose)

        tvWorkoutReject.setOnClickListener {
            popup.dismiss()
            workoutActivity.finish()
        }

        ivPopupClose.setOnClickListener {
            popup.dismiss()
        }

        bnWorkoutCancelSave.setOnClickListener { workoutActivity.finishWorkout(bnWorkoutCancelSave) }

        popup.setOnDismissListener { popupListener?.onClose() }
    }

    interface PopupListener {
        fun onClose()
    }
}
