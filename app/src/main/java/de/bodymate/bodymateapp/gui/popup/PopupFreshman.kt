package de.bodymate.bodymateapp.gui.popup

import android.content.Intent
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.FrameLayout
import android.widget.PopupWindow
import android.widget.TextView
import com.prolificinteractive.materialcalendarview.CalendarDay
import de.bodymate.bodymateapp.util.C.PLAN_ID_FITNESSTEST
import de.bodymate.bodymateapp.R
import de.bodymate.bodymateapp.activity.MainActivity
import de.bodymate.bodymateapp.activity.WorkoutActivity
import de.bodymate.bodymateapp.util.DateUtils
import de.bodymate.bodymateapp.util.Util

class PopupFreshman(private val activityMain: MainActivity, private val root: ViewGroup) {

    private lateinit var popup: PopupWindow
    private lateinit var flFitnesstestStart: FrameLayout
    private lateinit var tvFitnesstestLater: TextView

    fun show() {

        //Inflate content view
        val popupView = Util.inflateView(activityMain, R.layout.popup_freshman)

        //Create popup
        popup = PopupWindow(popupView, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, true)
        popup.animationStyle = R.style.popupAnimationStyle

        flFitnesstestStart = popupView.findViewById(R.id.flFitnesstestStart)
        tvFitnesstestLater = popupView.findViewById(R.id.tvFitnesstestLater)

        flFitnesstestStart.setOnClickListener { startFitnesstest() }
        tvFitnesstestLater.setOnClickListener { close() }

        val popInAnim = AnimationUtils.loadAnimation(activityMain, R.anim.grow_in)

        popInAnim.setAnimationListener(object : Animation.AnimationListener{
            override fun onAnimationRepeat(animation: Animation?) {}

            override fun onAnimationEnd(animation: Animation?) {
                flFitnesstestStart.visibility = View.VISIBLE
            }

            override fun onAnimationStart(animation: Animation?) {
                flFitnesstestStart.visibility = View.VISIBLE
            }
        })

        popup.showAtLocation(root, Gravity.NO_GRAVITY, 0, 0)

        flFitnesstestStart.startAnimation(popInAnim)
    }

    private fun startFitnesstest(){

        val intent = Intent(activityMain, WorkoutActivity::class.java)
        intent.putExtra(WorkoutActivity.EXTRA_PLAN_ID, PLAN_ID_FITNESSTEST)
        intent.putExtra(WorkoutActivity.EXTRA_PLAN_PLANNED_FOR, DateUtils.getSqlDateFromCalendarDay(CalendarDay.today()))
        activityMain.startActivity(intent)

        close()
    }

    fun close(){

        flFitnesstestStart.setOnClickListener(null)
        tvFitnesstestLater.setOnClickListener(null)

        popup.dismiss()
    }
}