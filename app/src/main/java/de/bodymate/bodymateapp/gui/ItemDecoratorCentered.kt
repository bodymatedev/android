package de.bodymate.bodymateapp.gui

import android.graphics.Rect
import android.support.v7.widget.RecyclerView
import android.view.View

/**
 * Created by Leonard on 10.12.2017.
 *
 */

class ItemDecoratorCentered internal constructor(private val itemCount: Int, private val spacingY: Int, private val cardSizeFactorX: Float, private val cardSizeFactorY: Float) : RecyclerView.ItemDecoration() {

    private var itemWidth: Int = 0
    private var spacingYFirstLast: Int = 0

    private var measuredItems = false

    fun informAboutSelfSpacingVertical(selfSpacing: Int) {
        spacingYFirstLast -= selfSpacing
    }

    override fun getItemOffsets(rect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {

        if (!measuredItems) {

            itemWidth = (parent.width * cardSizeFactorX).toInt()

            //Calculate spacing to center item on screen
            spacingYFirstLast = (parent.height - parent.height * cardSizeFactorY).toInt() / 2

            measuredItems = true
        }

        val position = parent.getChildAdapterPosition(view)

        //Top and Bottom spacings
        rect.top = if (position > 0) spacingY / 2 else spacingYFirstLast
        rect.bottom = if (position < itemCount) spacingY / 2 else spacingYFirstLast

        //Left and Right spacings
        rect.left = (parent.width - itemWidth) / 2
        rect.right = rect.left
    }
}
