package de.bodymate.bodymateapp.gui;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PagerSnapHelper;
import android.support.v7.widget.RecyclerView;
import android.util.SparseArray;
import android.util.SparseIntArray;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.TextView;

import com.dd.morphingbutton.MorphingAnimation;
import com.dd.morphingbutton.MorphingButton;

import org.jetbrains.annotations.NotNull;

import java.util.HashSet;

import de.bodymate.bodymateapp.data.internentity.Exercise;
import de.bodymate.bodymateapp.gui.popup.PopupExerciseInfo;
import de.bodymate.bodymateapp.util.C;
import de.bodymate.bodymateapp.R;
import de.bodymate.bodymateapp.WorkoutLogger;
import de.bodymate.bodymateapp.activity.WorkoutActivity;
import de.bodymate.bodymateapp.gui.adapter.AdapterWorkoutExercise;
import de.bodymate.bodymateapp.data.internentity.Plan;
import de.bodymate.bodymateapp.data.internentity.PlanPosition;
import de.bodymate.bodymateapp.util.ExerciseTimer;
import de.bodymate.bodymateapp.util.Util;
import de.bodymate.bodymateapp.viewmodel.ViewModelActivityWorkout;

/**
 * Created by Leonard on 03.01.2018.
 *
 */

public class WorkoutExerciseViewer{

	private static final int SPACING_VERTICAL = 30;
	private static final int SELF_SPACING_VERTICAL = 60;
	public static final float CARD_SIZE_FACTOR_X = 0.9f;
	public static final float CARD_SIZE_FACTOR_Y = 0.9f;

	private WorkoutActivity workoutActivity;
	private Plan plan;
	private RecyclerView recyclerView;
	private AdapterWorkoutExercise adapterWorkoutExercise;
	private WorkoutLogger workoutLogger;
	private LinearLayoutManagerWorkout layoutManagerExercises;
	private SparseArray<SparseArray<ExerciseTimer>> timers;
	private SparseArray<SparseArray<TextView>> counters;
	private SparseIntArray selectedSetIds;
	private SparseArray<HashSet<Integer>> setsDone;
	private SparseIntArray additionalSets;
	private SparseArray<ExerciseVideoPlayer> videoPlayers;
	private boolean touchEventsDisabled = false;
	private int currentAdapterPosition;
	private AnimationSet animationSetTimerReset;
	private Animation animationSlideInLeft, animationSlideOutLeft, animationSlideInRight, animationSlideOutRight;
	private MorphingButton.Params morphToCircle;
	private WorkoutSummaryViewer workoutSummaryViewer;

	private ViewModelActivityWorkout modelActivityWorkout;
	private int completedStandardSets = 0;

	public WorkoutExerciseViewer(WorkoutActivity workoutActivity, Plan plan, RecyclerView recyclerView){

		this.workoutActivity = workoutActivity;
		this.plan = plan;
		this.recyclerView = recyclerView;

		modelActivityWorkout = ViewModelProviders.of(workoutActivity).get(ViewModelActivityWorkout.class);

		initSelf();
		createUX();
	}

	private void initSelf(){

		//Step 1: Create workout logger
		workoutLogger = new WorkoutLogger(plan);

		//Step 2: Create multidimensional array of of ExerciseTimer's
		timers = new SparseArray<>();

		for( int i = 0; i < plan.getPositions().size(); i++ ){
			if( plan.getExerciseAt(i).isTypeTime() ){
				SparseArray<ExerciseTimer> exerciseTimerArray = new SparseArray<>();
				for( int j = 0; j < plan.getSetCountAt(i); j++ ){
					exerciseTimerArray.append(j, ExerciseTimer.Companion.create(plan.getRepsAt(i, j)));
				}
				timers.append(i, exerciseTimerArray);
			}
		}

		//Step 3: Initialize multidimensional array of textview references to the rep counters for non-time allExercises
		counters = new SparseArray<>();
		for( int i = 0; i < plan.positionCount(); i++ ){
			if( plan.getExerciseAt(i).isTypeReps() ){
				counters.append(i, new SparseArray<TextView>());
			}
		}

		//Step 4: Initialize sparse array holding current selected set-id for every plan position
		selectedSetIds = new SparseIntArray();
		for( int i = 0; i < plan.positionCount(); i++ ){
			selectedSetIds.append(i, 0);
		}

		//Step 5: Initialize array for the additional set-counters
		additionalSets = new SparseIntArray();

		//Step 6: Initialize sparse array holding sets which are marked as done
		setsDone = new SparseArray<>();
		for( int i = 0; i < plan.positionCount(); i++ ){
			setsDone.put(i, new HashSet<Integer>());
		}

		//Step 7: Initialize list that holds allEquipments video players
		videoPlayers = new SparseArray<>();

		//Step 8: Initialize animations
		animationSlideInLeft = AnimationUtils.loadAnimation(workoutActivity, android.R.anim.slide_in_left);
		animationSlideInRight = AnimationUtils.loadAnimation(workoutActivity, R.anim.slide_in_right_a);
		animationSlideOutLeft = AnimationUtils.loadAnimation(workoutActivity, R.anim.slide_out_to_left_fade);
		animationSlideOutRight = AnimationUtils.loadAnimation(workoutActivity, android.R.anim.slide_out_right);

		Animation rotateLeft = AnimationUtils.loadAnimation(workoutActivity, R.anim.rotate_left);
		rotateLeft.setDuration(400);
		Animation alphaOut = AnimationUtils.loadAnimation(workoutActivity, R.anim.alpha_out);
		alphaOut.setDuration(300);
		animationSetTimerReset = new AnimationSet(false);
		animationSetTimerReset.addAnimation(rotateLeft);
		animationSetTimerReset.addAnimation(alphaOut);

		morphToCircle = MorphingButton.Params.create()
				.duration(300)
				.cornerRadius(Util.INSTANCE.dimen(workoutActivity, R.dimen.set_done_corner_radius))
				.width((Util.INSTANCE.dimen(workoutActivity, R.dimen.set_done_morph_finish)))
				.height((Util.INSTANCE.dimen(workoutActivity, R.dimen.set_done_morph_finish)))
				.colorPressed(Util.INSTANCE.color(workoutActivity, R.color.green_light))
				.icon(R.drawable.ic_check);

	}

	private void createUX(){

		//Step 1: Create Adapter for Exercises
		adapterWorkoutExercise = new AdapterWorkoutExercise(this);

		layoutManagerExercises = new LinearLayoutManagerWorkout(workoutActivity, LinearLayoutManager.VERTICAL);
		recyclerView.setLayoutManager(layoutManagerExercises);

		//Step 2: Create Item Decorator
		ItemDecoratorCentered decoratorCentered = new ItemDecoratorCentered(plan.getPositions().size(),
				SPACING_VERTICAL, CARD_SIZE_FACTOR_X, CARD_SIZE_FACTOR_Y);

		decoratorCentered.informAboutSelfSpacingVertical(SELF_SPACING_VERTICAL);
		recyclerView.addItemDecoration(decoratorCentered);

		//Step 3: Apply one by one snap helper
		if( recyclerView.getOnFlingListener() == null ) {
			new PagerSnapHelper().attachToRecyclerView(recyclerView);
		}

		//Step 4: Assign Adapter to View
		recyclerView.setAdapter(adapterWorkoutExercise);

		//Step 5: Set scroll listener
		recyclerView.addOnScrollListener(onVerticalScroll);

		//Step 6: Create workout summary viewer
		workoutSummaryViewer = new WorkoutSummaryViewer(plan, workoutActivity);
	}


	public Plan getPlan(){
		return plan;
	}

	public WorkoutActivity getActivity(){
		return workoutActivity;
	}

	private final RecyclerView.OnScrollListener onVerticalScroll = new RecyclerView.OnScrollListener(){
		@Override
		public void onScrollStateChanged(RecyclerView recyclerView, int newState){

			super.onScrollStateChanged(recyclerView, newState);

			if( newState == RecyclerView.SCROLL_STATE_IDLE ){

				final int newAdapterPosition = layoutManagerExercises.findFirstCompletelyVisibleItemPosition();

				if( newAdapterPosition == currentAdapterPosition || newAdapterPosition == -1 ){
					//No scroll changes at allEquipments
					return;
				}

				if( adapterWorkoutExercise.getItemViewType(currentAdapterPosition) == AdapterWorkoutExercise.VIEW_TYPE_EXERCISE){

					//Stop the timer page the old selected set view (if exercise type is TIME)
					if( plan.getExerciseAt(currentAdapterPosition).isTypeTime() ){
						timers.get(currentAdapterPosition).get(getCurrentSetId()).pause();
					}

					//Stop video player of old visible RespExercise
					videoPlayers.get(currentAdapterPosition).pause();
				}

				//Update the progress bar
				if( adapterWorkoutExercise.getItemViewType(newAdapterPosition) == AdapterWorkoutExercise.VIEW_TYPE_EXERCISE) {

					//Update progress bar
					if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
						workoutActivity.getPbWorkoutProgress().setProgress(newAdapterPosition, true);
					} else {
						workoutActivity.getPbWorkoutProgress().setProgress(newAdapterPosition);
					}
				}

				//Hide the progress bar and the top timer if on last card
				if( adapterWorkoutExercise.getItemViewType(newAdapterPosition) == AdapterWorkoutExercise.VIEW_TYPE_END ){
					workoutActivity.getPbWorkoutProgress().setVisibility(View.INVISIBLE);
					workoutActivity.getTvWorkoutTime().setVisibility(View.INVISIBLE);
				}else{
					workoutActivity.getPbWorkoutProgress().setVisibility(View.VISIBLE);
					workoutActivity.getTvWorkoutTime().setVisibility(View.VISIBLE);
				}

				//Update current visible plan position
				currentAdapterPosition = newAdapterPosition;
			}
		}
	};

	public void appendSetViewReps(final AdapterWorkoutExercise.ViewHolderWorkoutExercise vHolderExercise, final PlanPosition planPosition, final int setId, boolean additionalSet){

		View viewSet = LayoutInflater.from(workoutActivity).inflate(R.layout.set_reps, vHolderExercise.getVfWorkoutSetFlipper(), false);

		final ImageButton ibSetRepMinus = viewSet.findViewById(R.id.ibSetRepMinus);
		final ImageButton ibSetRepPlus = viewSet.findViewById(R.id.ibSetRepPlus);
		final TextView tvRepCounter = viewSet.findViewById(R.id.tvSetRepAmount);
		final MorphingButton bnWorkoutSetDone = viewSet.findViewById(R.id.bnWorkoutSetDone);

		//Save reference to rep counter textview
		counters.get(planPosition.getPosition()-1).put(setId, tvRepCounter);

		//Print goal
		//If this is an additional set, take the goal of the last 'real' set
		tvRepCounter.setText(String.valueOf(planPosition.getSets().get( (!additionalSet) ? setId : (planPosition.getSetCount()-1) ).getReps()));

		//Set color of 'Next' button
		bnWorkoutSetDone.setBackground(ContextCompat.getDrawable(workoutActivity, R.drawable.button_round_green_lite));
		bnWorkoutSetDone.setBackgroundTintList(Util.INSTANCE.getOneColorStateList(workoutActivity, R.color.green_light));

		//Apply onClick listeners
		ibSetRepMinus.setOnClickListener(new View.OnClickListener(){
			@Override
			public void onClick(View v){
				onClickRepMinus(tvRepCounter, vHolderExercise.getAdapterPosition());
			}
		});
		ibSetRepPlus.setOnClickListener(new View.OnClickListener(){
			@Override
			public void onClick(View v){
				onClickRepPlus(tvRepCounter, vHolderExercise.getAdapterPosition());
			}
		});

		bnWorkoutSetDone.setOnClickListener(new View.OnClickListener(){
			@Override
			public void onClick(View v){
				onClickFinishSet(bnWorkoutSetDone, vHolderExercise, setId, null);
			}
		});

		//Add the fresh view to the viewflipper
		vHolderExercise.getVfWorkoutSetFlipper().addView(viewSet);
	}

	private void onClickRepMinus(TextView tvRepCounter, final int adapterPosition){

		if( touchEventsDisabled || adapterPosition != currentAdapterPosition){
			return;
		}

		//Subtract the visual rep-text by 1
		int newReps = Integer.valueOf(tvRepCounter.getText().toString()) - 1;
		if( newReps >= 0 ){
			tvRepCounter.setText(String.valueOf(newReps));
		}

		//Check if the current set is already marked as done
		//If so: Update the rep log for this exercise and set
		if( setsDone.get(adapterPosition).contains(getCurrentSetId()) ){
			onWorkoutProgress(newReps);
		}
	}

	private void onClickRepPlus(TextView tvRepCounter, final int adapterPosition){

		if( touchEventsDisabled || adapterPosition != currentAdapterPosition){
			return;
		}

		//Add up visual rep-text by 1
		int newReps = Integer.valueOf(tvRepCounter.getText().toString()) + 1;

		if( newReps == 1000 ){
			return;
		}

		tvRepCounter.setText(String.valueOf(newReps));

		//Check if the current set is already marked as done
		//If so: Update the rep log for this exercise and set
		if( setsDone.get(adapterPosition).contains(getCurrentSetId()) ){
			onWorkoutProgress(newReps);
		}
	}

	public void appendSetViewTime(final AdapterWorkoutExercise.ViewHolderWorkoutExercise vHolderExercise, final PlanPosition planPosition, final int setId, boolean additionalSet){

		View viewSet = LayoutInflater.from(workoutActivity).inflate(R.layout.set_time, vHolderExercise.getVfWorkoutSetFlipper(), false);

		final TextView tvGoal = viewSet.findViewById(R.id.tvSetTimeGoal);
		final TextView tvTimer = viewSet.findViewById(R.id.tvSetTimeTimer);
		final ImageButton ibTimerMinus = viewSet.findViewById(R.id.ibSetTimeMinus);
		final ImageButton ibTimerPlus = viewSet.findViewById(R.id.ibSetTimePlus);
		final ImageButton ibTimerReset = viewSet.findViewById(R.id.ibWorkoutSetTimerReset);
		final MorphingButton bnWorkoutSetDone = viewSet.findViewById(R.id.bnWorkoutSetDone);

		//Determine goal
		//If this is an additional set, take the goal of the last 'real' set
		final int goal = planPosition.getSets().get( (!additionalSet) ? setId : (planPosition.getSets().size()-1) ).getReps();

		//Set goal text
		if( goal != 0 ){
			String goalDescr = String.valueOf(goal).concat(workoutActivity.getString(R.string.seconds));
			tvGoal.setText(goalDescr);
		}

		//Set color of 'Next' button
		bnWorkoutSetDone.setBackground(ContextCompat.getDrawable(workoutActivity, R.drawable.button_round_green_lite));
		bnWorkoutSetDone.setBackgroundTintList(Util.INSTANCE.getOneColorStateList(workoutActivity, R.color.green_light));

		final ExerciseTimer timer = timers.get(planPosition.getPosition() - 1).get(setId);

		tvTimer.setOnClickListener(new View.OnClickListener(){
			@Override
			public void onClick(View v){
				onClickTimer(timer, tvTimer, ibTimerMinus, ibTimerPlus, ibTimerReset, vHolderExercise.getAdapterPosition(), setId);
			}
		});

		ibTimerMinus.setOnClickListener(new View.OnClickListener(){
			@Override
			public void onClick(View v){
				onClickTimerMinus(timer, tvTimer, goal, vHolderExercise.getAdapterPosition());
			}
		});

		ibTimerPlus.setOnClickListener(new View.OnClickListener(){
			@Override
			public void onClick(View v){
				onClickTimerPlus(timer, tvTimer, goal, vHolderExercise.getAdapterPosition());
			}
		});

		ibTimerReset.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				onClickTimerReset(timer, ibTimerReset, vHolderExercise.getAdapterPosition());
			}
		});

		bnWorkoutSetDone.setOnClickListener(new View.OnClickListener(){
			@Override
			public void onClick(View v){
				onClickFinishSet(bnWorkoutSetDone, vHolderExercise, setId, timer);
			}
		});

		vHolderExercise.getVfWorkoutSetFlipper().addView(viewSet);
	}

	private void onClickTimer(final ExerciseTimer timer, final TextView tvTimer, final ImageButton ibTimerMinus, final ImageButton ibTimerPlus, final ImageButton ibTimerReset, final int adapterPosition, final int set){

		if( touchEventsDisabled || adapterPosition != currentAdapterPosition){
			return;
		}

		if( !timer.isRunning() ){

			if( !timer.hasListener() ){

				timer.setEventListener(new ExerciseTimer.ExerciseTimerEventListener(){
					@Override
					public void onCountDownStart(){

						tvTimer.setTextColor(Util.INSTANCE.color(workoutActivity, R.color.colorPrimaryLight));
						tvTimer.setTextSize(TypedValue.COMPLEX_UNIT_PX, Util.INSTANCE.dimen(workoutActivity, R.dimen.workout_set_timer_size_big));
					}

					@Override
					public void onTimerStart(){

						tvTimer.setTextColor(Util.INSTANCE.color(workoutActivity, android.R.color.white));
						tvTimer.setTextSize(TypedValue.COMPLEX_UNIT_PX, Util.INSTANCE.dimen(workoutActivity, R.dimen.workout_set_timer_size));
					}

					@Override
					public void onGoalReached(){

						tvTimer.setTextColor(Util.INSTANCE.color(workoutActivity, R.color.green_light));
						tvTimer.setShadowLayer(10, 0, 0, Util.INSTANCE.color(workoutActivity, R.color.green));
					}

					@Override
					public void onPause(){

						animationSlideInLeft.reset();
						animationSlideInRight.reset();
						ibTimerMinus.setVisibility(View.VISIBLE);
						ibTimerPlus.setVisibility(View.VISIBLE);
						ibTimerMinus.startAnimation(animationSlideInLeft);
						ibTimerPlus.startAnimation(animationSlideInRight);
						ibTimerReset.setVisibility(View.VISIBLE);

						if( setsDone.get(adapterPosition).contains(set) ){
							//Update logged seconds if marked as done
							onWorkoutProgress(timer.getElapsedTime());
						}
					}

					@Override
					public void onResume(){

						animationSlideOutLeft.reset();
						animationSlideOutRight.reset();
						ibTimerMinus.setVisibility(View.GONE);
						ibTimerPlus.setVisibility(View.GONE);
						ibTimerMinus.startAnimation(animationSlideOutLeft);
						ibTimerPlus.startAnimation(animationSlideOutRight);
					}

					@Override
					public void onReset(boolean duringCountdown){

						tvTimer.setTextColor(Util.INSTANCE.color(workoutActivity, android.R.color.white));
						tvTimer.setShadowLayer(0, 0, 0, 0);
						tvTimer.setTextSize(TypedValue.COMPLEX_UNIT_PX, Util.INSTANCE.dimen(workoutActivity, R.dimen.workout_set_timer_size));
						tvTimer.setText(R.string.timer_initial);
						ibTimerMinus.setVisibility(View.GONE);
						ibTimerPlus.setVisibility(View.GONE);

						if( !duringCountdown) {
							animationSlideOutLeft.reset();
							animationSlideOutRight.reset();
							ibTimerMinus.startAnimation(animationSlideOutLeft);
							ibTimerPlus.startAnimation(animationSlideOutRight);
						}
					}

					@Override
					public void onTick(@NotNull String time){
						tvTimer.setText(time);
					}
				});
			}

			if( !timer.isPaused() ){
				timer.start();
			}else{
				//Don't resume if the set is marked as done
				//RespUser has to reset the timer in this case
				if( !setsDone.get(adapterPosition).contains(getCurrentSetId()) ){
					timer.resume();
				}
			}

		}else{

			timer.pause();
		}
	}

	private void onClickTimerMinus(ExerciseTimer timer, TextView tvTimer, int goal, final int adapterPosition){

		if( touchEventsDisabled || adapterPosition != currentAdapterPosition){
			return;
		}

		timer.subtractSecond();
		tvTimer.setText(timer.toString());
		if( timer.getElapsedTime() < goal ){
			tvTimer.setTextColor(Util.INSTANCE.color(workoutActivity, android.R.color.white));
			tvTimer.setShadowLayer(0, 0, 0, 0);
		}

		//Check if the current set is already marked as done
		//If so: Update the rep log for this exercise and set
		if( setsDone.get(adapterPosition).contains(getCurrentSetId()) ){
			onWorkoutProgress(timer.getElapsedTime());
		}
	}

	private void onClickTimerPlus(ExerciseTimer timer, TextView tvTimer, int goal, final int adapterPosition){

		if( touchEventsDisabled || adapterPosition != currentAdapterPosition){
			return;
		}

		timer.addSecond();
		tvTimer.setText(timer.toString());
		if( timer.getElapsedTime() >= goal ){
			tvTimer.setTextColor(Util.INSTANCE.color(workoutActivity, R.color.green_light));
			tvTimer.setShadowLayer(10, 0, 0, Util.INSTANCE.color(workoutActivity, R.color.green));
		}

		//Check if the current set is already marked as done
		//If so: Update the rep log for this exercise and set
		if( setsDone.get(adapterPosition).contains(getCurrentSetId()) ){
			onWorkoutProgress(timer.getElapsedTime());
		}
	}

	private void onClickTimerReset(@NotNull ExerciseTimer timer, @NotNull ImageButton ibTimerReset, final int adapterPosition){

		timer.reset();
		ibTimerReset.setVisibility(View.GONE);

		//If the set is already marked as done, also set logged seconds to zero
		if( setsDone.get(adapterPosition).contains(getCurrentSetId()) ){
			onWorkoutProgress(0);
		}

		//Animate
		animationSetTimerReset.reset();
		ibTimerReset.startAnimation(animationSetTimerReset);
	}

	private void onClickFinishSet(MorphingButton bnWorkoutSetDone, @NotNull final AdapterWorkoutExercise.ViewHolderWorkoutExercise vHolderExercise, final int setId, ExerciseTimer timer){

		final int adapterPosition = vHolderExercise.getAdapterPosition();

		if( touchEventsDisabled || adapterPosition != currentAdapterPosition){
			return;
		}

		//Check if set is already marked as done
		if( setsDone.get(adapterPosition).contains(setId) ){
			return;
		}

		//Mark set as done
		setsDone.get(adapterPosition).add(setId);

		//If the set was not an added-set, increment completed set counter
		if( setId < plan.getPosition(adapterPosition).getSetCount() ){
			completedStandardSets++;
			modelActivityWorkout.getCompletedStandardSets().postValue(completedStandardSets);
		}

		if( timer != null ){
			//Stop set timer if running
			timer.pause();
		}

		//Update the finished-sets counter in the workout summary list
		workoutSummaryViewer.updateSetCountFor(adapterPosition, setId+1);

		//Log reps or seconds for this set
		onWorkoutProgress();

		//Enable the next- or the addAll-set button
		if( plan.getId() != C.PLAN_ID_FITNESSTEST) {
			if (vHolderExercise.getVfWorkoutSetFlipper().getChildCount() == (setId + 1)) {
				//Enable the addAll-set button
				vHolderExercise.getIbWorkoutSetNext().getDrawable().setTint(Util.INSTANCE.color(workoutActivity, R.color.grey850));
				vHolderExercise.getIbWorkoutSetAdd().getDrawable().setTint(Util.INSTANCE.color(workoutActivity, android.R.color.white));
			} else {
				//Enable the next-set button
				vHolderExercise.getIbWorkoutSetNext().getDrawable().setTint(Util.INSTANCE.color(workoutActivity, android.R.color.white));
				vHolderExercise.getIbWorkoutSetAdd().getDrawable().setTint(Util.INSTANCE.color(workoutActivity, R.color.grey850));
			}
		}

		//Apply animation-end-listener to morphing button
		morphToCircle.animationListener(new MorphingAnimation.Listener(){
			@Override
			public void onAnimationEnd(){
				//If there are further sets, scroll to next set
				if( vHolderExercise.getVfWorkoutSetFlipper().getChildCount() > (setId+1) ){
					onClickNextSet(vHolderExercise);
				}else if( vHolderExercise.getVfWorkoutSetFlipper().getChildCount() == setId+1 ){

				}
				//Remove self
				morphToCircle.animationListener(null);
			}
		});

		//Animate button
		bnWorkoutSetDone.morph(morphToCircle);
	}

	public void onClickPreviousSet(final AdapterWorkoutExercise.ViewHolderWorkoutExercise vHolderExercise){

		final int adapterPosition = vHolderExercise.getAdapterPosition();

		if( touchEventsDisabled || adapterPosition != currentAdapterPosition){
			return;
		}

		final int currSetId = selectedSetIds.get(adapterPosition);

		if( currSetId > 0 ){

			//Update current selected set id for this plan position
			selectedSetIds.put(adapterPosition, currSetId-1);

			//Stop or reset current timer, if necessary
			if( plan.getExerciseAt(adapterPosition).isTypeTime() ){
				timers.get(adapterPosition).get(currSetId).pause();
			}

			//Set in and out animations
			vHolderExercise.getVfWorkoutSetFlipper().setInAnimation(workoutActivity, R.anim.slide_in_left);
			vHolderExercise.getVfWorkoutSetFlipper().setOutAnimation(workoutActivity, R.anim.slide_out_right);

			//Show previous view
			vHolderExercise.getVfWorkoutSetFlipper().showPrevious();

			//Hide addAll-button
			vHolderExercise.getIbWorkoutSetNext().setVisibility(View.VISIBLE);
			vHolderExercise.getIbWorkoutSetAdd().setVisibility(View.GONE);

			//Check if new set-id is the firstAsSingle set
			if( (currSetId - 1) == 0 ){
				vHolderExercise.getIbWorkoutSetPrevious().getDrawable().setTint(Util.INSTANCE.color(workoutActivity, R.color.grey850));
			}

			//Check if the new set is marked as done
			if( setsDone.get(adapterPosition).contains(currSetId-1) ){
				vHolderExercise.getIbWorkoutSetNext().getDrawable().setTint(Util.INSTANCE.color(workoutActivity, android.R.color.white));
			}else{
				vHolderExercise.getIbWorkoutSetNext().getDrawable().setTint(Util.INSTANCE.color(workoutActivity, R.color.grey850));
			}

			//Update set progress text
			final int setAmount = plan.getSetCountAt(adapterPosition) + additionalSets.get(adapterPosition, 0);
			String progressSet = String.valueOf(currSetId)
				.concat(workoutActivity.getString(R.string.of))
				.concat(String.valueOf(setAmount));
			vHolderExercise.getTvWorkoutProgressSets().setText(progressSet);
		}
	}

	public void onClickNextSet(final AdapterWorkoutExercise.ViewHolderWorkoutExercise vHolderExercise){

		final int adapterPosition = vHolderExercise.getAdapterPosition();

		if( touchEventsDisabled || adapterPosition != currentAdapterPosition){
			return;
		}

		final int currSetId = selectedSetIds.get(adapterPosition);

		//Check if the current set is marked as done
		if( !setsDone.get(adapterPosition).contains(currSetId) ){
			return;
		}

		final int setCount = plan.getSetCountAt(adapterPosition) + additionalSets.get(adapterPosition, 0);

		if( (currSetId + 1) < setCount ){

			//Update current selected set id for this plan position
			selectedSetIds.put(adapterPosition, currSetId + 1);

			//Stop or reset current timer, if necessary
			if( plan.getExerciseAt(adapterPosition).isTypeTime() ){
				timers.get(adapterPosition).get(currSetId).pause();
			}

			//Set in and out animations
			vHolderExercise.getVfWorkoutSetFlipper().setInAnimation(workoutActivity, R.anim.slide_in_right);
			vHolderExercise.getVfWorkoutSetFlipper().setOutAnimation(workoutActivity, R.anim.slide_out_to_left);

			//Show next view
			vHolderExercise.getVfWorkoutSetFlipper().showNext();

			//Check if the new set id is the last set
			if( ( currSetId + 2 ) == setCount ){
				vHolderExercise.getIbWorkoutSetNext().setVisibility(View.GONE);
				vHolderExercise.getIbWorkoutSetAdd().setVisibility(View.VISIBLE);
			}

			if( setsDone.get(adapterPosition).contains(currSetId+1) ){
				vHolderExercise.getIbWorkoutSetNext().getDrawable().setTint(Util.INSTANCE.color(workoutActivity, android.R.color.white));
				vHolderExercise.getIbWorkoutSetAdd().getDrawable().setTint(Util.INSTANCE.color(workoutActivity, android.R.color.white));
			}else{
				vHolderExercise.getIbWorkoutSetNext().getDrawable().setTint(Util.INSTANCE.color(workoutActivity, R.color.grey850));
				vHolderExercise.getIbWorkoutSetAdd().getDrawable().setTint(Util.INSTANCE.color(workoutActivity, R.color.grey850));
			}

			vHolderExercise.getIbWorkoutSetPrevious().getDrawable().setTint(Util.INSTANCE.color(workoutActivity, android.R.color.white));

			//Update set progress text
			String progressSet = String.valueOf(currSetId+2)
				.concat(workoutActivity.getString(R.string.of))
				.concat(String.valueOf(setCount));
			vHolderExercise.getTvWorkoutProgressSets().setText(progressSet);
		}
	}

	public void onClickAddSet(final AdapterWorkoutExercise.ViewHolderWorkoutExercise vHolderExercise){

		final int adapterPosition = vHolderExercise.getAdapterPosition();

		if( touchEventsDisabled || adapterPosition != currentAdapterPosition){
			return;
		}

		final PlanPosition planPosition = plan.getPosition(adapterPosition);
		int lastPlannedReps = planPosition.getSets().get( planPosition.getSetCount()-1 ).getReps();
		int newSetId = vHolderExercise.getVfWorkoutSetFlipper().getChildCount();

		//Check if current set is marked as done
		if( !setsDone.get(adapterPosition).contains(newSetId-1) ){
			return;
		}

		if( planPosition.getExercise().isTypeReps() ){

			//Append the view to the viewflipper
			appendSetViewReps(vHolderExercise, planPosition, newSetId, true);

		}else if( planPosition.getExercise().isTypeTime() ){

			//Add new timer to the timer array
			timers.get(adapterPosition).put(newSetId, new ExerciseTimer(lastPlannedReps));

			//Append the view to the viewflipper
			appendSetViewTime(vHolderExercise,planPosition, newSetId, true);
		}

		//Update additional set counter for this plan position
		int newAdditionalSetCount = additionalSets.get(adapterPosition, 0) + 1;
		additionalSets.put(adapterPosition, newAdditionalSetCount);

		onClickNextSet(vHolderExercise);

		//Inform activity about new set
		workoutLogger.informAboutAdditionalSet(adapterPosition, newSetId);
	}

	public void onClickExerciseInfo(Exercise exercise, ViewGroup root){

		PopupExerciseInfo popupExerciseInfo = new PopupExerciseInfo(workoutActivity, root);
		popupExerciseInfo.show(exercise);
    }

	public void registerVideoPlayer(int position, ExerciseVideoPlayer videoPlayer){
		videoPlayers.put(position, videoPlayer);
	}

	public void onWorkoutPaused(){

		if( currentAdapterPosition >= plan.positionCount() ){
			//Not on a exercise card right now
			return;
		}

		//Pause current exercise timer if necessary
		if( plan.getExerciseAt(currentAdapterPosition).isTypeTime() ){
			timers.get(currentAdapterPosition).get(getCurrentSetId()).pause();
		}

		//Pause allEquipments running video players
		for( int i = 0; i < videoPlayers.size(); i++ ){
			videoPlayers.get(videoPlayers.keyAt(i)).pause();
		}

		//Disable scrolling and allEquipments other touch events
		layoutManagerExercises.setScrollable(false);
		touchEventsDisabled = true;
	}

	public void onWorkoutResumed(){

		//Enable scrolling and allEquipments other touch events
		layoutManagerExercises.setScrollable(true);
		touchEventsDisabled = false;
	}

	//Logs the rep count of the current selected plan position and set
	//Called by the 'WEITER' button page every set view
	private void onWorkoutProgress(){

		if( currentAdapterPosition >= plan.positionCount() ){
			//Not on a exercise card right now
			return;
		}

		int reps = 0;

		if( plan.getExerciseAt(currentAdapterPosition).isTypeTime() ){
			reps = timers.get(currentAdapterPosition).get(getCurrentSetId()).getElapsedTime();
		}else if( plan.getExerciseAt(currentAdapterPosition).isTypeReps() ){
			reps = Integer.valueOf(counters.get(currentAdapterPosition).get(getCurrentSetId()).getText().toString());
		}

		//Save new rep count to logger
		workoutLogger.logReps(currentAdapterPosition, getCurrentSetId(), reps);

		//Update workout summary data
		workoutSummaryViewer.updateRepSumFor(currentAdapterPosition, workoutLogger.getRepSum(currentAdapterPosition));

		workoutSummaryViewer.updateView();
	}

	//Logs the given rep count for the current selected plan position and set
	private void onWorkoutProgress(int newReps){

		if( currentAdapterPosition >= plan.positionCount() ){
			//Not on a exercise card right now
			return;
		}

		workoutLogger.logReps(currentAdapterPosition, getCurrentSetId(), newReps);

		//Update workout summary data
		workoutSummaryViewer.updateRepSumFor(currentAdapterPosition, workoutLogger.getRepSum(currentAdapterPosition));

		workoutSummaryViewer.updateView();
	}

	public void onBeforeWorkoutFinish(){

		//Release allEquipments video players
		for( int i = 0; i < videoPlayers.size(); i++ ){
			videoPlayers.get(videoPlayers.keyAt(i)).release();
		}
	}

	private int getCurrentSetId(){
		return selectedSetIds.get(currentAdapterPosition);
	}

	public WorkoutLogger getWorkoutLogger(){
		return workoutLogger;
	}
}
