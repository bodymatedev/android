package de.bodymate.bodymateapp.gui.popup

import android.content.Context
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.PopupWindow
import android.widget.ProgressBar
import android.widget.TextView
import de.bodymate.bodymateapp.R
import de.bodymate.bodymateapp.util.Util
import de.bodymate.bodymateapp.util.Util.text

/**
 * Created by Leonard on 02.06.2018.
 */
class PopupLoadingCredentials(private val mContext: Context, private val parentViewGroup: ViewGroup) {

    companion object {

        const val METHOD_MAIL = 1
        const val METHOD_GOOGLE = 2
        const val METHOD_FACEBOOK = 3
        const val USE_LOGIN = 1
        const val USE_REGISTER = 2
    }

    private var popup : PopupWindow? = null
    private lateinit var popupView: View

    fun show(method: Int, use: Int) {

        //Inflate content view
        popupView = Util.inflateView(mContext, R.layout.popup_sign_in_up_loading)

        //Create popup
        popup = PopupWindow(popupView, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, false)

        //Customize look
        val vSignInUpPopupDecoration : View = popupView.findViewById(R.id.vSignInUpPopupDecoration)
        val ivSignInUpPopupDecoration : ImageView = popupView.findViewById(R.id.ivSignInUpPopupDecoration)
        val pbSignInUpPopupLoading : ProgressBar = popupView.findViewById(R.id.pbSignInUpPopupLoading)
        val tvSignInUpPopupHeader : TextView = popupView.findViewById(R.id.tvSignInUpPopupHeader)
        val tvSignInUpPopupText : TextView = popupView.findViewById(R.id.tvSignInUpPopupText)

        when (method){

            METHOD_MAIL -> {
                vSignInUpPopupDecoration.setBackgroundResource(R.color.colorPrimary)
                ivSignInUpPopupDecoration.setImageResource(R.drawable.ic_mail)
                pbSignInUpPopupLoading.indeterminateTintList = Util.getOneColorStateList(mContext, R.color.colorPrimary)

                when (use){
                    USE_LOGIN -> {
                        tvSignInUpPopupHeader.text = text(mContext, R.string.email_login)
                        tvSignInUpPopupText.text = text(mContext, R.string.email_login_description)
                    }
                    USE_REGISTER -> {
                        tvSignInUpPopupHeader.text = text(mContext, R.string.email_registration)
                        tvSignInUpPopupText.text = text(mContext, R.string.email_registration_description)
                    }
                }
            }
            METHOD_GOOGLE -> {

                vSignInUpPopupDecoration.setBackgroundResource(android.R.color.white)
                ivSignInUpPopupDecoration.setImageResource(R.drawable.ic_google)
                pbSignInUpPopupLoading.indeterminateTintList = Util.getOneColorStateList(mContext, R.color.colorPrimary)

                when (use){
                    USE_LOGIN -> {
                        tvSignInUpPopupHeader.text = text(mContext, R.string.google_login)
                        tvSignInUpPopupText.text = text(mContext, R.string.google_login_description)
                    }
                    USE_REGISTER -> {
                        tvSignInUpPopupHeader.text = text(mContext, R.string.google_registration)
                        tvSignInUpPopupText.text = text(mContext, R.string.google_registration_description)
                    }
                }
            }
            METHOD_FACEBOOK -> {

                vSignInUpPopupDecoration.setBackgroundResource(R.color.facebook_blue)
                ivSignInUpPopupDecoration.setImageResource(R.drawable.ic_facebook)
                pbSignInUpPopupLoading.indeterminateTintList = Util.getOneColorStateList(mContext, R.color.facebook_blue_bright)

                when (use){
                    USE_LOGIN -> {
                        tvSignInUpPopupHeader.text =  text(mContext, R.string.facebook_login)
                        tvSignInUpPopupText.text = text(mContext, R.string.facebook_login_description)
                    }
                    USE_REGISTER -> {
                        tvSignInUpPopupHeader.text = text(mContext, R.string.facebook_registration)
                        tvSignInUpPopupText.text = text(mContext, R.string.facebook_registration_description)
                    }
                }
            }
        }

        //Show popup
        popup?.showAtLocation(parentViewGroup.rootView, Gravity.CENTER, 0, 0)
    }

    fun close(){
        popup?.dismiss()
    }
}

