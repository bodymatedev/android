package de.bodymate.bodymateapp.gui

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.util.SparseArray

/**
 * Created by leonard on 24.03.2017.
 *
 */

class MyFragmentPagerAdapter(fragmentManager: FragmentManager?, vararg fragments: Fragment) : FragmentPagerAdapter(fragmentManager) {

    private val fragments = SparseArray<Fragment>()

    private var indexCounter = 0

    val maxOffscreenPageLimit: Int
        get() = if (count > 0) count - 1 else 0

    init {
        for (fragment in fragments) {
            addFragment(fragment)
        }
    }

    override fun getItem(index: Int): Fragment {
        return fragments.get(index)
    }

    override fun getCount(): Int {
        return fragments.size()
    }

    fun addFragment(newFragment: Fragment) {
        fragments.put(indexCounter, newFragment)
        indexCounter++
    }
}
