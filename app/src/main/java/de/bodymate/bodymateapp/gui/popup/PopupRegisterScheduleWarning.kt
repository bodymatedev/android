package de.bodymate.bodymateapp.gui.popup

import android.view.ViewGroup
import android.widget.Button
import de.bodymate.bodymateapp.R
import de.bodymate.bodymateapp.activity.RegisterDetailActivity
import de.bodymate.bodymateapp.util.Util

/**
 * Created by Leonard on 22.04.2018.
 */
object PopupRegisterScheduleWarning {

    fun show(registerDetailActivity: RegisterDetailActivity, root: ViewGroup) {

        val popup = Util.showDimPopup(registerDetailActivity, root, R.layout.schedule_warning)

        val bnScheduleWarningSetWeekdays = popup.contentView.findViewById<Button>(R.id.bnScheduleWarningSetWeekdays)

        bnScheduleWarningSetWeekdays.setOnClickListener {
            popup.dismiss()
            bnScheduleWarningSetWeekdays.setOnClickListener(null)
        }
    }
}
