package de.bodymate.bodymateapp.gui.popup

import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.ListView
import android.widget.PopupWindow
import android.widget.TextView
import de.bodymate.bodymateapp.R
import de.bodymate.bodymateapp.gui.adapter.AdapterExerciseDetail
import de.bodymate.bodymateapp.data.internentity.Plan
import de.bodymate.bodymateapp.data.repository.entity.RoomExercise
import de.bodymate.bodymateapp.util.Util

/**
 * Created by Leonard on 10.03.2018.
 */

class PopupPlanDetail(private val mContext: Context, private val root: ViewGroup) {

    private lateinit var tvPlanDetailName: TextView
    private lateinit var lvPlanDetailExercise: ListView
    private lateinit var tvPlanDetailStartWorkout: TextView
    private lateinit var bnPlanDetailActionHeader: ImageButton

    private lateinit var popupWindow: PopupWindow

    fun show(plan: Plan, exercises: List<RoomExercise>, beginListener: View.OnClickListener?, buttonText: String?) {

        popupWindow = Util.showBlurPopup(mContext, root, R.layout.plan_detail_popup_startable, .85f, .7f)

        tvPlanDetailName = popupWindow.contentView.findViewById(R.id.tvPlanDetailName)
        lvPlanDetailExercise = popupWindow.contentView.findViewById(R.id.lvPlanDetailExercise)
        tvPlanDetailStartWorkout = popupWindow.contentView.findViewById(R.id.tvPlanDetailStartWorkout)
        bnPlanDetailActionHeader = popupWindow.contentView.findViewById(R.id.bnPlanDetailActionHeader)

        // Display plan name
        tvPlanDetailName.text = plan.getDisplayName(mContext)

        // List allExercises
        lvPlanDetailExercise.adapter = AdapterExerciseDetail.create(exercises, plan.positions, mContext)

        if (beginListener != null) {
            tvPlanDetailStartWorkout.setOnClickListener(beginListener)
            tvPlanDetailStartWorkout.tag = plan.id
            tvPlanDetailStartWorkout.text = buttonText
        } else {
            tvPlanDetailStartWorkout.visibility = View.GONE
        }

        bnPlanDetailActionHeader.setOnClickListener {
            popupWindow.dismiss()
            tvPlanDetailStartWorkout.setOnClickListener(null)
        }
    }

    fun close(){

        popupWindow.dismiss()
        tvPlanDetailStartWorkout.setOnClickListener(null)
    }
}