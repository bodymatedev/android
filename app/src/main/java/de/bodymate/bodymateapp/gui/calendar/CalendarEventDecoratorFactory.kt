package de.bodymate.bodymateapp.gui.calendar

import android.support.annotation.ColorRes

import de.bodymate.bodymateapp.data.repository.entity.RoomSchedule
import de.bodymate.bodymateapp.util.DateUtils

import com.prolificinteractive.materialcalendarview.CalendarDay
import com.prolificinteractive.materialcalendarview.DayViewDecorator
import com.prolificinteractive.materialcalendarview.DayViewFacade


import org.jetbrains.annotations.Contract

import java.util.Calendar
import java.util.HashSet
import java.util.Locale

/**
 * Created by leonard on 12.06.2017.
 *
 */

object CalendarEventDecoratorFactory {

    @Contract("null, _ -> null")
    fun getDecorators(schedules: List<RoomSchedule>?, @ColorRes color: Int): CalendarEventDecoratorX? {

        if (schedules == null) {
            return null
        }

        val datesOne = HashSet<Long>()

        //Today
        val calStart = Calendar.getInstance(Locale.GERMAN)
        calStart.time = CalendarDay.today().date

        //Sunday in 3 weeks
        val calEnd = Calendar.getInstance(Locale.GERMAN)
        calEnd.time = DateUtils.getDayByWeekdayPlusWeekOffset(Calendar.SUNDAY, 3)!!.date

        //Get the weekdays page schedules
        val weekdays = HashSet<Int>()
        for ((weekday) in schedules) {
            weekdays.add(DateUtils.weekdayToJavaWeekday(weekday))
        }

        while (calStart.before(calEnd)) {
            if (weekdays.contains(calStart.get(Calendar.DAY_OF_WEEK))) {
                datesOne.add(calStart.time.time)
            }
            calStart.add(Calendar.DATE, 1)
        }

        return CalendarEventDecoratorX(datesOne, MyDotSpan.ONE, color)
    }

    class CalendarEventDecoratorX(private val dates: HashSet<Long>, private val dotCount: Int, @param:ColorRes @field:ColorRes

    private val color: Int) : DayViewDecorator {

        override fun shouldDecorate(date: CalendarDay): Boolean {
            return dates.contains(date.date.time)
        }

        override fun decorate(view: DayViewFacade) {
            view.addSpan(MyDotSpan(color, dotCount))
        }
    }
}
