package de.bodymate.bodymateapp.gui.popup

import android.content.Context
import android.support.design.widget.TextInputLayout
import android.util.Patterns
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.google.firebase.FirebaseNetworkException
import com.google.firebase.auth.FirebaseAuth
import de.bodymate.bodymateapp.R
import de.bodymate.bodymateapp.gui.MyTextWatcher
import de.bodymate.bodymateapp.util.Util

/**
 * Created by Leonard on 04.06.2018.
 */

class PopupPasswordReset(private val mContext : Context, private val parentViewGroup: ViewGroup){

    private lateinit var popup: PopupWindow

    fun show(){

        //Inflate content view
        val popupView = Util.inflateView(mContext, R.layout.popup_forgot_password)

        //Create popup
        popup = PopupWindow(popupView, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, true)

        val bnPopupPwResetSendMail = popupView.findViewById<Button>(R.id.bnPopupPwResetSendMail)

        //Get progress bar
        val pbPopupPwResetLoading = popupView.findViewById<ProgressBar>(R.id.pbPopupPwResetLoading)

        //Apply onTextChangeListener for email field
        val etPopupPwResetEmail = popupView.findViewById<TextView>(R.id.etPopupPwResetEmail)
        val tilPopupPwResetEmail = popupView.findViewById<TextInputLayout>(R.id.tilPopupPwResetEmail)


        val textChangeListenerEmail = MyTextWatcher( object : MyTextWatcher.OnTextChanged {
            override fun onTextChanged(newText: String) {

                tilPopupPwResetEmail.error = null

                if( Patterns.EMAIL_ADDRESS.matcher(newText).matches() ){
                    bnPopupPwResetSendMail.backgroundTintList = Util.getOneColorStateList(mContext, R.color.colorPrimary)
                }else{
                    bnPopupPwResetSendMail.backgroundTintList = Util.getOneColorStateList(mContext, R.color.colorPrimaryLighter)
                }
            }
        })
        etPopupPwResetEmail.addTextChangedListener(textChangeListenerEmail)

        //Apply onClickListener to send mail button
        bnPopupPwResetSendMail.setOnClickListener{

            if( etPopupPwResetEmail.text != null && Patterns.EMAIL_ADDRESS.matcher(etPopupPwResetEmail.text).matches()){

                pbPopupPwResetLoading.visibility = View.VISIBLE
                bnPopupPwResetSendMail.isEnabled = false
                etPopupPwResetEmail.isEnabled = false

                FirebaseAuth.getInstance().sendPasswordResetEmail(etPopupPwResetEmail.text.toString())
                        .addOnCompleteListener { sendMailTask ->

                            if( sendMailTask.isSuccessful ){

                                //Sent mail successfully

                                Util.toastIt(mContext, mContext.getString(R.string.password_reset_mail_sent, etPopupPwResetEmail.text))

                                //Close popup now
                                popup.dismiss()

                            }else{

                                //Sending mail failed

                                if( sendMailTask.exception is FirebaseNetworkException ){
                                    Util.toastIt(mContext, R.string.network_error)
                                }else{
                                    Util.toastIt(mContext, mContext.getString(R.string.password_reset_no_mail_sent, etPopupPwResetEmail.text))
                                }

                                pbPopupPwResetLoading.visibility = View.INVISIBLE
                                bnPopupPwResetSendMail.isEnabled = true
                                etPopupPwResetEmail.isEnabled = true
                            }
                }

            }else{

                //No valid email address input
                tilPopupPwResetEmail.error = Util.text(mContext, R.string.error_email_wrong_pattern)
            }
        }

        //Set listener to background -> To dismiss the popup
        val flPopupPwResetBackground = popupView.findViewById<FrameLayout>(R.id.flPopupPwResetBackground)
        flPopupPwResetBackground.setOnClickListener{
            popup.dismiss()
        }

        //Cleanup in case of dismiss
        popup.setOnDismissListener {

            bnPopupPwResetSendMail.setOnClickListener(null)
            flPopupPwResetBackground.setOnClickListener(null)
            etPopupPwResetEmail.removeTextChangedListener(textChangeListenerEmail)
            popup.setOnDismissListener(null)
        }

        //Show popup
        popup.showAtLocation(parentViewGroup.rootView, Gravity.NO_GRAVITY, 0, 0)
    }
}