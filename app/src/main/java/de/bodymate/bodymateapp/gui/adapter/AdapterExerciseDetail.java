package de.bodymate.bodymateapp.gui.adapter;


import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import de.bodymate.bodymateapp.R;
import de.bodymate.bodymateapp.util.C;
import de.bodymate.bodymateapp.data.internentity.PlanPosition;
import de.bodymate.bodymateapp.data.repository.entity.RoomExercise;
import de.bodymate.bodymateapp.util.Util;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by leonard on 27.05.2017.
 *
 */

public class AdapterExerciseDetail extends ArrayAdapter<AdapterExerciseDetail.ExerciseDetail>{

	@LayoutRes
	private final static int LAYOUT_ID = R.layout.exercise_detail;

	private List<ExerciseDetail> exerciseDetails;
	private Context mContext;

	@NonNull
	public static AdapterExerciseDetail create(List<RoomExercise> exercises, List<PlanPosition> planPositions, Context context){

		return new AdapterExerciseDetail(ExerciseDetail.listFrom(exercises, planPositions), context);
	}

	private AdapterExerciseDetail(List<ExerciseDetail> exerciseDetails, Context context){

		super(context, LAYOUT_ID, exerciseDetails);
		this.exerciseDetails = exerciseDetails;
		this.mContext = context;
	}

	@NonNull
	@Override
	public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent){

		ViewHolderExerciseDetail guiHolder;
		ExerciseDetail exerciseDetail = exerciseDetails.get(position);

		if( convertView == null ){

			LayoutInflater inflater = Util.INSTANCE.getSystemLayoutInflater(mContext);
			convertView = inflater.inflate(LAYOUT_ID, parent, false);

			guiHolder = new ViewHolderExerciseDetail();
			guiHolder.tvExerciseDetailName = convertView.findViewById(R.id.tvExerciseDetailName);
			guiHolder.tvExerciseDetailSets = convertView.findViewById(R.id.tvExerciseDetailSets);
			guiHolder.ivExerciseDetailType = convertView.findViewById(R.id.ivExerciseDetailType);

			convertView.setTag(guiHolder);

		}else{

			guiHolder = (ViewHolderExerciseDetail) convertView.getTag();
		}


		//TODO String resource with placeholder
		String sets = String.valueOf(exerciseDetail.getSets()) + " x";
		guiHolder.tvExerciseDetailName.setText(exerciseDetail.getName());
		guiHolder.tvExerciseDetailName.setSelected(true);
		guiHolder.tvExerciseDetailSets.setText(sets);

		switch( exerciseDetail.getType() ){
			case C.TYPE_REPS:
				guiHolder.ivExerciseDetailType.setImageResource(R.drawable.ic_repeating);
				break;
			case C.TYPE_TIME:
				guiHolder.ivExerciseDetailType.setImageResource(R.drawable.ic_time);
				break;
			default:
				guiHolder.ivExerciseDetailType.setImageResource(0);
				break;
		}

		return convertView;
	}

	/*
	 *	Viewholder
	 */
	public static class ViewHolderExerciseDetail {

		TextView tvExerciseDetailName;
		TextView tvExerciseDetailSets;
		ImageView ivExerciseDetailType;
	}

	/*
	 *  Datamodel
	 */
	@SuppressWarnings("WeakerAccess")
	@AllArgsConstructor
	@Getter
	public static class ExerciseDetail{

		private String name;
		private String type;
		private int sets;

		@Contract("null, _ -> !null; !null, null -> !null")
		public static List<ExerciseDetail> listFrom(List<RoomExercise> exercises, List<PlanPosition> planPositions){

			if( exercises == null || planPositions == null || exercises.isEmpty() || planPositions.isEmpty() ){
				return new ArrayList<>();
			}

			final ArrayList<ExerciseDetail> exerciseDetails = new ArrayList<>();

			for( PlanPosition planPosition : planPositions ){
				for( RoomExercise exercise : exercises ){
					if( planPosition.getExerciseId() == exercise.getId() ){
						exerciseDetails.add(ExerciseDetail.create(planPosition, exercise));
					}
				}
			}

			return exerciseDetails;
		}

		@NonNull
		private static ExerciseDetail create(@NotNull PlanPosition planPosition, @NotNull RoomExercise exercise){
			planPosition.getSets();
			return new ExerciseDetail(exercise.getName(), exercise.getType(), planPosition.getSets().size());
		}
	}
}
