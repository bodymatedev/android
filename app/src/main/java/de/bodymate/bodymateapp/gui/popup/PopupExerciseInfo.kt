package de.bodymate.bodymateapp.gui.popup

import android.support.constraint.ConstraintLayout
import android.support.v7.widget.CardView
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.FrameLayout
import android.widget.PopupWindow
import android.widget.TextView
import de.bodymate.bodymateapp.R
import de.bodymate.bodymateapp.activity.WorkoutActivity
import de.bodymate.bodymateapp.data.internentity.Exercise
import de.bodymate.bodymateapp.util.Util

class PopupExerciseInfo(private val activityWorkout: WorkoutActivity, private val root: ViewGroup) {

    private lateinit var popup: PopupWindow
    private lateinit var flPopupExerciseInfoOk: FrameLayout
    private lateinit var clPopupExerciseDetail: ConstraintLayout
    private lateinit var cwPopupExerciseDetail: CardView

    fun show(exercise: Exercise) {

        //Inflate content view
        val popupView = Util.inflateView(activityWorkout, R.layout.popup_exercise_info)

        //Create popup
        popup = PopupWindow(popupView, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, true)
        popup.animationStyle = R.style.popupAnimationStyle

        flPopupExerciseInfoOk = popupView.findViewById(R.id.flPopupExerciseInfoOk)
        val tvExerciseInfoName = popupView.findViewById<TextView>(R.id.tvExerciseInfoName)
        val tvExerciseInfoDescription = popupView.findViewById<TextView>(R.id.tvExerciseInfoDescription)
        clPopupExerciseDetail = popupView.findViewById(R.id.clPopupExerciseDetail)
        cwPopupExerciseDetail = popupView.findViewById(R.id.cwPopupExerciseDetail)

        flPopupExerciseInfoOk.setOnClickListener { close() }
        clPopupExerciseDetail.setOnClickListener { close() }
        cwPopupExerciseDetail.setOnClickListener {  }

        tvExerciseInfoName.text = exercise.name
        tvExerciseInfoDescription.text = exercise.description

        val popInAnim = AnimationUtils.loadAnimation(activityWorkout, R.anim.grow_in)

        popInAnim.setAnimationListener(object : Animation.AnimationListener {
            override fun onAnimationRepeat(animation: Animation?) {}

            override fun onAnimationEnd(animation: Animation?) {
                flPopupExerciseInfoOk.visibility = View.VISIBLE
            }

            override fun onAnimationStart(animation: Animation?) {
                flPopupExerciseInfoOk.visibility = View.VISIBLE
            }
        })

        popup.showAtLocation(root, Gravity.NO_GRAVITY, 0, 0)

        flPopupExerciseInfoOk.startAnimation(popInAnim)
    }

    fun close() {

        flPopupExerciseInfoOk.setOnClickListener(null)
        clPopupExerciseDetail.setOnClickListener(null)
        cwPopupExerciseDetail.setOnClickListener(null)

        popup.dismiss()
    }
}