package de.bodymate.bodymateapp.gui.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import de.bodymate.bodymateapp.R;
import de.bodymate.bodymateapp.gui.adapter.datamodel.WorkoutSummary;
import de.bodymate.bodymateapp.util.C;
import de.bodymate.bodymateapp.util.Util;

/**
 * Created by Leonard on 21.01.2018.
 *
 */

public class AdapterWorkoutSummary extends ArrayAdapter<WorkoutSummary> {

    private final static int RESOURCE_ID = R.layout.workout_summary;

    private ArrayList<WorkoutSummary> workoutSummaries;
    private Context context;
    private final StringBuilder strBuilder = new StringBuilder(8);

    public AdapterWorkoutSummary(ArrayList<WorkoutSummary> workoutSummaries, Context context) {

        super(context, RESOURCE_ID, workoutSummaries);
        this.workoutSummaries = workoutSummaries;
        this.context = context;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        WorkoutSummary workoutSummary = workoutSummaries.get(position);
        LayoutInflater inflater = Util.INSTANCE.getSystemLayoutInflater(context);
        GuiHolderWorkoutSummary guiHolder;

        if( convertView == null ) {
            convertView = inflater.inflate(RESOURCE_ID, parent, false);
            guiHolder = new GuiHolderWorkoutSummary(convertView);
            convertView.setTag(guiHolder);
        }else{
            guiHolder = (GuiHolderWorkoutSummary) convertView.getTag();
        }

        guiHolder.tvWorkoutSumExerciseName.setText(workoutSummary.getExerciseName());
        guiHolder.tvWorkoutSumSets.setText("x".concat(String.valueOf(workoutSummary.getSetCount())));

        guiHolder.tvWorkoutSumExerciseName.setSelected(true);

        String repsSum = "0";
        if( workoutSummary.getType().equals(C.TYPE_REPS) ){
            repsSum = String.valueOf(workoutSummary.getRepSum());
        }else if( workoutSummary.getType().equals(C.TYPE_TIME) ){
            repsSum = DateUtils.formatElapsedTime(strBuilder, workoutSummary.getRepSum());
        }
        guiHolder.tvWorkoutSumReps.setText(repsSum);

        if( workoutSummary.getSetsGoal() <= workoutSummary.getSetCount()
				&& workoutSummary.getRepSum() >= workoutSummary.getRepsTotalGoal() ){
            guiHolder.tvWorkoutSumExerciseName.setTextColor(Util.INSTANCE.color(context, R.color.green_light));
            guiHolder.tvWorkoutSumSets.setTextColor(Util.INSTANCE.color(context, R.color.green_light));
            guiHolder.tvWorkoutSumReps.setTextColor(Util.INSTANCE.color(context, R.color.green_light));
        }else{
            guiHolder.tvWorkoutSumExerciseName.setTextColor(Util.INSTANCE.color(context, android.R.color.white));
			guiHolder.tvWorkoutSumSets.setTextColor(Util.INSTANCE.color(context, android.R.color.white));
            guiHolder.tvWorkoutSumReps.setTextColor(Util.INSTANCE.color(context, android.R.color.white));
        }

        return convertView;
    }

    static final class GuiHolderWorkoutSummary{

        TextView tvWorkoutSumExerciseName;
        TextView tvWorkoutSumSets;
        TextView tvWorkoutSumReps;

		GuiHolderWorkoutSummary(View root){
            tvWorkoutSumExerciseName = root.findViewById(R.id.tvWorkoutSumExerciseName);
            tvWorkoutSumSets = root.findViewById(R.id.tvWorkoutSumSets);
            tvWorkoutSumReps = root.findViewById(R.id.tvWorkoutSumReps);
        }
    }
}
