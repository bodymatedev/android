package de.bodymate.bodymateapp.gui

import android.text.Editable
import android.text.TextWatcher

/**
 * Created by Leonard on 08.04.2018.
 */
class MyTextWatcher(private val onTextChanged: OnTextChanged) : TextWatcher {

    override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
        onTextChanged.onTextChanged(s.toString())
    }

    override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

    override fun afterTextChanged(s: Editable) {}

    interface OnTextChanged {
        fun onTextChanged(newText: String)
    }
}
