package de.bodymate.bodymateapp.gui;

import android.content.Context;
import android.net.Uri;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.util.SparseArray;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.ui.SimpleExoPlayerView;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;

import de.bodymate.bodymateapp.R;

import static com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade;

/**
 * Created by Leonard on 09.04.2018.
 */
public class OnlineVideoPlayer{

	private Context mContext;
	private RequestOptions glideReqOpt;

	private String videoUrl;
	private int thumbnail;

	private SimpleExoPlayer player;

	private SimpleExoPlayerView exoPlayerView;
	private ProgressBar pbLoad;
	private RelativeLayout rlControllerOverlay;

	@NonNull
	public static OnlineVideoPlayer create(@NonNull Context mContext, @NonNull String videoUrl, @DrawableRes int thumbnail){

		return new OnlineVideoPlayer(mContext, videoUrl, thumbnail);
	}

	private OnlineVideoPlayer(@NonNull Context mContext, @NonNull String videoUrl, @DrawableRes int thumbnail){

		this.mContext = mContext;
		this.videoUrl = videoUrl;
		this.thumbnail = thumbnail;

		glideReqOpt = new RequestOptions().placeholder(thumbnail).error(thumbnail);
	}

	public void load(final SimpleExoPlayerView exoPlayerView,
					 final ImageView ivThumbnail,
					 final ProgressBar pbLoad,
					 final RelativeLayout rlControllerOverlay,
					 final ImageButton ibPlayDummy){

		this.exoPlayerView = exoPlayerView;
		this.pbLoad = pbLoad;
		this.rlControllerOverlay = rlControllerOverlay;

		ibPlayDummy.setImageResource(R.drawable.ic_play_video);

		//Hide exoplayer view
		exoPlayerView.setVisibility(View.INVISIBLE);

		//Load thumbnail
		Glide.with(mContext)
				.load(thumbnail)
				.apply(glideReqOpt)
				.transition(withCrossFade())
				.into(ivThumbnail);

		//Display dummy play button
		rlControllerOverlay.setVisibility(View.VISIBLE);

		//Set action listener to dummy play button
		ibPlayDummy.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				if( player == null ) {
					ivThumbnail.setVisibility(View.GONE);
					exoPlayerView.setVisibility(View.VISIBLE);
					loadExternalVideo();
				}else{
					player.setPlayWhenReady(true);
					exoPlayerView.setUseController(true);
					exoPlayerView.setControllerShowTimeoutMs(3000);
				}

				rlControllerOverlay.setVisibility(View.GONE);
			}
		});
	}

	private void loadExternalVideo(){

		if( videoUrl == null ){
			return;
		}

		//Set up and load video resource
		DefaultBandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();

		TrackSelection.Factory videoTrackSelectionFactory = new AdaptiveTrackSelection.Factory(bandwidthMeter);

		DefaultDataSourceFactory dataSourceFactory = new DefaultDataSourceFactory(mContext,
				Util.getUserAgent(mContext, de.bodymate.bodymateapp.util.Util.INSTANCE.getApplicationName(mContext))
		);
		MediaSource videoSource = new ExtractorMediaSource.Factory(dataSourceFactory)
				.createMediaSource(Uri.parse(videoUrl));

		player = ExoPlayerFactory.newSimpleInstance(mContext, new DefaultTrackSelector(videoTrackSelectionFactory));

		exoPlayerView.setPlayer(player);
		exoPlayerView.setUseController(true);
		exoPlayerView.setControllerShowTimeoutMs(3000);
		player.setPlayWhenReady(true);
		player.prepare(videoSource);

		player.addListener(new Player.DefaultEventListener() {
			@Override
			public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {

				if( playbackState == Player.STATE_ENDED ) {

					player.setPlayWhenReady(false);
					player.seekTo(0);
					rlControllerOverlay.setVisibility(View.VISIBLE);
					exoPlayerView.setUseController(false);
				}
				pbLoad.setVisibility((playbackState == Player.STATE_BUFFERING) ? View.VISIBLE : View.INVISIBLE );
			}
		});
	}

	public void destroy(){

		if( player != null ){
			player.stop();
			player.release();
			player = null;
		}
	}

	public void pause(){
		if( player != null ){
			player.setPlayWhenReady(false);
		}
	}
}
