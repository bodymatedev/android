package de.bodymate.bodymateapp.gui.adapter

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import br.com.simplepass.loading_button_lib.customViews.CircularProgressButton
import com.google.android.exoplayer2.ui.SimpleExoPlayerView
import de.bodymate.bodymateapp.R
import de.bodymate.bodymateapp.data.internentity.Exercise
import de.bodymate.bodymateapp.data.internentity.Plan
import de.bodymate.bodymateapp.gui.ExerciseVideoPlayer
import de.bodymate.bodymateapp.gui.WorkoutExerciseViewer
import de.bodymate.bodymateapp.util.C.PLAN_ID_FITNESSTEST
import de.bodymate.bodymateapp.util.Util
import de.bodymate.bodymateapp.viewmodel.ViewModelActivityWorkout

/**
 * Created by Leonard on 07.12.2017.
 *
 */

class AdapterWorkoutExercise(private val workoutExerciseViewer: WorkoutExerciseViewer) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val plan: Plan = workoutExerciseViewer.plan
    private var cardWidth: Int = 0
    private var cardHeight: Int = 0
    private var dimensCalculated = false
    private val viewHolderInitialized: MutableSet<Int> = HashSet()
    private val mContext: Context = workoutExerciseViewer.activity

    //var completedStandardSets: Int = 0

    private val onClickWorkoutSave = View.OnClickListener {
        workoutExerciseViewer.activity.finishWorkout(it as CircularProgressButton)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

        if (!dimensCalculated) {
            dimensCalculated = true
            cardWidth = (parent.width * WorkoutExerciseViewer.CARD_SIZE_FACTOR_X).toInt()
            cardHeight = (parent.height * WorkoutExerciseViewer.CARD_SIZE_FACTOR_Y).toInt()
        }

        val cardView = LayoutInflater.from(parent.context)
                .inflate(if (viewType == VIEW_TYPE_EXERCISE) R.layout.card_exercise else R.layout.card_training_end, parent, false)

        cardView.layoutParams.width = cardWidth
        cardView.layoutParams.height = cardHeight

        return if (viewType == VIEW_TYPE_EXERCISE)
            ViewHolderWorkoutExercise(cardView)
        else
            ViewHolderWorkoutEnd(cardView)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, adapterPosition: Int) {

        if (adapterPosition < plan.positionCount()) {

            //Only initialize view holder one time
            if (viewHolderInitialized.contains(adapterPosition)) {
                return
            }

            val viewHolderExercise = holder as ViewHolderWorkoutExercise

            val planPosition = plan.getPosition(adapterPosition)
            val exercise = planPosition.exercise

            //Print set progress text
            val progressSet = 1.toString() + workoutExerciseViewer.activity.getString(R.string.of) + planPosition.setCount.toString()

            var visibilitySetNext = View.VISIBLE
            var visibilitySetAdd = View.GONE
            var buttonTintSetAdd: Int? = null

            //Determine visibility of the next-set and the addAll-set buttons
            if (planPosition.setCount == 1 && plan.id != PLAN_ID_FITNESSTEST) {
                visibilitySetNext = View.GONE
                visibilitySetAdd = View.VISIBLE
                buttonTintSetAdd = Util.color(mContext, R.color.grey850)
            }

            //Bind the view holder
            viewHolderExercise.bind(progressSet, exercise, cardWidth / 16 * 9, visibilitySetNext, visibilitySetAdd, buttonTintSetAdd, exercise.videoUrl)

            //Inflate Set View's
            if (viewHolderExercise.vfWorkoutSetFlipper.childCount == 0) {
                for (i in 0 until planPosition.setCount) {
                    if (exercise.isTypeReps) {
                        workoutExerciseViewer.appendSetViewReps(viewHolderExercise, planPosition, i, false)
                    } else if (exercise.isTypeTime) {
                        workoutExerciseViewer.appendSetViewTime(viewHolderExercise, planPosition, i, false)
                    }
                }
            }

        } else {

            val viewHolderEnd = holder as ViewHolderWorkoutEnd

            viewHolderEnd.bind(onClickWorkoutSave)
        }

        viewHolderInitialized.add(adapterPosition)
    }

    override fun getItemCount(): Int {
        return plan.positions.size + 1
    }

    override fun getItemViewType(adapterPosition: Int): Int =
            if (adapterPosition < plan.positionCount()) VIEW_TYPE_EXERCISE else VIEW_TYPE_END

    inner class ViewHolderWorkoutExercise(private val cardView: View) : RecyclerView.ViewHolder(cardView) {

        var expWorkoutExerciseVideo: SimpleExoPlayerView = cardView.findViewById(R.id.expWorkoutExerciseVideo)
        var tvWorkoutExerciseName: TextView = cardView.findViewById(R.id.tvWorkoutExerciseName)
        var expControllerOverlay: RelativeLayout = cardView.findViewById(R.id.expControllerOverlay)
        var ibExoPlayDummy: ImageButton = cardView.findViewById(R.id.ibExoPlayDummy)
        var ivExerciseThumbnail: ImageView = cardView.findViewById(R.id.ivExerciseThumbnail)
        var pbExoLoad: ProgressBar = cardView.findViewById(R.id.exo_load)
        var tvWorkoutProgressSets: TextView = cardView.findViewById(R.id.tvWorkoutProgressSets)
        var ibWorkoutSetPrevious: ImageButton = cardView.findViewById(R.id.ibWorkoutSetPrevious)
        var ibWorkoutSetNext: ImageButton = cardView.findViewById(R.id.ibWorkoutSetNext)
        var ibWorkoutSetAdd: ImageButton = cardView.findViewById(R.id.ibWorkoutSetAdd)
        var vfWorkoutSetFlipper: ViewFlipper = cardView.findViewById(R.id.vfWorkoutSetFlipper)
        var ibExerciseInfo: ImageButton = cardView.findViewById(R.id.ibExerciseInfo)

        fun bind(progressSet: String, exercise: Exercise, videoPlayerHeight: Int, visibilitySetNext: Int, visibilitySetAdd: Int, buttonTintSetAdd: Int?, videoUrl: String?){

            ibWorkoutSetPrevious.setOnClickListener { workoutExerciseViewer.onClickPreviousSet(this) }
            ibWorkoutSetNext.setOnClickListener { workoutExerciseViewer.onClickNextSet(this) }
            ibWorkoutSetAdd.setOnClickListener { workoutExerciseViewer.onClickAddSet(this) }
            ibExerciseInfo.setOnClickListener { workoutExerciseViewer.onClickExerciseInfo(exercise, cardView as ViewGroup?) }

            tvWorkoutProgressSets.text = progressSet
            tvWorkoutExerciseName.text = exercise.name
            expWorkoutExerciseVideo.layoutParams.height = videoPlayerHeight

            ibWorkoutSetNext.visibility = visibilitySetNext
            ibWorkoutSetAdd.visibility = visibilitySetAdd

            if( buttonTintSetAdd != null ) {
                ibWorkoutSetAdd.drawable.setTint(buttonTintSetAdd)
            }

            //Create Video Player
            val videoPlayer = ExerciseVideoPlayer(mContext)
            videoPlayer.init(ivExerciseThumbnail, expControllerOverlay, ibExoPlayDummy, expWorkoutExerciseVideo, pbExoLoad, videoUrl)
            workoutExerciseViewer.registerVideoPlayer(adapterPosition, videoPlayer)
        }
    }

    inner class ViewHolderWorkoutEnd(cardView: View) : RecyclerView.ViewHolder(cardView) {

        private var bnWorkoutSave: CircularProgressButton = cardView.findViewById(R.id.bnWorkoutSave)
        private var tvWorkoutEndTime: TextView = cardView.findViewById(R.id.tvWorkoutEndTime)
        private var tvWorkoutSetsDoneValue: TextView = cardView.findViewById(R.id.tvWorkoutSetsDoneValue)
        private var tvWorkoutSetsRemainingValue: TextView = cardView.findViewById(R.id.tvWorkoutSetsRemainingValue)
        private var ivWorkoutEndBackground: ImageView = cardView.findViewById(R.id.ivWorkoutEndBackground)

        private val modelMainActivity = ViewModelProviders.of(workoutExerciseViewer.activity).get(ViewModelActivityWorkout::class.java)

        fun bind(onClickWorkoutSave: View.OnClickListener) {

            bnWorkoutSave.setOnClickListener(onClickWorkoutSave)

            val modelMainActivity = ViewModelProviders.of(workoutExerciseViewer.activity).get(ViewModelActivityWorkout::class.java)

            modelMainActivity.workoutTime.observe(workoutExerciseViewer.activity, Observer { tvWorkoutEndTime.text = it ?: "" })

            if( !modelMainActivity.completedStandardSets.hasActiveObservers() ) {
                modelMainActivity.completedStandardSets.observe(workoutExerciseViewer.activity, observerCompletedSetCount)
                tvWorkoutSetsRemainingValue.text = modelMainActivity.standardSetAmount.toString()
            }else{
                observerCompletedSetCount.onChanged(modelMainActivity.completedStandardSets.value)
            }
        }

        private val observerCompletedSetCount = Observer<Int?> {

            tvWorkoutSetsDoneValue.text = it.toString()

            tvWorkoutSetsRemainingValue.text = (modelMainActivity.standardSetAmount - it!!).toString()

            if( (modelMainActivity.standardSetAmount - it) == 0 ){
                ivWorkoutEndBackground.setImageResource(R.drawable.background_workout_end_green)
                bnWorkoutSave.backgroundTintList = Util.getOneColorStateList(workoutExerciseViewer.activity, R.color.green_light)
            }
        }
    }

    companion object {
        const val VIEW_TYPE_EXERCISE = 0
        const val VIEW_TYPE_END = 1
    }
}