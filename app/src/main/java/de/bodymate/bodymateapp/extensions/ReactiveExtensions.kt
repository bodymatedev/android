package de.bodymate.bodymateapp.extensions

import io.reactivex.disposables.Disposable

fun Disposable.disposeSave(){
    if( !this.isDisposed ) dispose()
}