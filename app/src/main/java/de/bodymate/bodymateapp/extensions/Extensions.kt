package de.bodymate.bodymateapp.extensions

import android.content.Context
import android.support.annotation.ColorRes
import android.support.annotation.StringRes
import android.support.v4.content.ContextCompat
import android.util.ArrayMap
import com.google.firebase.auth.FirebaseAuth
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.JsonParseException
import com.google.gson.JsonSyntaxException
import com.google.gson.reflect.TypeToken
import org.json.JSONArray
import org.json.JSONObject

    fun Context.string(@StringRes stringId:Int):String = this.getString(stringId)

    fun Context.color(@ColorRes colorId: Int): Int = ContextCompat.getColor(this, colorId)

    inline fun <reified T> Gson.fromJsonExt(json: String) = this.fromJson<T>(json, object: TypeToken<T>() {}.type)

    inline fun <reified T> JSONArray.toObjectList() =
            try{
                GsonBuilder().create().fromJson<T>(toString(), object: TypeToken<T>() {}.type)
            }catch (e: JsonSyntaxException){
                e.printStackTrace()
                null
            }catch( e: JsonParseException){
                e.printStackTrace()
                null
            }

    inline fun <reified T> JSONObject.toObject() =
            try{
                GsonBuilder().create().fromJson<T>(toString(), T::class.java)
            }catch (e: JsonSyntaxException){
                e.printStackTrace()
                null
            }catch( e: JsonParseException){
                e.printStackTrace()
                null
            }

    fun FirebaseAuth.getFireId(onResult: (String?) -> Unit){

        if ( currentUser != null ) {

            currentUser!!.getIdToken(true)
                    .addOnSuccessListener {

                        if ( it.claims != null) {

                            val userData = it.claims ?: ArrayMap(0)

                            if (userData.containsKey("user_id")) {
                                onResult(userData["user_id"] as String)
                            }else {
                                onResult(null)
                            }
                        }else{
                            onResult(null)
                        }
                    }.addOnCanceledListener {
                        onResult(null)
                    }.addOnFailureListener {
                        onResult(null)
                    }
        }else{
            onResult(null)
        }
    }
