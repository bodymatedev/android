package de.bodymate.bodymateapp.extensions

import de.bodymate.bodymateapp.util.C

fun String.isTrue(): Boolean = this == C.TRUE
fun String.isFalse(): Boolean = this == C.FALSE