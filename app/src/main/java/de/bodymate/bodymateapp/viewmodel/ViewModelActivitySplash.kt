package de.bodymate.bodymateapp.viewmodel

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.MutableLiveData
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.GetTokenResult
import de.bodymate.bodymateapp.data.repository.entity.RoomUser
import de.bodymate.bodymateapp.data.service.UserService
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class ViewModelActivitySplash(application: Application) : AndroidViewModel(application) {

    val tokenTask: MutableLiveData<GetTokenResult> = MutableLiveData()
}