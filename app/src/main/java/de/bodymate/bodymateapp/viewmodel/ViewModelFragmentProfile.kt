package de.bodymate.bodymateapp.viewmodel

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.LiveData
import de.bodymate.bodymateapp.data.repository.entity.RoomSchedule
import de.bodymate.bodymateapp.data.repository.entity.RoomUser
import de.bodymate.bodymateapp.data.service.ScheduleService
import de.bodymate.bodymateapp.data.service.UserService
import de.bodymate.bodymateapp.data.service.WorkoutService
import de.bodymate.bodymateapp.paging.WorkoutPagingRepository
import io.reactivex.Single

/**
 * Created by Leonard on 18.06.2018.
 */

class ViewModelFragmentProfile(application: Application) : AndroidViewModel(application) {

    private val userService by lazy {
        UserService.getInstance(application)
    }

    private val scheduleService by lazy {
        ScheduleService.getInstance(application)
    }

    private val workoutService by lazy{
        WorkoutService.getInstance(application)
    }

    val user: LiveData<RoomUser> = userService.currentUser

    val schedules: LiveData<List<RoomSchedule>> = scheduleService.allSchedules

    val workoutCount: LiveData<Int> = workoutService.workoutCount()

    /**
     * Workout paging
     */

    private val workoutListing = WorkoutPagingRepository(application).workouts()

    val allWorkoutsByDate = workoutListing.pagedList
    val networkStateWorkoutPaging = workoutListing.networkState

    fun retry(){

        workoutListing.retry.invoke()
    }
}
