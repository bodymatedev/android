package de.bodymate.bodymateapp.viewmodel

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.util.Log
import com.android.volley.Response
import com.google.gson.GsonBuilder
import de.bodymate.bodymateapp.api.ApiFunctions
import de.bodymate.bodymateapp.api.ApiUtils
import de.bodymate.bodymateapp.api.response.RBEquipment
import de.bodymate.bodymateapp.extensions.fromJsonExt

/**
 * Created by Leonard on 29.05.2018.
 */

class ViewModelFragmentRegisterDetailEquipments : ViewModel() {

    val equipmentList: MutableLiveData<List<RBEquipment>> = MutableLiveData()

    init {

        //Make one time async backend call to receive allEquipments possible equipments
        ApiFunctions.getAllEquipments(
                Response.Listener { response ->

                    val rbEquipments = GsonBuilder().create().fromJsonExt<List<RBEquipment>>(response.toString())

                    //val rbEquipments = GsonBuilder().create().fromJsonExt<List<RBEquipment>>(response.toString(), object : TypeToken<List<RBEquipment>>() {}.type)

                    if (rbEquipments.isNotEmpty()) {
                        equipmentList.postValue(rbEquipments)
                    }
                }, Response.ErrorListener { error -> Log.e(ApiUtils.LOG_TAG_ERROR, ApiUtils.parseVolleyError(error)) })
    }
}
