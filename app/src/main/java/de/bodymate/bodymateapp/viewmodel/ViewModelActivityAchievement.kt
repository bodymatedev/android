package de.bodymate.bodymateapp.viewmodel

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import de.bodymate.bodymateapp.data.service.AchievementService

class ViewModelActivityAchievement(application: Application): AndroidViewModel(application)  {

    private val achievementService by lazy {
        AchievementService.getInstance(application)
    }

    val allAchievements = achievementService.allAchievements
}