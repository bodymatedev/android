package de.bodymate.bodymateapp.viewmodel

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.MutableLiveData

class ViewModelActivityMain(application: Application): AndroidViewModel(application) {

    val selectedTab: MutableLiveData<Int> = MutableLiveData()
}