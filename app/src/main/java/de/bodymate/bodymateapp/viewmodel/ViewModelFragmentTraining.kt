package de.bodymate.bodymateapp.viewmodel

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.LiveData

import de.bodymate.bodymateapp.data.internentity.Plan
import de.bodymate.bodymateapp.data.repository.entity.RoomExercise
import de.bodymate.bodymateapp.data.repository.entity.RoomSchedule
import de.bodymate.bodymateapp.data.repository.entity.RoomWorkout
import de.bodymate.bodymateapp.data.service.ExerciseService
import de.bodymate.bodymateapp.data.service.PlanService
import de.bodymate.bodymateapp.data.service.ScheduleService
import de.bodymate.bodymateapp.data.service.WorkoutService
import de.bodymate.bodymateapp.exception.EntityFormatException
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.functions.Function3
import io.reactivex.schedulers.Schedulers

class ViewModelFragmentTraining(application: Application) : AndroidViewModel(application) {

    private val planService by lazy {
        PlanService.getInstance(application)
    }

    private val workoutService by lazy {
        WorkoutService.getInstance(application)
    }

    private val exerciseService by lazy {
        ExerciseService.getInstance(application)
    }

    private val scheduleService by lazy {
        ScheduleService.getInstance(application)
    }

    val timeLineData: Single<TimeLineDataModel> =
            Single.zip(planService.allPlans, scheduleService.allSchedulesAsSingle, exerciseService.allExercises, Function3<List<Plan>, List<RoomSchedule>, List<RoomExercise>, TimeLineDataModel>
                { plans, schedules, exercises ->
                    if (plans.isEmpty() || schedules.isEmpty() || exercises.isEmpty()) throw EntityFormatException()
                    TimeLineDataModel(plans, schedules, exercises)
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())

    val latest3Workouts: LiveData<List<RoomWorkout>> = workoutService.latest3

    data class TimeLineDataModel(var plans: List<Plan>, var schedules: List<RoomSchedule>, var exercises: List<RoomExercise>)
}
