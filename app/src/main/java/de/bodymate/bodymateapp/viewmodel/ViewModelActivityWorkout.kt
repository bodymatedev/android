package de.bodymate.bodymateapp.viewmodel

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.MutableLiveData

import de.bodymate.bodymateapp.data.internentity.Plan
import de.bodymate.bodymateapp.data.repository.entity.RoomExercise
import de.bodymate.bodymateapp.data.service.ExerciseService
import de.bodymate.bodymateapp.data.service.PlanService
import de.bodymate.bodymateapp.exception.EntityFormatException
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.functions.BiFunction
import io.reactivex.schedulers.Schedulers

class ViewModelActivityWorkout(application: Application) : AndroidViewModel(application) {

    private val planService by lazy {
        PlanService.getInstance(application)
    }

    private val exerciseService by lazy {
        ExerciseService.getInstance(application)
    }

    val workoutTime : MutableLiveData<String> = MutableLiveData()
    val completedStandardSets: MutableLiveData<Int?> = MutableLiveData()
    var standardSetAmount: Int = 0

    private fun getPlan(planId: Int): Single<Plan> =
            planService.getById(planId)

    fun getWorkoutData(planId: Int): Single<WorkoutDataModel> =
            Single.zip(getPlan(planId), exerciseService.allExercises, BiFunction<Plan, List<RoomExercise>, WorkoutDataModel>
            { plan, exercises ->
                if (exercises.isEmpty()) throw EntityFormatException()
            WorkoutDataModel(plan, exercises)
            })
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())

    data class WorkoutDataModel(val plan: Plan, val exercises: List<RoomExercise>)
}
